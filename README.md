[![Netlify Status](https://api.netlify.com/api/v1/badges/d3e15d43-2568-4f59-b0c9-8d158f2adc3d/deploy-status)](https://app.netlify.com/sites/gofoss/deploys)

# GoFOSS

<img src="https://gofoss.today/assets/img/gofoss_favicon.png" alt="GoFOSS" width="150px"></img>

This is the source code of [https://gofoss.today](https://gofoss.today/). The aim of this website is to make privacy respecting, free and open source software accessible to all.

## Highlights

* **[Privacy](https://www.gofoss.today/nothing-to-hide)**: brief introduction to online privacy
* **[Browser](https://www.gofoss.today/intro-browse-freely)**: overview of safe and private browsers
* **[Communication](https://www.gofoss.today/intro-speak-freely)**: guide to secure emails, messages and calls with end-to-end encryption
* **[Data](https://www.gofoss.today/intro-store-safely)**: useful tips to secure online data with strong passwords, two-factor authentication, backups and encryption
* **[Desktop OS](https://www.gofoss.today/intro-free-your-computer)**: quick introduction to GNU/Linux and free & open source apps
* **[Mobile OS](https://www.gofoss.today/intro-free-your-phone)**: manual to degoogle your phone, install LineageOS or CalyxOS as well as tracker-free mobile apps
* **[Cloud](https://www.gofoss.today/intro-free-your-cloud)**: list of privacy-respecting online services & instructions to self-hosting

## Why?

[Surveillance Capitalism](https://en.wikipedia.org/wiki/Surveillance_capitalism/) is an economic system centred around tech monopolies which harvest private user data to maximize profit.

This system threatens fundamental humans rights such as freedom and privacy, the very core of democracy. It gives rise to mass surveillance, polarizes the political debate, interferes with electoral processes, drives uniformity of thought and facilitates censorship.

Free and open source software can contribute to protect those rights. Learn how to browse the Internet, chat with your loved ones or share memories without anyone collecting, recording, monetising or censoring your data.

## Contribute

GoFOSS is a community driven project with the aim to make online privacy accessible to all. You can contribute by improving the content, helping with translations or just reaching out.

[Please read the guidelines](https://gofoss.today/en/contributing/) to learn about all the ways to get involved.

## Screenshots

![GoFOSS screenshots](docs/assets/img/screenshot_gofoss.png)

## License

Released with ❤️ under [CC BY-NC-SA 4.0](http://creativecommons.org/licenses/by-nc-sa/4.0/).

Please also refer to the [privacy statement, legal disclaimer and license terms](https://gofoss.today/en/disclaimer/).