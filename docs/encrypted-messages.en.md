---
template: main.html
title: Encrypted messaging
description: Protect your messages with end-to-end encryption. Choose privacy respecting messenging apps like Signal, Element, Jami, Briar or Silence.
---

# Encrypt your messages

<div align="center">
<img src="../../assets/img/email_2.png" alt="Encrypted messenger apps" width="400px"></img>
</div>

Choose trustworthy messenging apps. Carefully assess aspects such as available features, encryption technologies or server locations — privacy legislation changes from country to country. This chapter provides a (brief) overview of popular, privacy respecting messenging apps.

!!! level "This chapter is geared towards beginners & intermediate users. Some tech skills may be required."

??? tip "Show me a comparision of privacy respecting messenger apps"

    <center>

    | | Signal | Element | Jami | Briar | Silence |
    | ------ | :------: | :------: | :------: | :------: | :------: |
    | Creation date | 2014 | 2016 | 2005/2017^1^ | 2018 | 2015 |
    | Jurisdiction | US | UK | Canada |N/A^2^ | N/A^2^ |
    | Funding | Various foundations^3^ | Matrix Foundation, New Vector Limited | Free Software Foundation^4^ |Donations | Donations |
    | Architecture | Centralised | Decentralised | Peer-to-Peer |Peer-to-Peer | Peer-to-Peer |
    | Open source | [Yes](https://github.com/signalapp/) | [Yes](https://github.com/vector-im/) | [Yes](https://git.jami.net/savoirfairelinux) |[Yes](https://code.briarproject.org/briar/briar/) | [Yes](https://git.silence.dev/Silence/Silence-Android/) |
    | Protocol | Signal | Matrix | SIP/OpenDHT |Bramble | SMS |
    | End-to-end encryption | <span style="color:green">✔</span> (by default)^5^ | <span style="color:green">✔</span> (not by default)^6^ | <span style="color:green">✔</span> (by default)^7^ |<span style="color:green">✔</span> (by default)^8^ | <span style="color:green">✔</span> (not by default)^9^ |
    | Perfect forward secrecy | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> |<span style="color:orange">?</span>^10^ | <span style="color:green">✔</span> |
    | Security audit | [2016](https://eprint.iacr.org/2016/1013.pdf), [2017](https://hal.inria.fr/hal-01575923/file/KobeissiBhargavanBlanchetEuroSP17.pdf) | <span style="color:red">✗</span>^11^ | <span style="color:red">✗</span> |[2017](https://briarproject.org/raw/BRP-01-report.pdf) | <span style="color:red">✗</span> |
    | No logging | <span style="color:green">✔</span> | <span style="color:orange">?</span>^12^ | <span style="color:red">✗</span>^13^ |<span style="color:green">✔</span> | <span style="color:green">✔</span> |
    | GDPR compliance | <span style="color:red">✗</span>^14^ | <span style="color:orange">?</span>^15^ | <span style="color:orange">?</span> | <span style="color:orange">?</span> |<span style="color:orange">?</span> |
    | Anonymous signup | <span style="color:red">✗</span>^16^ | <span style="color:green">✔</span> | <span style="color:green">✔</span> |<span style="color:green">✔</span> | <span style="color:red">✗</span>^16^ |
    | Instant messages | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> |<span style="color:green">✔</span> | <span style="color:green">✔</span> (SMS/MMS) |
    | Video/voice calls | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> |<span style="color:red">✗</span> | <span style="color:red">✗</span> |
    | Windows client | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:red">✗</span> | <span style="color:red">✗</span> |
    | macOS client | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:red">✗</span> | <span style="color:red">✗</span> |
    | Linux client | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:red">✗</span> | <span style="color:red">✗</span> |
    | Android client | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> |
    | iOS client | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:red">✗</span> | <span style="color:red">✗</span> |

    </center>

    1. *Jami builds on GNU/Ring and SFLphone, which have been around since 2015 and 2005, respectively.*
    2. *Briar and Silence are based on a peer-to-peer architecture, where users directly connect without relying on servers.*
    3. *Signal Foundation, Freedom of the Press Foundation, Shuttleworth Foundation, Knight Foundation, Open Technology Fund.*
    4. *In November 2016, it became part of the GNU Project, which is funded by the Free Software Foundation.*
    5. *Signal, AES-256, Curve25519, HMAC-SHA256.*
    6. *Olm/Megaolm, AES-256, Curve25519, HMAC-SHA256.*
    7. *TLS 1.3 with RSA keys & X.509 certificates.*
    8. *Bramble, AES-256.*
    9. *Signal/Axolotl, AES-256.*
    10. *While Briar offers perfect forward secrecy, notethat deniability is not provided for one-to-many communications, such as group chats.*
    11. *At the time of writing, there has been no full security audit of Element. In 2016, NCC Group Security Services [reviewed Matrix's encryption algorithm](https://www.nccgroup.com/globalassets/our-research/us/public-reports/2016/november/ncc_group_olm_cryptogrpahic_review_2016_11_01.pdf).*
    12. *Depends on the Matrix server. Users can choose between several independent servers, some log IP addresses, others don't. Keep in mind that the Matrix server administrator can access almost all users' data (e.g. ID, username, passwords, email, phone number, device information, usage pattern, IP address, etc.), even if the message contents remain encrypted.*
    13. *IP addresses can become visible if not protected carefully via Tor or VPN*
    14. *According to Signal, this is an ["evolving process"](https://support.signal.org/hc/en-us/articles/360007059412-Signal-and-the-General-Data-Protection-Regulation-GDPR-).*
    15. *Depends on the Matrix server, be sure to check the privacy policy. Keep in mind that the Matrix server administrator can access almost all users' data (e.g. ID, username, email, phone number, device information, usage pattern, IP address, etc.), even if the message contents remain encrypted.*
    16. *Mobile number required to sign up.*



??? question "What about XMPP?"

    [XMPP](https://xmpp.org/) is great! We chose however not to cover XMPP clients in this guide for a couple of reasons:

    * **End-to-end ecryption is not enabled by default**: while XMPP can in theory encrypt your communication, it's not enabled by default. Due to the fragmentation of servers and clients being used, your communication might be encrypted or not. In addition, group chats often don't support encryption. By default, XMPP users should therefore assume that their chats are not encrypted.

    * **XMPP collects meta data**: like Matrix, XMPP does not pursue the "zero knowledge" principle and collects meta data. Indeed, the account management is handled by the XMPP server administrators. By using a public XMPP server, you therefore entrust some of your data to these administrators. Even though communication can be encrypted, administrators are potentially able to keep a copy of all (encrypted) communication, log IP addresses or access (unencrypted) data such as usernames, passwords, email addresses, phone numbers, media files, device information, contact lists, usage profiles, group memberships and so on. Even if you self-host your own server, administrators of XMPP servers participating in a conversation can access this meta data.

    This being said, there is an active XMPP community and several open source clients. They all rely on XMPP's decentralized architecture, where users can choose between several independent servers or even self-host their own instance:

    * [Gajim](https://gajim.org/), a messenger and video calling app, available on all desktop environments
    * [Conversations](https://conversations.im/), a messenger and video calling app for Android



<br>

<center> <img src="../../assets/img/separator_signal.svg" alt="Signal" width="150px"></img> </center>

## Signal

[Signal](https://signal.org/en/) is a fast, simple and secure messaging and video calling app. It's free, [open source](https://github.com/signalapp/) and relies on a centralized architecture, operated by the Signal Foundation. The app features end-to-end encryption for text, voice, videos, pictures and files. This means that Signal can't access the content of messages. Signal also persues the "zero knowledge" principle, meaning that it doesn't want to have visibility on the user's social graph or usage profile. More detailed instructions below.

=== "Android"

    ??? tip "Show me the step-by-step guide for Android"

        ### Installation & registration

        | Instructions | Description |
        | ------ | ------ |
        |Install Signal |Install Signal from: <br>• [Google's Play Store](https://play.google.com/store/apps/details?id=org.thoughtcrime.securesms&hl=en_US)<br>• [Aurora Store](https://auroraoss.com/)<br>• [Signal's website](https://signal.org/android/apk/) (download the `.apk` file manually). |
        |Tracker free |The app [contains 0 trackers and requires 67 permissions](https://reports.exodus-privacy.eu.org/en/reports/org.thoughtcrime.securesms/latest/). By comparison:<br>• TikTok: 16 trackers, 76 permissions<br>• Snapchat: 2 trackers, 44 permissions<br>• WhatsApp: 1 tracker, 57 permissions |
        |No GCM |Note that Signal on Android can handle push notifications [without Google's Cloud Messaging (GCM)](https://github.com/signalapp/Signal-Android/commit/1669731329bcc32c84e33035a67a2fc22444c24b/) and falls back on websockets if GCM is not available. This comes in handy if you are looking to [de-google your phone](https://gofoss.today/intro-free-your-phone#google-ios-free-phones/). |
        |Registration |• Open Signal and follow the on-screen instructions <br>• When prompted, provide a phone number and tap `Register` <br>• You'll receive a verification code by SMS, it should be detected automatically <br>• Alternatively, you can request to be called. |

        ### One-to-one chats, voice or video calls

        | Instructions | Description |
        | ------ | ------ |
        |Start one-to-one conversation |• Open the Signal app <br>• Tap the `Compose` button <br>• Select a contact or enter a number <br>• Tap the `text input field` and write your message <br>• Tap the blue `Send` button |
        |Disappearing messages |• Signal allows to set a timer after which messages disappear from your devices <br>• Open a chat <br>• tap your contact's name <br>• Activate the option `Disappearing messages` <br>• Define a deadline of up to one week <br>• Once activated, a timer icon will be visible next to each disappearing message |
        |SMS/MMS | • On Android, Signal can also send unencrypted SMS or MMS through your mobile provider <br>• Go to `Profile ‣ SMS and MMS ‣ Activate SMS`<br>• Long-press the `Send` button in any conversation <br>• Select the grey button to send an unencrypted SMS message |
        |Start one-to-one voice call |• Open Signal <br>• Tap the `Compose` button  <br>• Select a contact or enter a number <br>• Tap the `Phone` icon in the top menu to start a voice call |
        |Start one-to-one video call |• Open Signal <br>• Tap the `Compose` button  <br>• Select a contact or enter a number <br>• Tap the `Camera` icon in the top menu to start a video call |

        ### Group chats, voice or video calls

        | Instructions | Description |
        | ------ | ------ |
        |Start group chat |• Open the Signal app <br>• Tap the `Compose` button <br>• Select `New group` <br>• Add existing contacts or enter new numbers <br>• Tap the `Next` button <br>• Pick a group name. <br>• Tap the `Create` button |
        |Disappearing messages |• Signal allows to set a timer after which messages disappear from your devices <br>• Open a chat <br>• Tap the group name <br>• Activate the option `Disappearing messages` <br>• Define a deadline of up to one week <br>• Once activated, a timer icon will be visible next to each disappearing message. |
        |SMS/MMS |• On Android, Signal can also create unencrypted MMS groups <br>• Messages are sent through your mobile provider <br>• The `Send` button will be grey instead of blue |
        |Start group voice or video call |• Open Signal <br>• Create a new or open an existing group chat <br>• Tap the `Camera` icon in the top menu <br>• Select `Start Call` or `Join Call` <br>• For a group voice call, disable the camera |

        ### Authentication, Signal-PIN & backups

        | Instructions | Description |
        | ------ | ------ |
        |Authentication |• Make sure you're speaking to the right people <br>• Signal can authenticate counterparts based on a fingerprint <br>• Open the Signal app  <br>• Open an ongoing chat <br>• Tap your contact's name <br>• Select `View Safety Number` <br>• Compare the appearing sequence of numbers with the one displayed on your contact's phone. If they match, tap the `Verified` switch <br>• Alternatively, scan the QR code appearing on your contact's phone <br>• After successful verification, a check mark will be displayed under your contact's name <br>• If the verification number changes, for example if you or your contact changes phones or reinstalls the app, a notification will be sent and the authentication process needs to be repeated |
        |Signal PIN |• You can define a Signal PIN to recover your profile, settings or contacts if you ever lose or switch devices <br>• All of this is possible without revealing your social graph to Signal <br>• Go to `Settings ‣ Privacy ‣ PIN` to set and manage your PIN <br>• Be sure to choose a [strong, unique PIN](https://gofoss.today/passwords/) and make sure to store it safely <br>• If you forgot your PIN, you may be locked out of your account for up to 7 days. Signal cannot reset the PIN for you |
        |Chat backups |• Messages are encrypted and only stored on your devices <br>• If you loose your device or mistakenly erase your messages, they are gone forever <br>• To be able to restore messages, or port them to another device, you need to create backups <br><br>**Backup**<br>• Go to `Settings ‣ Chats ‣ Chat backups ‣ Turn on` <br>• Choose a folder where to save the backup file <br>• [Safely store](https://gofoss.today/passwords/) the displayed 30-digit passphrase, which is required to restore backups <br>• Select `Enable backups` <br>• Tap `Create backup` <br>• Move the generated backup file to your [backup devices](https://gofoss.today/backups/) <br><br>**Restore**• Move the backup file to your device <br>• (Re-)install Signal <br>• Select `Transfer or restore account` <br>• Select `Restore from backup` <br>• Enter the 30-digit passphrase <br>• Submit your phone number for registration |
        |Media backups |• Photos, videos, audio files and so on are encrypted and only stored on your devices <br>• If you loose your devices or mistakenly erase your files, they are gone forever <br>• [Create backups](https://gofoss.today/backups/)! <br>• Go to `Settings ‣ Data and Storage ‣ Manage storage ‣ Review storage` <br>• Select the files you want to back up <br>• Tap the `Save` button and select `Yes` to save the (unencrypted) media files outside of Signal <br>• Move the saved media files to your [backup devices](https://gofoss.today/backups/) |


=== "iOS"

    ??? tip "Show me the step-by-step guide for iOS"

        ### Installation & registration

        | Instructions | Description |
        | ------ | ------ |
        |Installation |Install Signal from the [App Store](https://apps.apple.com/us/app/signal-private-messenger/id874139669/). |
        |Registration |• Open Signal and follow the on-screen instructions <br>• When prompted, provide a phone number and tap `Register` <br>• You'll receive a verification code by SMS, it should be detected automatically <br>• Alternatively, you can request to be called. |

        ### One-to-one chats, voice or video calls

        | Instructions | Description |
        | ------ | ------ |
        |Start one-to-one conversation |• Open the Signal app <br>• Tap the `Compose` button <br>• Select a contact or enter a number <br>• Tap the `text input field` and write your message <br>• Tap the blue `Send` button |
        |Disappearing messages | • Signal allows to set a timer after which messages disappear from your devices <br>• Open a chat <br>• Tap your contact's name <br>• Activate the option `Disappearing messages` <br>• Define a deadline of up to one week <br>• Once activated, a timer icon will be visible next to each disappearing message |
        |Start one-to-one voice call |• Open Signal <br>• Tap the `Compose` button  <br>• Select a contact or enter a number <br>• Tap the `Phone` icon in the top menu to start a voice call |
        |Start one-to-one video call |• Open Signal <br>• Tap the `Compose` button  <br>• Select a contact or enter a number <br>• Tap the `Camera` icon in the top menu to start a video call |

        ### Group chats, voice or video calls

        | Instructions | Description |
        | ------ | ------ |
        |Start group chat |• Open the Signal app <br>• Tap the `Compose` button <br>• Select `New group` <br>• Add existing contacts or enter new numbers <br>• Tap the `Next` button <br>• Pick a group name. <br>• Tap the `Create` button |
        |Disappearing messages |• Signal allows to set a timer after which messages disappear from your devices <br>• Open a chat <br>• Tap the group name <br>• Activate the option `Disappearing messages` <br>• Define a deadline of up to one week <br>• Once activated, a timer icon will be visible next to each disappearing message. |
        |Start group voice or video call |• Open Signal <br>• Create a new or open an existing group chat <br>• Tap the `Camera` icon in the top menu <br>• Select `Start Call` or `Join Call` <br>• For a group voice call, disable the camera |

        ### Authentication, Signal-PIN & backups

        | Instructions | Description |
        | ------ | ------ |
        |Authentication |• Make sure you're speaking to the right people <br>• Signal can authenticate counterparts based on a fingerprint <br>• Open the Signal app  <br>• Open an ongoing chat <br>• Tap your contact's name <br>• Select `View Safety Number` <br>• Compare the appearing sequence of numbers with the one displayed on your contact's phone. If they match, tap the `Verified` switch <br>• Alternatively, scan the QR code appearing on your contact's phone <br>• After successful verification, a check mark will be displayed under your contact's name <br>• If the verification number changes, for example if you or your contact changes phones or reinstalls the app, a notification will be sent and the authentication process needs to be repeated |
        |Signal PIN |• You can define a Signal PIN to recover your profile, settings or contacts if you ever lose or switch devices <br>• All of this is possible without revealing your social graph to Signal <br>• Go to `Settings ‣ Privacy ‣ PIN` to set and manage your PIN <br>• Be sure to choose a [strong, unique PIN](https://gofoss.today/passwords/) and make sure to store it safely <br>• If you forgot your PIN, you may be locked out of your account for up to 7 days. Signal cannot reset the PIN for you |
        |Media backups |• Photos, videos, audio files and so on are encrypted and only stored on your devices <br>• If you loose your devices or mistakenly erase your files, they are gone forever <br>• [Create backups](https://gofoss.today/backups/)! <br>• Open a chat <br>• Tap the contact name in the top menu to view the chat settings <br>• Select `All Media` <br>• Select the files you want to back up <br>• Tap the `Share` button and select `Save Image` or `Save Video` to save the (unencrypted) media files outside of Signal <br>• Move the saved media files to your [backup devices](https://gofoss.today/backups/) |


=== "Windows"

    ??? tip "Show me the step-by-step guide for Windows"

        ### Installation & linking

        | Instructions | Description |
        | ------ | ------ |
        |Installation |• Confirm that Signal is installed and working on your phone <br>• Download Signal's Electron-based [Windows Desktop client](https://signal.org/download/windows/) <br>• Click on the `Run` button <br>• Follow the installation wizard. |
        |Linking |• Open Signal on your phone <br>• Go to `Settings ‣ Linked devices` <br>• Tap the `+` button (Android) or `Link New Devices` (iPhone) <br>• Use your phone's Signal app to scan the QR code displayed on your Windows computer <br>• Choose a name for the linked device <br>• Select `Finish` |

        ### One-to-one chats, voice or video calls

        | Instructions | Description |
        | ------ | ------ |
        |Start one-to-one conversation |• Open Signal <br>• Click on the `Compose` button <br>• Select a contact or enter a number <br>• Start a new conversation, or select an existing one <br>• Write your message <br>• Click `ENTER` to send your message |
        |Disappearing messages | • Signal allows to set a timer after which messages disappear from your devices <br>• Open a chat <br>• Tap your contact's name <br>• Activate the option `Disappearing messages` <br>• Define a deadline of up to one week <br>• Once activated, a timer icon will be visible next to each disappearing message |
        |Start one-to-one voice call |• Open Signal <br>• Click on the `Compose` button  <br>• Select a contact or enter a number <br>• Click on the `Phone` icon in the top menu to start a voice call |
        |Start one-to-one video call |• Open Signal <br>• Click on the `Compose` button  <br>• Select a contact or enter a number <br>• Click on the `Camera` icon in the top menu to start a video call <br>• Screen sharing: while in a video call, click on the `Start presenting` button and select your entire screen or a specific window |

        ### Group chats, voice or video calls

        | Instructions | Description |
        | ------ | ------ |
        |Start group chat |• Open Signal <br>• Click on the `Compose` button <br>• Select `New group` <br>• Add existing contacts or enter new numbers <br>• Tap the `Next` button <br>• Pick a group name. <br>• Tap the `Create` button |
        |Disappearing messages |• Signal allows to set a timer after which messages disappear from your devices <br>• Open a chat <br>• Tap the group name <br>• Activate the option `Disappearing messages` <br>• Define a deadline of up to one week <br>• Once activated, a timer icon will be visible next to each disappearing message. |
        |Start group voice or video call |• Open Signal <br>• Create a new or open an existing group chat <br>• Click on the `Camera` icon in the top menu <br>• Select `Start Call` or `Join Call` <br>• Screen sharing: while in a video call, click on the `Start presenting` button and select your entire screen or a specific window <br>• For a group voice call, disable the camera |

        ### Authentication, Signal-PIN & backups

        | Instructions | Description |
        | ------ | ------ |
        |Authentication |• Make sure you're speaking to the right people <br>• Signal can authenticate counterparts based on a fingerprint <br>• Open Signal  <br>• Open an ongoing chat <br>• Tap your contact's name <br>• Select `View Safety Number` <br>• Compare the appearing sequence of numbers with the one displayed on your contact's device <br>•If the numbers match, select `Mark as verified` <br>• After successful verification, a check mark will be displayed under your contact's name <br>• If the verification number changes, for example if you or your contact changes phones or reinstalls the app, a notification will be sent and the authentication process needs to be repeated |
        |Media backups |• Photos, videos, audio files and so on are encrypted and only stored on your devices <br>• If you loose your devices or mistakenly erase your files, they are gone forever <br>• [Create backups](https://gofoss.today/backups/)! <br>• Open a chat <br>• Click on the contact name in the top menu to view the chat settings <br>• Select `View recent media` <br>• Select the files you want to back up <br>• Click on the `Save` button to save the (unencrypted) media files outside on your desktop device <br>• Move the saved media files to your [backup devices](https://gofoss.today/backups/) |


=== "macOS"

    ??? tip "Show me the step-by-step guide for macOS"

        ### Installation & linking

        | Instructions | Description |
        | ------ | ------ |
        |Installation |• Confirm that Signal is installed and working on your phone <br>• Download Signal's Electron-based [macOS Desktop client](https://signal.org/download/macos/), which should open by itself and mount a new volume containing the Signal application <br>• If not, open the downloaded `.dmg` file and drag the appearing Signal icon on top of the Application folder <br>• For easy access, open the Applications folder and drag the Signal icon to your dock |
        |Linking |• Open Signal on your phone <br>• Go to `Settings ‣ Linked devices` <br>• Tap the `+` button (Android) or `Link New Devices` (iPhone) <br>• Use your phone's Signal app to scan the QR code displayed on your macOS device <br>• Choose a name for the linked device <br>• Select `Finish` |

        ### One-to-one chats, voice or video calls

        | Instructions | Description |
        | ------ | ------ |
        |Start one-to-one conversation |• Open Signal <br>• Click on the `Compose` button <br>• Select a contact or enter a number <br>• Start a new conversation, or select an existing one <br>• Write your message <br>• Click `ENTER` to send your message |
        |Disappearing messages | • Signal allows to set a timer after which messages disappear from your devices <br>• Open a chat <br>• Tap your contact's name <br>• Activate the option `Disappearing messages` <br>• Define a deadline of up to one week <br>• Once activated, a timer icon will be visible next to each disappearing message |
        |Start one-to-one voice call |• Open Signal <br>• Click on the `Compose` button  <br>• Select a contact or enter a number <br>• Click on the `Phone` icon in the top menu to start a voice call |
        |Start one-to-one video call |• Open Signal <br>• Click on the `Compose` button  <br>• Select a contact or enter a number <br>• Click on the `Camera` icon in the top menu to start a video call <br>• Screen sharing: while in a video call, click on the `Start presenting` button and select your entire screen or a specific window |

        ### Group chats, voice or video calls

        | Instructions | Description |
        | ------ | ------ |
        |Start group chat |• Open Signal <br>• Click on the `Compose` button <br>• Select `New group` <br>• Add existing contacts or enter new numbers <br>• Tap the `Next` button <br>• Pick a group name. <br>• Tap the `Create` button |
        |Disappearing messages |• Signal allows to set a timer after which messages disappear from your devices <br>• Open a chat <br>• Tap the group name <br>• Activate the option `Disappearing messages` <br>• Define a deadline of up to one week <br>• Once activated, a timer icon will be visible next to each disappearing message. |
        |Start group voice or video call |• Open Signal <br>• Create a new or open an existing group chat <br>• Click on the `Camera` icon in the top menu <br>• Select `Start Call` or `Join Call` <br>• Screen sharing: while in a video call, click on the `Start presenting` button and select your entire screen or a specific window <br>• For a group voice call, disable the camera |

        ### Authentication, Signal-PIN & backups

        | Instructions | Description |
        | ------ | ------ |
        |Authentication |• Make sure you're speaking to the right people <br>• Signal can authenticate counterparts based on a fingerprint <br>• Open Signal  <br>• Open an ongoing chat <br>• Tap your contact's name <br>• Select `View Safety Number` <br>• Compare the appearing sequence of numbers with the one displayed on your contact's device <br>•If the numbers match, select `Mark as verified` <br>• After successful verification, a check mark will be displayed under your contact's name <br>• If the verification number changes, for example if you or your contact changes phones or reinstalls the app, a notification will be sent and the authentication process needs to be repeated |
        |Media backups |• Photos, videos, audio files and so on are encrypted and only stored on your devices <br>• If you loose your devices or mistakenly erase your files, they are gone forever <br>• [Create backups](https://gofoss.today/backups/)! <br>• Open a chat <br>• Click on the contact name in the top menu to view the chat settings <br>• Select `View recent media` <br>• Select the files you want to back up <br>• Click on the `Save` button to save the (unencrypted) media files outside on your desktop device <br>• Move the saved media files to your [backup devices](https://gofoss.today/backups/) |


=== "Linux (Ubuntu)"

    ??? tip "Show me the step-by-step guide for Linux (Ubuntu)"

        ### Installation & linking

        | Instructions | Description |
        | ------ | ------ |
        |Installation |• Confirm that Signal is installed and working on your phone <br>• Open the terminal with the `CTRL + ALT + T` shortcut, or click on the `Activities` button on the top left and search for `Terminal` <br>• Install the signing key: <br>`wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > signal-desktop-keyring.gpg` <br>`cat signal-desktop-keyring.gpg | sudo tee -a /usr/share/keyrings/signal-desktop-keyring.gpg > /dev/null` <br>• Add Signal to the repository list: <br> `echo "deb [arch=amd64] https://updates.signal.org/desktop/apt xenial main" | sudo tee -a /etc/apt/sources.list.d/signal-xenial.list` <br>• Update the package database and install Signal: <br>`sudo apt update && sudo apt install signal-desktop`|
        |Linking |• Open Signal on your phone <br>• Go to `Settings ‣ Linked devices` <br>• Tap the `+` button (Android) or `Link New Devices` (iPhone) <br>• Use your phone's Signal app to scan the QR code displayed on your Linux device <br>• Choose a name for the linked device <br>• Select `Finish` |

        ### One-to-one chats, voice or video calls

        | Instructions | Description |
        | ------ | ------ |
        |Start one-to-one conversation |• Open Signal <br>• Click on the `Compose` button <br>• Select a contact or enter a number <br>• Start a new conversation, or select an existing one <br>• Write your message <br>• Click `ENTER` to send your message |
        |Disappearing messages | • Signal allows to set a timer after which messages disappear from your devices <br>• Open a chat <br>• Tap your contact's name <br>• Activate the option `Disappearing messages` <br>• Define a deadline of up to one week <br>• Once activated, a timer icon will be visible next to each disappearing message |
        |Start one-to-one voice call |• Open Signal <br>• Click on the `Compose` button  <br>• Select a contact or enter a number <br>• Click on the `Phone` icon in the top menu to start a voice call |
        |Start one-to-one video call |• Open Signal <br>• Click on the `Compose` button  <br>• Select a contact or enter a number <br>• Click on the `Camera` icon in the top menu to start a video call <br>• Screen sharing: while in a video call, click on the `Start presenting` button and select your entire screen or a specific window |

        ### Group chats, voice or video calls

        | Instructions | Description |
        | ------ | ------ |
        |Start group chat |• Open Signal <br>• Click on the `Compose` button <br>• Select `New group` <br>• Add existing contacts or enter new numbers <br>• Tap the `Next` button <br>• Pick a group name. <br>• Tap the `Create` button |
        |Disappearing messages |• Signal allows to set a timer after which messages disappear from your devices <br>• Open a chat <br>• Tap the group name <br>• Activate the option `Disappearing messages` <br>• Define a deadline of up to one week <br>• Once activated, a timer icon will be visible next to each disappearing message. |
        |Start group voice or video call |• Open Signal <br>• Create a new or open an existing group chat <br>• Click on the `Camera` icon in the top menu <br>• Select `Start Call` or `Join Call` <br>• Screen sharing: while in a video call, click on the `Start presenting` button and select your entire screen or a specific window <br>• For a group voice call, disable the camera |

        ### Authentication, Signal-PIN & backups

        | Instructions | Description |
        | ------ | ------ |
        |Authentication |• Make sure you're speaking to the right people <br>• Signal can authenticate counterparts based on a fingerprint <br>• Open Signal  <br>• Open an ongoing chat <br>• Tap your contact's name <br>• Select `View Safety Number` <br>• Compare the appearing sequence of numbers with the one displayed on your contact's device <br>•If the numbers match, select `Mark as verified` <br>• After successful verification, a check mark will be displayed under your contact's name <br>• If the verification number changes, for example if you or your contact changes phones or reinstalls the app, a notification will be sent and the authentication process needs to be repeated |
        |Media backups |• Photos, videos, audio files and so on are encrypted and only stored on your devices <br>• If you loose your devices or mistakenly erase your files, they are gone forever <br>• [Create backups](https://gofoss.today/backups/)! <br>• Open a chat <br>• Click on the contact name in the top menu to view the chat settings <br>• Select `View recent media` <br>• Select the files you want to back up <br>• Click on the `Save` button to save the (unencrypted) media files outside on your desktop device <br>• Move the saved media files to your [backup devices](https://gofoss.today/backups/) |

<div style="margin-top:-20px">
</div>

??? warning "Signal is encrypted, but not anonymous"

    Signal puts several measures in place to achieve "zero knowledge" and collect as little meta data as possible: [Private-Contact-Discovery](https://signal.org/blog/private-contact-discovery/), [Sealed-Sender](https://signal.org/blog/sealed-sender/), [Signal-PIN](https://signal.org/blog/signal-pins/), [Secure Value Recovery](https://signal.org/blog/secure-value-recovery/) and so on. Mind that despite these privacy measures, Signal isn't anonymous since it requires access to your phone number. More detailed instructions below.



<br>

<center> <img src="../../assets/img/separator_element.svg" alt="Element" width="150px"></img> </center>

## Element

[Element](https://element.io/) is an [open source](https://github.com/vector-im/) messenger, voice and video calling app, available on all desktop and mobile devices. It's based on the [Matrix protocol](https://matrix.org/) and relies on a decentralized architecture. No phone number is required to register. Users can register with several independent public servers or even self-host their own server. Element also supports end-to-end encryption, which means that message contents can't be accessed by intermediaries.

=== "Android"

    ??? tip "Show me the step-by-step guide for Android"

        ### Installation

        | Instructions | Description |
        | ------ | ------ |
        |Install Element |Install Element from: <br>• [Google's Play Store](https://play.google.com/store/apps/details?id=im.vector.app&hl=en_US)<br>• [Aurora Store](https://auroraoss.com/)<br>• [F-Droid](https://f-droid.org/en/packages/im.vector.app/) |
        |Tracker free |The app [contains 0 trackers and requires 37 permissions](https://reports.exodus-privacy.eu.org/de/reports/im.vector.app/latest/). By comparison:<br>• TikTok: 16 trackers, 76 permissions<br>• Snapchat: 2 trackers, 44 permissions<br>• WhatsApp: 1 tracker, 57 permissions |
        |No GCM |Note that the F-Droid version of Element's app can handle push notifications without Google's Cloud Messaging (GCM). This comes in handy if you are looking to [de-google your phone](https://gofoss.today/intro-free-your-phone#google-ios-free-phones/). |

        ### Registration

        | Instructions | Description |
        | ------ | ------ |
        |Preliminary remarks |Each Matrix user has an unique identifier, which works similar to an email address and looks like this: `@username:servername.net`. You can create a new Matrix identifier with any provider of your choice, or even become your own provider by self-hosting Matrix. |
        |Open Element |Open the Element app and click on `Get started`. |
        |Select a server |Choose the default [matrix.org](https://matrix-client.matrix.org) server, or any public server of your choice. A selection can be found here: <br>• [hello-matrix.net](https://www.hello-matrix.net/public_servers.php) <br>• [anchel.nl](https://www.anchel.nl/matrix-publiclist/) <br>• [Tatsumoto](https://tatsumoto-ren.github.io/blog/list-of-matrix-servers.html) |
        |Sign up |Click on `Sign Up` and provide a username as well as a [strong, unique password](https://gofoss.today/passwords/). These credentials are required to log into Element. |
        |Privacy policy |Review the server's privacy policy and click on `Accept`. |
        |Email verification |Provide an existing or a [disposable email address](https://gofoss.today/cloud-providers/#other-cloud-services) to verify and recover your account. Open the link in the confirmation email to finalise the registration. |

        ### Security key

        | Instructions | Description |
        | ------ | ------ |
        |Preliminary remarks |**This step is only required after the first log in**. <br> Element uses end-to-end encryption. To make sure you always have access to your encrypted messages from any device, you'll need to generate a so-called Security Key. |
        |Set up |Go to `Settings ‣ Security & Privacy ‣ Secure Backup ‣ Set up on this device`. |
        |Security Phrase |• Select `Use a Security Phrase` and enter a [strong, unique Security Phrase](https://gofoss.today/passwords/) <br>• This Security Phrase will protect your Security Key <br>• **Make sure you don't use your Element account password!** <br>• Tap `Continue`, confirm your Security Phrase and tap `Continue` again. |
        |Security Key |Element will now generate a Security Key and back up an encrypted copy on the Matrix server. |
        |Storage |Make sure to store the Security Phrase as well as the Security Key somewhere safe, like a [password manager](https://gofoss.today/passwords/)! You should have three things stored in your password manager: <br>• your password, required to log into Element <br>• a Security Phrase, which protects your Security Key <br>• a Security Key, which is required to access your encrypted messages |
        |Recovery |If ever you are unable to read your messages on your Android device, it's because it doesn't have the right Security Key. In this case, go to `Settings ‣ Security & Privacy ‣ Cryptography Keys Management ‣ Encrypted Messages Recovery`, click on `Restore from backup` and provide your Security Phrase. |

        ### One-to-one chats, voice and video calls

        | Instructions | Description |
        | ------ | ------ |
        |Start a chat |• Open the Element app <br>• Select the `Direct Messages` tab <br>• Tap the `+` button <br>• Search for contacts using their username or email address <br>• Tap `CREATE` <br>• Start chatting |
        |Send a voice message |• Open the Element app <br>• Open an existing chat, or start a new one <br>• Tap `CREATE` <br>• The `Voice Message` button sits next to the composer <br>• Press & hold the `Voice Message` button to record your message, and release to send (or slide left to cancel) <br>• Press & drag the `Voice Message` button up to record longer voice messages. You can review them prior to pressing the `Send` button (or pressing the `Trash` button to delete the voice message) |
        |Start a voice call |• Open the Element app <br>• Select the `Direct Messages` tab <br>• Tap the `+` button <br>• Search for contacts using their username or email address <br>• Tap `CREATE` <br>• Tap the `phone icon` in the top menu to launch a voice call |
        |Start a video call |• Open the Element app <br>• Select the `Direct Messages` tab <br>• Tap the `+` button <br>• Search for contacts using their username or email address <br>• Tap `CREATE` <br>• Tap the `camera icon` in the top menu to launch a video call |

        ### Group chats, voice and video calls

        | Instructions | Description |
        | ------ | ------ |
        |Join a public room |• Open the Element app <br>• Select the `Rooms` tab <br>• Tap the `#+` button <br>• Browse through the room directory <br>• Or tap the magnifying glass to search for a room corresponding to your needs <br>• You can also select a different server to find additional rooms <br>• Tap `JOIN` to connect with a community |
        |Create a new room |• Open the Element app <br>• Select the `Rooms` tab <br>• Tap the `#+` button <br>• Tap `CREATE NEW ROOM` <br>• Provide a room name <br>• Choose whether the room is private or public (in which case an addresses needs to be provided) <br>• Enable or disable encryption <br>• Optionally, block users from other Matrix servers <br>• Tap `CREATE` <br>• Tap the `Add people` button and invite others to join using their username or email address <br>• Optionally, very granular room settings can be defined, such as URL previews, room access, message history, user permissions, and so on|
        |Start a group voice call |• Open the Element app <br>• Select the `Rooms` tab <br>• Select an existing room, or create a new one <br>• Tap the `phone icon` in the top menu to launch a group voice call |
        |Start a group video call |• Open the Element app <br>• Select the `Rooms` tab <br>• Select an existing room, or create a new one <br>• Tap the `camera icon` in the top menu to launch a group voice call |

        ### Notifications

        | Instructions | Description |
        | ------ | ------ |
        |Global notification settings |• Open the Element app <br>• Go to `Settings ‣ Notifications` and configure global notifications to your liking <br>• Enable/disable notifications for the account or the session <br>• Enable/disable notifications for messages containing your name, for one-to-one or group chats, for invitations and more <br>• Configure LED color, vibration and sounds |
        |Direct Messages notification settings |• Open the Element app <br>• Select the `Direct Messages` tab <br>• Long-press on an existing chat session <br>• Enable/disable notifications for all messages or messages mentioning your name |
        |Room notification settings |• Open the Element app <br>• Select the `Room` tab <br>• Long-press on an existing room <br>• Enable/disable notifications for all messages or messages mentioning your name |

        ### Authentication & verification

        | Instructions | Description |
        | ------ | ------ |
        |Preliminary remarks |In certain circumstances, Element might want to verify your identity. For instance if you log in with a new device or open a new session. Or if you log in with multiple devices at the same time. In such cases, a notification will appear: `Verify this login` or `New login. Was this you?`. Several options exist to prove your identity. |
        |Security Phrase or Key |The most straight forward solution is to provide either your Security Phrase, or your Security Key. |
        |Other device |Alternatively, you can use another device which is already logged in. Click on the verification request appearing in the other device. Now either scan the QR code, or compare an Emoji code. |
        |Successful verification |If you successfully verify your identity, you'll have full access to your messages and will appear as trusted to others. |
        |Active sessions |You can check all active and verified sessions by going to `Settings ‣ Security & Privacy ‣ Active Sessions`. |
        |User verification |For additional security, you can also verify the identity of other users you are chatting with. Open the chat, go to `Settings ‣ People`, select the relevant user and click on `Verify ‣ Start verification`. Wait until the other user accepts the verification request. Now either scan the QR codes with the other user's device, or compare an Emoji code. |



=== "iOS"

    ??? tip "Show me the step-by-step guide for iOS"

        ### Installation

        Install Element from the [App Store](https://apps.apple.com/app/vector/id1083446067/).

        ### Registration

        | Instructions | Description |
        | ------ | ------ |
        |Preliminary remarks |Each Matrix user has an unique identifier, which works similar to an email address and looks like this: `@username:servername.net`. You can create a new Matrix identifier with any provider of your choice, or even become your own provider by self-hosting Matrix. |
        |Open Element |Open the Element app and click on `Get started`. |
        |Select a server |Choose the default [matrix.org](https://matrix-client.matrix.org) server, or any public server of your choice. A selection can be found here: <br>• [hello-matrix.net](https://www.hello-matrix.net/public_servers.php) <br>• [anchel.nl](https://www.anchel.nl/matrix-publiclist/) <br>• [Tatsumoto](https://tatsumoto-ren.github.io/blog/list-of-matrix-servers.html) |
        |Sign up |Click on `Sign Up` and provide a username as well as a [strong, unique password](https://gofoss.today/passwords/). These credentials are required to log into Element. |
        |Privacy policy |Review the server's privacy policy and click on `Accept`. |
        |Email verification |Provide an existing or a [disposable email address](https://gofoss.today/cloud-providers/#other-cloud-services) to verify and recover your account. Open the link in the confirmation email to finalise the registration. |

        ### Security key

        | Instructions | Description |
        | ------ | ------ |
        |Preliminary remarks |**This step is only required after the first log in**. <br> Element uses end-to-end encryption. To make sure you always have access to your encrypted messages from any device, you'll need to generate a so-called Security Key. |
        |Set up |Go to `Settings ‣ Security & Privacy ‣ Secure Backup ‣ Set up on this device`. |
        |Security Phrase |• Select `Use a Security Phrase` and enter a [strong, unique Security Phrase](https://gofoss.today/passwords/) <br>• This Security Phrase will protect your Security Key <br>• **Make sure you don't use your Element account password!** <br>• Tap `Continue`, confirm your Security Phrase and tap `Continue` again. |
        |Security Key |Element will now generate a Security Key and back up an encrypted copy on the Matrix server. |
        |Storage |Make sure to store the Security Phrase as well as the Security Key somewhere safe, like a [password manager](https://gofoss.today/passwords/)! You should have three things stored in your password manager: <br>• your password, required to log into Element <br>• a Security Phrase, which protects your Security Key <br>• a Security Key, which is required to access your encrypted messages |
        |Recovery |If ever you are unable to read your messages on your iOS device, it's because it doesn't have the right Security Key. In this case, go to `Settings ‣ Security & Privacy ‣ Cryptography Keys Management ‣ Encrypted Messages Recovery`, click on `Restore from backup` and provide your Security Phrase. |

        ### One-to-one chats, voice and video calls

        | Instructions | Description |
        | ------ | ------ |
        |Start a chat |• Open the Element app <br>• Select the `Direct Messages` tab <br>• Tap the `+` button <br>• Search for contacts using their username or email address <br>• Tap `CREATE` <br>• Start chatting |
        |Send a voice message |• Open the Element app <br>• Open an existing chat, or start a new one <br>• The `Voice Message` button sits next to the composer <br>• Press & hold the `Voice Message` button to record your message, and release to send (or slide left to cancel) <br>• Press & drag the `Voice Message` button up to record longer voice messages. You can review them prior to pressing the `Send` button (or pressing the `Trash` button to delete the voice message) |
        |Start a voice call |• Open the Element app <br>• Select the `Direct Messages` tab <br>• Tap the `+` button <br>• Search for contacts using their username or email address <br>• Tap `CREATE` <br>• Tap the `phone icon` in the top menu to launch a voice call |
        |Start a video call |• Open the Element app <br>• Select the `Direct Messages` tab <br>• Tap the `+` button <br>• Search for contacts using their username or email address <br>• Tap `CREATE` <br>• Tap the `camera icon` in the top menu to launch a video call |

        ### Group chats, voice and video calls

        | Instructions | Description |
        | ------ | ------ |
        |Join a public room |• Open the Element app <br>• Select the `Rooms` tab <br>• Tap the `#+` button <br>• Browse through the room directory <br>• Or tap the magnifying glass to search for a room corresponding to your needs <br>• You can also select a different server to find additional rooms <br>• Tap `JOIN` to connect with a community |
        |Create a new room |• Open the Element app <br>• Select the `Rooms` tab <br>• Tap the `#+` button <br>• Tap `CREATE NEW ROOM` <br>• Provide a room name <br>• Choose whether the room is private or public (in which case an addresses needs to be provided) <br>• Enable or disable encryption <br>• Optionally, block users from other Matrix servers <br>• Tap `CREATE` <br>• Tap the `Add people` button and invite others to join using their username or email address <br>• Optionally, very granular room settings can be defined, such as URL previews, room access, message history, user permissions, and so on|
        |Start a group voice call |• Open the Element app <br>• Select the `Rooms` tab <br>• Select an existing room, or create a new one <br>• Tap the `phone icon` in the top menu to launch a group voice call |
        |Start a group video call |• Open the Element app <br>• Select the `Rooms` tab <br>• Select an existing room, or create a new one <br>• Tap the `camera icon` in the top menu to launch a group voice call |

        ### Notifications

        | Instructions | Description |
        | ------ | ------ |
        |Global notification settings |• Open the Element app <br>• Go to `Settings ‣ Notifications` and configure global notifications to your liking <br>• Enable/disable notifications for the account or the session <br>• Enable/disable notifications for messages containing your name, for one-to-one or group chats, for invitations and more <br>• Configure LED color, vibration and sounds |
        |Direct Messages notification settings |• Open the Element app <br>• Select the `Direct Messages` tab <br>• Long-press on an existing chat session <br>• Enable/disable notifications for all messages or messages mentioning your name |
        |Room notification settings |• Open the Element app <br>• Select the `Room` tab <br>• Long-press on an existing room <br>• Enable/disable notifications for all messages or messages mentioning your name |

        ### Authentication & verification

        | Instructions | Description |
        | ------ | ------ |
        |Preliminary remarks |In certain circumstances, Element might want to verify your identity. For instance if you log in with a new device or open a new session. Or if you log in with multiple devices at the same time. In such cases, a notification will appear: `Verify this login` or `New login. Was this you?`. Several options exist to prove your identity. |
        |Security Phrase or Key |The most straight forward solution is to provide either your Security Phrase, or your Security Key. |
        |Other device |Alternatively, you can use another device which is already logged in. Click on the verification request appearing in the other device. Now either scan the QR code, or compare the Emoji code. |
        |Successful verification |If you successfully verify your identity, you'll have full access to your messages and will appear as trusted to others. |
        |Active sessions |You can check all active and verified sessions by going to `Settings ‣ Security & Privacy ‣ Active Sessions`. |
        |User verification |For additional security, you can also verify the identity of other users you are chatting with. Open the chat, go to `Settings ‣ People`, select the relevant user and click on `Verify ‣ Start verification`. Wait until the other user accepts the verification request. Now either scan the QR codes with the other user's device, or compare an Emoji code. |



=== "Windows"

    ??? tip "Show me the step-by-step guide for Windows"

        ### Installation

        Download and run the [Element installer for Windows](https://packages.riot.im/desktop/install/win32/x64/Element%20Setup.exe).


        ### Registration

        | Instructions | Description |
        | ------ | ------ |
        |Preliminary remarks |Each Matrix user has an unique identifier, which works similar to an email address and looks like this: `@username:servername.net`. You can create a new Matrix identifier with any provider of your choice, or even become your own provider by self-hosting Matrix. |
        |Open Element |Open the Element app and click on `Create account`. |
        |Select a server |Choose the default [matrix.org](https://matrix-client.matrix.org) server, or any public server of your choice. A selection can be found here: <br>• [hello-matrix.net](https://www.hello-matrix.net/public_servers.php) <br>• [anchel.nl](https://www.anchel.nl/matrix-publiclist/) <br>• [Tatsumoto](https://tatsumoto-ren.github.io/blog/list-of-matrix-servers.html) |
        |Sign up |Provide a username as well as a [strong, unique password](https://gofoss.today/passwords/). These credentials are required to log into Element. |
        |Email verification |Provide an existing or a [disposable email address](https://gofoss.today/cloud-providers/#other-cloud-services) to verify and recover your account. Click on `Register` and accept the Terms and Conditions. Open the link in the confirmation email to finalise the registration. |

        ### Security key

        | Instructions | Description |
        | ------ | ------ |
        |Preliminary remarks |**This step is only required after the first log in**. <br> Element uses end-to-end encryption. To make sure you always have access to your encrypted messages from any device, you'll need to generate a so-called Security Key. |
        |Set up |After signing in, go to `User menu ‣ Security & Privacy ‣ Secure Backup ‣ Set up`. |
        |Security Phrase |• Select `Use a Security Phrase` and enter a [strong, unique Security Phrase](https://gofoss.today/passwords/) <br>• This Security Phrase will protect your Security Key <br>• **Make sure you don't use your Element account password!** <br>• Tap `Continue`, confirm your Security Phrase and tap `Continue` again. |
        |Security Key |Element will now generate a Security Key and back up an encrypted copy on the Matrix server. |
        |Storage |Make sure to store the Security Phrase as well as the Security Key somewhere safe, like a [password manager](https://gofoss.today/passwords/)! You should have three things stored in your password manager: <br>• your password, required to log into Element <br>• a Security Phrase, which protects your Security Key <br>• a Security Key, which is required to access your encrypted messages |
        |Recovery |If ever you are unable to read your messages on your Windows device, it's because it doesn't have the right Security Key. In this case, go to `User menu ‣ Security & Privacy ‣ Encryption ‣ Secure Backup`, click on `Restore from Backup` and provide your Security Phrase. |

        ### One-to-one chats, voice and video calls

        | Instructions | Description |
        | ------ | ------ |
        |Start a chat |• Open the Element app <br>• Click on the `+` button next to the `People` section <br>• Search for contacts using their username or email address <br>• Click on `Go` <br>• Start chatting |
        |Send a voice message |• Open the Element app <br>• Open an existing chat, or start a new one <br>• The `Voice Message` button sits next to the composer <br>• Click on it to start recording your message <br>• When you're ready, click on the `Send` button <br>• Or click on the `Trash` button to delete the voice message |
        |Start a voice call |• Open the Element app <br>• Click on the `+` button next to the `People` section <br>• Search for contacts using their username or email address <br>• Click on `Go` <br>• Click on the `phone icon` in the top menu to launch a voice call|
        |Start a video call |• Open the Element app <br>• Click on the `+` button next to the `People` section <br>• Search for contacts using their username or email address <br>• Click on `Go` <br>• Click on the `camera icon` in the top menu to launch a video call |

        ### Group chats, voice and video calls

        | Instructions | Description |
        | ------ | ------ |
        |Join a public room |• Open the Element app <br>• Click on the `+` button next to the `Rooms` section <br>•Select `Explore public rooms` <br>• Browse through the room directory <br>• Or search for a room corresponding to your needs <br>• You can also select a different server to find additional rooms <br>• Click on `Join` to connect with a community |
        |Create a new room |• Open the Element app <br>• Click on the `+` button next to the `Rooms` section <br>• Select `Create new room` <br>• Provide a room name  <br>• Choose whether the room is private or public (in which case an addresses needs to be provided) <br>• Enable or disable encryption <br>• Optionally, block users from other Matrix servers <br>• Click on `Create Room` <br>• Click on `Invite to this room` and invite others to join using their username or email address <br>• Optionally, very granular room settings can be defined, such as URL previews, room access, message history, user permissions, and so on|
        |Start a group voice call |• Open the Element app <br>• Select an existing room, or create a new one <br>• Tap the `phone icon` in the top menu to launch a group voice call |
        |Start a group video call |• Open the Element app <br>• Select an existing room, or create a new one <br>• Tap the `camera icon` in the top menu to launch a group voice call |

        ### Notifications

        | Instructions | Description |
        | ------ | ------ |
        |Global notification settings |• Open the Element app <br>• Go to `User menu ‣ Notification settings` and configure global notifications to your liking <br>• Enable/disable notifications for the account <br>• Enable/disable desktop notifications for the session <br>• Enable/disable email notifications <br>• Enable/disable notifications for one-to-one or group chats <br>• Enable/disable notifications for messages containing your name or keywords <br>• Enable/disable messages for invitations and more |
        |Direct Messages notification settings |• Open the Element app <br>• Hover over a chat session in the `People` section and click on the `bell` icon to access notification options <br>• Enable/disable notifications for all messages or messages mentioning your name and keywords |
        |Room notification settings |• Open the Element app <br>• Hover over a room in the `Rooms` section and click on the `bell` icon to access notification options <br>• Enable/disable notifications for all messages or messages mentioning your name and keywords |

        ### Authentication & verification

        | Instructions | Description |
        | ------ | ------ |
        |Preliminary remarks |In certain circumstances, Element might want to verify your identity. For instance if you log in with a new device or open a new session. Or if you log in with multiple devices at the same time. In such cases, a notification will appear: `Verify this login` or `New login. Was this you?`. Several options exist to prove your identity. |
        |Security Phrase or Key |The most straight forward solution is to provide either your Security Phrase, or your Security Key. |
        |Other device |Alternatively, you can use another device which is already logged in. Click on the verification request appearing in the other device. Now either scan the QR code, or compare the Emoji code. |
        |Successful verification |If you successfully verify your identity, you'll have full access to your messages and will appear as trusted to others. |
        |Active sessions |You can check all active and verified sessions by going to `User menu ‣ Security & Privacy`. |
        |User verification |For additional security, you can also verify the identity of other users you are chatting with. Open the chat, go to `Room info ‣ Person`, select the relevant user and click on `Verify ‣ Start verification`. Wait until the other user accepts the verification request. Now either scan the QR codes with the other user's device, or compare an Emoji code. |



=== "macOS"

    ??? tip "Show me the step-by-step guide for macOS"

        ### Installation

        Download the [Element disk image](https://packages.riot.im/desktop/install/macos/Element.dmg), open it and drag the Element icon on top of the Application folder. For easy access, open the Applications folder and drag the Element icon to your dock.

        ### Registration

        | Instructions | Description |
        | ------ | ------ |
        |Preliminary remarks |Each Matrix user has an unique identifier, which works similar to an email address and looks like this: `@username:servername.net`. You can create a new Matrix identifier with any provider of your choice, or even become your own provider by self-hosting Matrix. |
        |Open Element |Open the Element app and click on `Create account`. |
        |Select a server |Choose the default [matrix.org](https://matrix-client.matrix.org) server, or any public server of your choice. A selection can be found here: <br>• [hello-matrix.net](https://www.hello-matrix.net/public_servers.php) <br>• [anchel.nl](https://www.anchel.nl/matrix-publiclist/) <br>• [Tatsumoto](https://tatsumoto-ren.github.io/blog/list-of-matrix-servers.html) |
        |Sign up |Provide a username as well as a [strong, unique password](https://gofoss.today/passwords/). These credentials are required to log into Element. |
        |Email verification |Provide an existing or a [disposable email address](https://gofoss.today/cloud-providers/#other-cloud-services) to verify and recover your account. Click on `Register` and accept the Terms and Conditions. Open the link in the confirmation email to finalise the registration. |

        ### Security key

        | Instructions | Description |
        | ------ | ------ |
        |Preliminary remarks |**This step is only required after the first log in**. <br> Element uses end-to-end encryption. To make sure you always have access to your encrypted messages from any device, you'll need to generate a so-called Security Key. |
        |Set up |After signing in, go to `User menu ‣ Security & Privacy ‣ Secure Backup ‣ Set up`. |
        |Security Phrase |• Select `Use a Security Phrase` and enter a [strong, unique Security Phrase](https://gofoss.today/passwords/) <br>• This Security Phrase will protect your Security Key <br>• **Make sure you don't use your Element account password!** <br>• Tap `Continue`, confirm your Security Phrase and tap `Continue` again. |
        |Security Key |Element will now generate a Security Key and back up an encrypted copy on the Matrix server. |
        |Storage |Make sure to store the Security Phrase as well as the Security Key somewhere safe, like a [password manager](https://gofoss.today/passwords/)! You should have three things stored in your password manager: <br>• your password, required to log into Element <br>• a Security Phrase, which protects your Security Key <br>• a Security Key, which is required to access your encrypted messages |
        |Recovery |If ever you are unable to read your messages on your macOS device, it's because it doesn't have the right Security Key. In this case, go to `User menu ‣ Security & Privacy ‣ Encryption ‣ Secure Backup`, click on `Restore from Backup` and provide your Security Phrase. |

        ### One-to-one chats, voice and video calls

        | Instructions | Description |
        | ------ | ------ |
        |Start a chat |• Open the Element app <br>• Click on the `+` button next to the `People` section <br>• Search for contacts using their username or email address <br>• Click on `Go` <br>• Start chatting |
        |Send a voice message |• Open the Element app <br>• Open an existing chat, or start a new one <br>• The `Voice Message` button sits next to the composer <br>• Click on it to start recording your message <br>• When you're ready, click on the `Send` button <br>• Or click on the `Trash` button to delete the voice message |
        |Start a voice call |• Open the Element app <br>• Click on the `+` button next to the `People` section <br>• Search for contacts using their username or email address <br>• Click on `Go` <br>• Click on the `phone icon` in the top menu to launch a voice call|
        |Start a video call |• Open the Element app <br>• Click on the `+` button next to the `People` section <br>• Search for contacts using their username or email address <br>• Click on `Go` <br>• Click on the `camera icon` in the top menu to launch a video call |

        ### Group chats, voice and video calls

        | Instructions | Description |
        | ------ | ------ |
        |Join a public room |• Open the Element app <br>• Click on the `+` button next to the `Rooms` section <br>•Select `Explore public rooms` <br>• Browse through the room directory <br>• Or search for a room corresponding to your needs <br>• You can also select a different server to find additional rooms <br>• Click on `Join` to connect with a community |
        |Create a new room |• Open the Element app <br>• Click on the `+` button next to the `Rooms` section <br>• Select `Create new room` <br>• Provide a room name  <br>• Choose whether the room is private or public (in which case an addresses needs to be provided) <br>• Enable or disable encryption <br>• Optionally, block users from other Matrix servers <br>• Click on `Create Room` <br>• Click on `Invite to this room` and invite others to join using their username or email address <br>• Optionally, very granular room settings can be defined, such as URL previews, room access, message history, user permissions, and so on|
        |Start a group voice call |• Open the Element app <br>• Select an existing room, or create a new one <br>• Tap the `phone icon` in the top menu to launch a group voice call |
        |Start a group video call |• Open the Element app <br>• Select an existing room, or create a new one <br>• Tap the `camera icon` in the top menu to launch a group voice call |

        ### Notifications

        | Instructions | Description |
        | ------ | ------ |
        |Global notification settings |• Open the Element app <br>• Go to `User menu ‣ Notification settings` and configure global notifications to your liking <br>• Enable/disable notifications for the account <br>• Enable/disable desktop notifications for the session <br>• Enable/disable email notifications <br>• Enable/disable notifications for one-to-one or group chats <br>• Enable/disable notifications for messages containing your name or keywords <br>• Enable/disable messages for invitations and more |
        |Direct Messages notification settings |• Open the Element app <br>• Hover over a chat session in the `People` section and click on the `bell` icon to access notification options <br>• Enable/disable notifications for all messages or messages mentioning your name and keywords |
        |Room notification settings |• Open the Element app <br>• Hover over a room in the `Rooms` section and click on the `bell` icon to access notification options <br>• Enable/disable notifications for all messages or messages mentioning your name and keywords |

        ### Authentication & verification

        | Instructions | Description |
        | ------ | ------ |
        |Preliminary remarks |In certain circumstances, Element might want to verify your identity. For instance if you log in with a new device or open a new session. Or if you log in with multiple devices at the same time. In such cases, a notification will appear: `Verify this login` or `New login. Was this you?`. Several options exist to prove your identity. |
        |Security Phrase or Key |The most straight forward solution is to provide either your Security Phrase, or your Security Key. |
        |Other device |Alternatively, you can use another device which is already logged in. Click on the verification request appearing in the other device. Now either scan the QR code, or compare the Emoji code. |
        |Successful verification |If you successfully verify your identity, you'll have full access to your messages and will appear as trusted to others. |
        |Active sessions |You can check all active and verified sessions by going to `User menu ‣ Security & Privacy`. |
        |User verification |For additional security, you can also verify the identity of other users you are chatting with. Open the chat, go to `Room info ‣ Person`, select the relevant user and click on `Verify ‣ Start verification`. Wait until the other user accepts the verification request. Now either scan the QR codes with the other user's device, or compare an Emoji code. |



=== "Linux (Ubuntu)"

    ??? tip "Show me the step-by-step guide for Linux (Ubuntu)"

        ### Installation

        | Instructions | Description |
        | ------ | ------ |
        |Open the terminal |Use the `Ctrl+Alt+T` shortcut or click on the `Activities` button on the top left and search for `Terminal` |
        |Enable secure access to repositories |`sudo apt install -y wget apt-transport-https` |
        |Add the signing keys |`sudo wget -O /usr/share/keyrings/riot-im-archive-keyring.gpg https://packages.riot.im/debian/riot-im-archive-keyring.gpg` |
        |Add the repository to the apt source list |`echo "deb [signed-by=/usr/share/keyrings/riot-im-archive-keyring.gpg] https://packages.riot.im/debian/ default main" | sudo tee /etc/apt/sources.list.d/riot-im.list` |
        |Update the local apt cache |`sudo apt update` |
        |Install the Element client |`sudo apt install element-desktop`|

        ### Registration

        | Instructions | Description |
        | ------ | ------ |
        |Preliminary remarks |Each Matrix user has an unique identifier, which works similar to an email address and looks like this: `@username:servername.net`. You can create a new Matrix identifier with any provider of your choice, or even become your own provider by self-hosting Matrix. |
        |Open Element |Open the Element app and click on `Create account`. |
        |Select a server |Choose the default [matrix.org](https://matrix-client.matrix.org) server, or any public server of your choice. A selection can be found here: <br>• [hello-matrix.net](https://www.hello-matrix.net/public_servers.php) <br>• [anchel.nl](https://www.anchel.nl/matrix-publiclist/) <br>• [Tatsumoto](https://tatsumoto-ren.github.io/blog/list-of-matrix-servers.html) |
        |Sign up |Provide a username as well as a [strong, unique password](https://gofoss.today/passwords/). These credentials are required to log into Element. |
        |Email verification |Provide an existing or a [disposable email address](https://gofoss.today/cloud-providers/#other-cloud-services) to verify and recover your account. Click on `Register` and accept the Terms and Conditions. Open the link in the confirmation email to finalise the registration. |

        ### Security key

        | Instructions | Description |
        | ------ | ------ |
        |Preliminary remarks |**This step is only required after the first log in**. <br> Element uses end-to-end encryption. To make sure you always have access to your encrypted messages from any device, you'll need to generate a so-called Security Key. |
        |Set up |After signing in, go to `User menu ‣ Security & Privacy ‣ Secure Backup ‣ Set up`. |
        |Security Phrase |• Select `Use a Security Phrase` and enter a [strong, unique Security Phrase](https://gofoss.today/passwords/) <br>• This Security Phrase will protect your Security Key <br>• **Make sure you don't use your Element account password!** <br>• Tap `Continue`, confirm your Security Phrase and tap `Continue` again. |
        |Security Key |Element will now generate a Security Key and back up an encrypted copy on the Matrix server. |
        |Storage |Make sure to store the Security Phrase as well as the Security Key somewhere safe, like a [password manager](https://gofoss.today/passwords/)! You should have three things stored in your password manager: <br>• your password, required to log into Element <br>• a Security Phrase, which protects your Security Key <br>• a Security Key, which is required to access your encrypted messages |
        |Recovery |If ever you are unable to read your messages on your Linux/Ubuntu device, it's because it doesn't have the right Security Key. In this case, go to `User menu ‣ Security & Privacy ‣ Encryption ‣ Secure Backup`, click on `Restore from Backup` and provide your Security Phrase. |

        ### One-to-one chats, voice and video calls

        | Instructions | Description |
        | ------ | ------ |
        |Start a chat |• Open the Element app <br>• Click on the `+` button next to the `People` section <br>• Search for contacts using their username or email address <br>• Click on `Go` <br>• Start chatting |
        |Send a voice message |• Open the Element app <br>• Open an existing chat, or start a new one <br>• The `Voice Message` button sits next to the composer <br>• Click on it to start recording your message <br>• When you're ready, click on the `Send` button <br>• Or click on the `Trash` button to delete the voice message |
        |Start a voice call |• Open the Element app <br>• Click on the `+` button next to the `People` section <br>• Search for contacts using their username or email address <br>• Click on `Go` <br>• Click on the `phone icon` in the top menu to launch a voice call|
        |Start a video call |• Open the Element app <br>• Click on the `+` button next to the `People` section <br>• Search for contacts using their username or email address <br>• Click on `Go` <br>• Click on the `camera icon` in the top menu to launch a video call |

        ### Group chats, voice and video calls

        | Instructions | Description |
        | ------ | ------ |
        |Join a public room |• Open the Element app <br>• Click on the `+` button next to the `Rooms` section <br>•Select `Explore public rooms` <br>• Browse through the room directory <br>• Or search for a room corresponding to your needs <br>• You can also select a different server to find additional rooms <br>• Click on `Join` to connect with a community |
        |Create a new room |• Open the Element app <br>• Click on the `+` button next to the `Rooms` section <br>• Select `Create new room` <br>• Provide a room name  <br>• Choose whether the room is private or public (in which case an addresses needs to be provided) <br>• Enable or disable encryption <br>• Optionally, block users from other Matrix servers <br>• Click on `Create Room` <br>• Click on `Invite to this room` and invite others to join using their username or email address <br>• Optionally, very granular room settings can be defined, such as URL previews, room access, message history, user permissions, and so on|
        |Start a group voice call |• Open the Element app <br>• Select an existing room, or create a new one <br>• Tap the `phone icon` in the top menu to launch a group voice call |
        |Start a group video call |• Open the Element app <br>• Select an existing room, or create a new one <br>• Tap the `camera icon` in the top menu to launch a group voice call |

        ### Notifications

        | Instructions | Description |
        | ------ | ------ |
        |Global notification settings |• Open the Element app <br>• Go to `User menu ‣ Notification settings` and configure global notifications to your liking <br>• Enable/disable notifications for the account <br>• Enable/disable desktop notifications for the session <br>• Enable/disable email notifications <br>• Enable/disable notifications for one-to-one or group chats <br>• Enable/disable notifications for messages containing your name or keywords <br>• Enable/disable messages for invitations and more |
        |Direct Messages notification settings |• Open the Element app <br>• Hover over a chat session in the `People` section and click on the `bell` icon to access notification options <br>• Enable/disable notifications for all messages or messages mentioning your name and keywords |
        |Room notification settings |• Open the Element app <br>• Hover over a room in the `Rooms` section and click on the `bell` icon to access notification options <br>• Enable/disable notifications for all messages or messages mentioning your name and keywords |

        ### Authentication & verification

        | Instructions | Description |
        | ------ | ------ |
        |Preliminary remarks |In certain circumstances, Element might want to verify your identity. For instance if you log in with a new device or open a new session. Or if you log in with multiple devices at the same time. In such cases, a notification will appear: `Verify this login` or `New login. Was this you?`. Several options exist to prove your identity. |
        |Security Phrase or Key |The most straight forward solution is to provide either your Security Phrase, or your Security Key. |
        |Other device |Alternatively, you can use another device which is already logged in. Click on the verification request appearing in the other device. Now either scan the QR code, or compare the Emoji code. |
        |Successful verification |If you successfully verify your identity, you'll have full access to your messages and will appear as trusted to others. |
        |Active sessions |You can check all active and verified sessions by going to `User menu ‣ Security & Privacy`. |
        |User verification |For additional security, you can also verify the identity of other users you are chatting with. Open the chat, go to `Room info ‣ Person`, select the relevant user and click on `Verify ‣ Start verification`. Wait until the other user accepts the verification request. Now either scan the QR codes with the other user's device, or compare an Emoji code. |



=== "Browser"

    ??? tip "Show me the step-by-step guide for Android"

        ### Registration

        | Instructions | Description |
        | ------ | ------ |
        |Preliminary remarks |Each Matrix user has an unique identifier, which works similar to an email address and looks like this: `@username:servername.net`. You can create a new Matrix identifier with any provider of your choice, or even become your own provider by self-hosting Matrix. |
        |Open Element |Open your browser and navigate to [https://app.element.io](https://app.element.io/). When prompted, allow access to persistent storage. This enables Element to store keys, messages and so on in the browser session. Then, click on `Create Account`. |
        |Select a server |Choose the default [matrix.org](https://matrix-client.matrix.org) server, or any public server of your choice. A selection can be found here: <br>• [hello-matrix.net](https://www.hello-matrix.net/public_servers.php) <br>• [anchel.nl](https://www.anchel.nl/matrix-publiclist/) <br>• [Tatsumoto](https://tatsumoto-ren.github.io/blog/list-of-matrix-servers.html) |
        |Sign up |Also provide a username as well as a [strong, unique password](https://gofoss.today/passwords/). These credentials are required to log into Element. |
        |Email verification |Provide an existing or a [disposable email address](https://gofoss.today/cloud-providers/#other-cloud-services) to verify and recover your account. Click on `Register`, review the server's privacy policy and click on `Accept`. Open the link in the confirmation email to finalise the registration.  |

        ### Security key

        | Instructions | Description |
        | ------ | ------ |
        |Preliminary remarks |**This step is only required after the first log in**. <br> Element uses end-to-end encryption. To make sure you always have access to your encrypted messages from any device, you'll need to generate a so-called Security Key. |
        |Set up |After signing in, go to `Settings ‣ Security & Privacy ‣ Secure Backup ‣ Set up`. |
        |Security Phrase |• Select `Enter a Security Phrase` and enter a [strong, unique Security Phrase](https://gofoss.today/passwords/) <br>• This Security Phrase will protect your Security Key <br>• **Make sure you don't use your Element account password!** <br>• Tap `Continue`, confirm your Security Phrase and tap `Continue` again. |
        |Security Key |Element will now generate a Security Key and back up an encrypted copy on the Matrix server. |
        |Storage |Make sure to store the Security Phrase as well as the Security Key somewhere safe, like a [password manager](https://gofoss.today/passwords/)! You should have three things stored in your password manager: <br>• your password, required to log into Element <br>• a Security Phrase, which protects your Security Key <br>• a Security Key, which is required to access your encrypted messages |
        |Recovery |If ever you are unable to read your messages in your browser, it's because it doesn't have the right Security Key. In this case, go to `Settings ‣ Security & Privacy ‣ Encryption ‣ Secure Backup`, click on `Restore from Backup` and provide your Security Phrase. |

        ### One-to-one chats, voice and video calls

        | Instructions | Description |
        | ------ | ------ |
        |Start a chat |• Open your browser and sign in to [https://app.element.io](https://app.element.io/) <br>• Click on the `+` button next to the `People` section <br>• Search for contacts using their username or email address <br>• Click on `GO` <br>• Start chatting |
        |Send a voice message |• Open the Element app <br>• Open an existing chat, or start a new one <br>• The `Voice Message` button sits next to the composer <br>• Click on it to start recording your message <br>• When you're ready, click on the `Send` button <br>• Or click on the `Trash` button to delete the voice message |
        |Start a voice call |• Open your browser and sign in to [https://app.element.io](https://app.element.io/) <br>• Click on the `+` button next to the `People` section <br>• Search for contacts using their username or email address <br>• Click on `GO` <br>• Click on the `phone icon` in the top menu to launch a voice call|
        |Start a video call |• Open your browser and sign in to [https://app.element.io](https://app.element.io/) <br>• Click on the `+` button next to the `People` section <br>• Search for contacts using their username or email address <br>• Click on `GO` <br>• Click on the `camera icon` in the top menu to launch a video call |

        ### Group chats, voice and video calls

        | Instructions | Description |
        | ------ | ------ |
        |Join a public room |• Open your browser and sign in to [https://app.element.io](https://app.element.io/) <br>• Click on the `+` button next to the `Rooms` section <br>• Select `Explore public rooms` <br>• Browse through the room directory <br>• Or search for a room corresponding to your needs <br>• You can also select a different server to find additional rooms <br>• Click on `JOIN` to connect with a community |
        |Create a new room |• Open your browser and sign in to [https://app.element.io](https://app.element.io/)  <br>• Click on the `+` button next to the `Rooms` section <br>• Select `Create new room` <br>• Provide a room name  <br>• Choose whether the room is private or public (in which case an addresses needs to be provided) <br>• Enable or disable encryption <br>• Optionally, block users from other Matrix servers <br>• Click on `Create Room` <br>• Click on `Invite to this room` and invite others to join using their username or email address <br>• Optionally, very granular room settings can be defined, such as URL previews, room access, message history, user permissions, and so on|
        |Start a group voice call |• Open the Element app <br>• Select an existing room, or create a new one <br>• Tap the `phone icon` in the top menu to launch a group voice call |
        |Start a group video call |• Open the Element app <br>• Select an existing room, or create a new one <br>• Tap the `camera icon` in the top menu to launch a group voice call |

        ### Notifications

        | Instructions | Description |
        | ------ | ------ |
        |Global notification settings |• Open your browser and sign in to [https://app.element.io](https://app.element.io/) <br>• Go to `User menu ‣ Notification settings` and configure global notifications to your liking <br>• Enable/disable notifications for the account <br>• Enable/disable desktop notifications for the session <br>• Enable/disable email notifications <br>• Enable/disable notifications for one-to-one or group chats <br>• Enable/disable notifications for messages containing your name or keywords <br>• Enable/disable messages for invitations and more |
        |Direct Messages notification settings |• Open your browser and sign in to [https://app.element.io](https://app.element.io/) <br>• Hover over a chat session in the `People` section and click on the `bell` icon to access notification options <br>• Enable/disable notifications for all messages or messages mentioning your name and keywords |
        |Room notification settings |• Open your browser and sign in to [https://app.element.io](https://app.element.io/) <br>• Hover over a room in the `Rooms` section and click on the `bell` icon to access notification options <br>• Enable/disable notifications for all messages or messages mentioning your name and keywords |

        ### Authentication & verification

        | Instructions | Description |
        | ------ | ------ |
        |Preliminary remarks |In certain circumstances, Element might want to verify your identity. For instance if you log in with a new device or open a new session. Or if you log in with multiple devices at the same time. In such cases, a notification will appear: `Verify this login` or `New login. Was this you?`. Several options exist to prove your identity.|
        |Security Phrase or Key |The most straight forward solution is to provide either your Security Phrase, or your Security Key. |
        |Other device |Alternatively, you can use another device which is already logged in. Click on the verification request appearing in the other device. Now either scan the QR code, or compare the Emoji code. |
        |Successful verification |If you successfully verify your identity, you'll have full access to your messages and will appear as trusted to others. |
        |Active sessions |You can check all active and verified sessions by going to `User menu ‣ Security & Privacy`. |
        |User verification |For additional security, you can also verify the identity of other users you are chatting with. Open the chat, go to `Room info ‣ People`, select the relevant user and click on `Verify ‣ Start Verification`. Wait until the other user accepts the verification request. Now either scan the QR codes with the other user's device, or compare an Emoji code. |


<div style="margin-top:-20px">
</div>

??? warning "Matrix is encrypted, but requires trust"

    Note that Matrix does not pursue the "zero knowledge" principle and [collects meta data](https://gitlab.com/libremonde-org/papers/research/privacy-matrix.org/-/blob/master/part1/README.md/). As can also be read in [Element's privacy policy](https://element.io/privacy): *[...] We might profile metadata pertaining to the configuration and management of hosted homeservers so that we can improve our products and services*.

    Account management is indeed handled by the Matrix server administrators. By using a public Matrix server, you therefore entrust some of your data to these administrators. Even though communication can be encrypted, administrators are potentially able to indefinitely keep a copy of all (encrypted) communication, log IP addresses or access (unencrypted) data such as usernames, email addresses, media files, device information, contact lists, usage profiles, group memberships and so on.

    Even if you self-host your own server, other administrators of Matrix servers participating in a conversation can access this meta data. As formulated by the [project lead](https://teddit.net/r/privacy/comments/da219t/im_project_lead_for_matrixorg_the_open_protocol/f20r6vp/): *"[...] if you invite a user to your chatroom who's on a server that you don't trust, then the history will go to that server. If the room is end-to-end encrypted then that server won't be able to see the messages, but it will be able to see the metadata of who was talking to who and when (but not what). [...]"*.



<br>

<center> <img src="../../assets/img/separator_jami.svg" alt="Jami" width="150px"></img> </center>

## Jami

[Jami](https://jami.net/) is a secure and [open source](https://git.jami.net/savoirfairelinux/) messenger and video calling app, available on all desktop and mobile devices. It's based on a peer-to-peer architecture, where users directly connect without relying on servers. All communication between devices is end-to-end encrypted, with perfect forward secrecy.

=== "Android"

    ??? tip "Show me the step-by-step guide for Android"

        ### Installation

        | Instructions | Description |
        | ------ | ------ |
        |Installation |Install Jami from: <br>• [Google's Play Store](https://play.google.com/store/apps/details?id=cx.ring)<br>• [Aurora Store](https://auroraoss.com/)<br>• [F-Droid](https://f-droid.org/repository/browse/?fdid=cx.ring) |
        |Trackers |The app [contains 0 trackers and requires 21 permissions](https://reports.exodus-privacy.eu.org/en/reports/cx.ring/latest/). By comparison:<br>• TikTok: 16 trackers, 76 permissions<br>• Snapchat: 2 trackers, 44 permissions<br>• WhatsApp: 1 tracker, 57 permissions |
        |Push notifications |Note that while F-Droid version of Jami's app is Google-free, [only the Google Play Store version of Jami's app can handle push notifications](https://git.jami.net/savoirfairelinux/ring-client-android/-/issues/781), using Google's Cloud Messaging (GCM). |

        ### Registration

        | Instructions | Description |
        | ------ | ------ |
        |Create an account |Open the Jami app and click on `Create a Jami account`. Provide a username. |
        |Set a password |Optionally, you can provide a [strong, unique password](https://gofoss.today/passwords/) to encrypt your account. Make sure to store this password safely, as it can't be recovered. |
        |Set up your profile |Optionally, define a username visible to others and a profile picture. |

        ### One-to-one chats, voice and video calls

        | Instructions | Description |
        | ------ | ------ |
        |Start a chat |• Open the Jami app <br>• Tap `Start conversation` <br>• Search for contacts using their username <br>• Tap `Add to contacts`. This will send an invitation to the other user <br>• As soon as the invitation is accepted, you can start chatting |
        |Send a voice message |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• The `Record audio clip` button sits in the menu, next to the composer <br>• Click on it to start recording your message <br>• When you're ready, click on the `Share` button and share it with your Jami contact |
        |Send a video message |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• The `Record video clip` button sits in the menu, next to the composer <br>• Click on it to start recording your message <br>• When you're ready, click on the `Share` button and share it with your Jami contact |
        |Start a voice call |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• Click on the `phone icon` in the top menu to launch a voice call |
        |Start a video call |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• Click on the `camera icon` in the top menu to launch a video call <br>• Screen sharing: while in a video call, click on the `Screensharing` button |
        |Send a file |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• The `Send file` button sits in the menu, next to the composer <br>• Select a file to send |

        ### Group chats, voice and video calls

        | Instructions | Description |
        | ------ | ------ |
        |Start a group chat |As of the time of writing, Jami is [still working on the implementation of group chat](https://jami.net/swarm-introducing-a-new-generation-of-group-conversations/) capability. These so-called "Swarms" are fully distributed and peer-to-peer text conversations, with a potentially unlimited number of participants. |
        |Start a group voice call |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• Click on the `phone icon` in the top menu to launch a voice call <br>• Once the call starts, tap the `Add more contacts` icon  |
        |Start a group video call |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• Click on the `camera icon` in the top menu to launch a video call <br>• Once the call starts, tap the `Add more contacts` icon <br>• Screen sharing: while in a group video call, click on the `Screensharing` button |

        ### Backup

        | Instructions | Description |
        | ------ | ------ |
        |Create a backup |Your account information is stored locally on your device. It will be gone forever if you loose your device or uninstall the application. Make sure to regularly [create a backup](https://gofoss.today/backups/) of your account by going to `Settings ‣ Account ‣ Account ‣ Backup account`. You'll be prompted to provide your password, if you've set one up during the registration. Your backup will be saved to a `.gz` archive file. |
        |Restore a backup |After having re-installed Jami, click on `Connect from backup`. Navigate to the archive file which contains your account backup. If required, provide your password and click on `Connect from backup`. |



=== "iOS"

    ??? tip "Show me the step-by-step guide for iOS"

        ### Installation

        Install Jami from the [App Store](https://apps.apple.com/ca/app/ring-a-gnu-package/id1306951055).

        ### Registration

        | Instructions | Description |
        | ------ | ------ |
        |Create an account |Open the Jami app and click on `Create a Jami account`. Provide a username. |
        |Set a password |Optionally, you can provide a [strong, unique password](https://gofoss.today/passwords/) to encrypt your account. Make sure to store this password safely, as it can't be recovered. |
        |Set up your profile |Optionally, define a username visible to others and a profile picture. |

        ### One-to-one chats, voice and video calls

        | Instructions | Description |
        | ------ | ------ |
        |Start a chat |• Open the Jami app <br>• Tap `Start conversation` <br>• Search for contacts using their username <br>• Tap `Add to contacts`. This will send an invitation to the other user <br>• As soon as the invitation is accepted, you can start chatting |
        |Send a voice message |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• The `Record audio clip` button sits in the menu, next to the composer <br>• Click on it to start recording your message <br>• When you're ready, click on the `Share` button and share it with your Jami contact |
        |Send a video message |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• The `Record video clip` button sits in the menu, next to the composer <br>• Click on it to start recording your message <br>• When you're ready, click on the `Share` button and share it with your Jami contact |
        |Start a voice call |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• Click on the `phone icon` in the top menu to launch a voice call |
        |Start a video call |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• Click on the `camera icon` in the top menu to launch a video call <br>• Screen sharing: while in a video call, click on the `Screensharing` button |
        |Send a file |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• The `Send file` button sits in the menu, next to the composer <br>• Select a file to send |

        ### Group chats, voice and video calls

        | Instructions | Description |
        | ------ | ------ |
        |Start a group chat |As of the time of writing, Jami is [still working on the implementation of group chat](https://jami.net/swarm-introducing-a-new-generation-of-group-conversations/) capability. These so-called "Swarms" are fully distributed and peer-to-peer text conversations, with a potentially unlimited number of participants. |
        |Start a group voice call |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• Click on the `phone icon` in the top menu to launch a voice call <br>• Once the call starts, tap the `Add more contacts` icon  |
        |Start a group video call |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• Click on the `camera icon` in the top menu to launch a video call <br>• Once the call starts, tap the `Add more contacts` icon <br>• Screen sharing: while in a group video call, click on the `Screensharing` button |

        ### Backup

        | Instructions | Description |
        | ------ | ------ |
        |Create a backup |Your account information is stored locally on your device. It will be gone forever if you loose your device or uninstall the application. Make sure to regularly [create a backup](https://gofoss.today/backups/) of your account by going to `Settings ‣ Account ‣ Account ‣ Backup account`. You'll be prompted to provide your password, if you've set one up during the registration. Your backup will be saved to a `.gz` archive file. |
        |Restore a backup |After having re-installed Jami, click on `Connect from backup`. Navigate to the archive file which contains your account backup. If required, provide your password and click on `Connect from backup`. |



=== "Windows"

    ??? tip "Show me the step-by-step guide for Windows"

        ### Installation

        Download and run the [Jami installer for Windows](https://jami.net/download-jami-windows/).

        ### Registration

        | Instructions | Description |
        | ------ | ------ |
        |Create an account |Open the Jami app and click on `Create a Jami account`. Provide a username. |
        |Set a password |Optionally, you can provide a [strong, unique password](https://gofoss.today/passwords/) to encrypt your account. Make sure to store this password safely, as it can't be recovered! |
        |Set up your profile |Optionally, define a username visible to others and a profile picture. |
        |Create a first backup |Your account information is stored locally on your device. It will be gone forever if you loose your device or uninstall the application. During the registration process, you can [create a first backup](https://gofoss.today/backups/) of your account. |

        ### One-to-one chats, voice and video calls

        | Instructions | Description |
        | ------ | ------ |
        |Start a chat |• Open the Jami app <br>• Search for contacts using their username <br>• Click on the `Add to conversations` button. This will send an invitation to the other user <br>• As soon as the invitation is accepted, you can start chatting |
        |Send a voice message |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• The `Leave audio message` button sits in the menu, next to the composer <br>• Click on it to start recording your message <br>• When you're ready, click on the `Send` button <br>• Or leave the recording dialogue to cancel your message |
        |Send a video message |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• The `Leave video message` button sits in the menu, next to the composer <br>• Click on it to start recording your message <br>• When you're ready, click on the `Send` button <br>• Or leave the recording dialogue to cancel your message |
        |Start a voice call |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• Click on the `phone icon` in the top menu to launch a voice call |
        |Start a video call |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• Click on the `camera icon` in the top menu to launch a video call <br>• Screen sharing: while in a video call, click on the `Menu ‣ Screensharing` button |
        |Send a file |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• The `Send file` button sits in the menu, next to the composer <br>• Select a file to send <br>• Alternatively, simply drag & drop a file into the Jami window |

        ### Group chats, voice and video calls

        | Instructions | Description |
        | ------ | ------ |
        |Start a group chat |As of the time of writing, Jami is [still working on the implementation of group chat](https://jami.net/swarm-introducing-a-new-generation-of-group-conversations/) capability. These so-called "Swarms" are fully distributed and peer-to-peer text conversations, with a potentially unlimited number of participants. |
        |Start a group voice call |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• Click on the `phone icon` in the top menu to launch a voice call <br>• Once the call starts, click on `Menu ‣ Add more contacts`  |
        |Start a group video call |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• Click on the `camera icon` in the top menu to launch a video call <br>• Once the call starts, tap the `Menu ‣ Add more contacts` icon <br>• Screen sharing: while in a group video call, click on the `Menu ‣ Screensharing` button |

        ### Backup

        | Instructions | Description |
        | ------ | ------ |
        |Create a backup |Your account information is stored locally on your device. It will be gone forever if you loose your device or uninstall the application. Make sure to regularly [create a backup](https://gofoss.today/backups/) of your account by going to `Settings ‣ Account ‣ Backup account`. You'll be prompted to provide your password, if you've set one up during the registration. Your backup will be saved to a `.gz` archive file. |
        |Restore a backup |After having re-installed Jami, click on `Restore an account from backup`. Navigate to the `.gz` archive file which contains your account backup. If required, provide your password and click on `Restore an account from backup`. |



=== "macOS"

    ??? tip "Show me the step-by-step guide for macOS"

        ### Installation

        Download the [Jami disk image](https://jami.net/download-jami-macos/), open it and drag the Element icon on top of the Application folder. For easy access, open the Applications folder and drag the Element icon to your dock.

        ### Registration

        | Instructions | Description |
        | ------ | ------ |
        |Create an account |Open the Jami app and click on `Create a Jami account`. Provide a username. |
        |Set a password |Optionally, you can provide a [strong, unique password](https://gofoss.today/passwords/) to encrypt your account. Make sure to store this password safely, as it can't be recovered! |
        |Set up your profile |Optionally, define a username visible to others and a profile picture. |
        |Create a first backup |Your account information is stored locally on your device. It will be gone forever if you loose your device or uninstall the application. During the registration process, you can [create a first backup](https://gofoss.today/backups/) of your account. |

        ### One-to-one chats, voice and video calls

        | Instructions | Description |
        | ------ | ------ |
        |Start a chat |• Open the Jami app <br>• Search for contacts using their username <br>• Click on the `Add to conversations` button. This will send an invitation to the other user <br>• As soon as the invitation is accepted, you can start chatting |
        |Send a voice message |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• The `Leave audio message` button sits in the menu, next to the composer <br>• Click on it to start recording your message <br>• When you're ready, click on the `Send` button <br>• Or leave the recording dialogue to cancel your message |
        |Send a video message |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• The `Leave video message` button sits in the menu, next to the composer <br>• Click on it to start recording your message <br>• When you're ready, click on the `Send` button <br>• Or leave the recording dialogue to cancel your message |
        |Start a voice call |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• Click on the `phone icon` in the top menu to launch a voice call |
        |Start a video call |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• Click on the `camera icon` in the top menu to launch a video call <br>• Screen sharing: while in a video call, click on the `Menu ‣ Screensharing` button |
        |Send a file |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• The `Send file` button sits in the menu, next to the composer <br>• Select a file to send <br>• Alternatively, simply drag & drop a file into the Jami window |

        ### Group chats, voice and video calls

        | Instructions | Description |
        | ------ | ------ |
        |Start a group chat |As of the time of writing, Jami is [still working on the implementation of group chat](https://jami.net/swarm-introducing-a-new-generation-of-group-conversations/) capability. These so-called "Swarms" are fully distributed and peer-to-peer text conversations, with a potentially unlimited number of participants. |
        |Start a group voice call |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• Click on the `phone icon` in the top menu to launch a voice call <br>• Once the call starts, click on `Menu ‣ Add more contacts`  |
        |Start a group video call |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• Click on the `camera icon` in the top menu to launch a video call <br>• Once the call starts, tap the `Menu ‣ Add more contacts` icon <br>• Screen sharing: while in a group video call, click on the `Menu ‣ Screensharing` button |

        ### Backup

        | Instructions | Description |
        | ------ | ------ |
        |Create a backup |Your account information is stored locally on your device. It will be gone forever if you loose your device or uninstall the application. Make sure to regularly [create a backup](https://gofoss.today/backups/) of your account by going to `Settings ‣ Account ‣ Backup account`. You'll be prompted to provide your password, if you've set one up during the registration. Your backup will be saved to a `.gz` archive file. |
        |Restore a backup |After having re-installed Jami, click on `Restore an account from backup`. Navigate to the `.gz` archive file which contains your account backup. If required, provide your password and click on `Restore an account from backup`. |



=== "Linux (Ubuntu)"

    ??? tip "Show me the step-by-step guide for Linux (Ubuntu)"

        ### Installation

        | Instructions | Description |
        | ------ | ------ |
        |Open the terminal |Use the `Ctrl+Alt+T` shortcut or click on the `Activities` button on the top left and search for `Terminal` |
        |Install dependencies |`sudo apt install gnupg dirmngr ca-certificates curl --no-install-recommends` |
        |Add the signing keys |`curl -s https://dl.jami.net/public-key.gpg | sudo tee /usr/share/keyrings/jami-archive-keyring.gpg > /dev/null` |
        |Add the repository to the apt source list |`sudo sh -c "echo 'deb [signed-by=/usr/share/keyrings/jami-archive-keyring.gpg] https://dl.jami.net/nightly/ubuntu_20.04/ jami main' > /etc/apt/sources.list.d/jami.list"` |
        |Update the local apt cache |`sudo apt update` |
        |Install the Jami client |`sudo apt install jami`|

        ### Registration

        | Instructions | Description |
        | ------ | ------ |
        |Create an account |Open the Jami app and click on `Create a Jami account`. Provide a username. |
        |Set a password |Optionally, you can provide a [strong, unique password](https://gofoss.today/passwords/) to encrypt your account. Make sure to store this password safely, as it can't be recovered! |
        |Set up your profile |Optionally, define a username visible to others and a profile picture. |
        |Create a first backup |Your account information is stored locally on your device. It will be gone forever if you loose your device or uninstall the application. During the registration process, you can [create a first backup](https://gofoss.today/backups/) of your account. |

        ### One-to-one chats, voice and video calls

        | Instructions | Description |
        | ------ | ------ |
        |Start a chat |• Open the Jami app <br>• Search for contacts using their username <br>• Click on the `Add to conversations` button. This will send an invitation to the other user <br>• As soon as the invitation is accepted, you can start chatting |
        |Send a voice message |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• The `Leave audio message` button sits in the menu, next to the composer <br>• Click on it to start recording your message <br>• When you're ready, click on the `Send` button <br>• Or leave the recording dialogue to cancel your message |
        |Send a video message |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• The `Leave video message` button sits in the menu, next to the composer <br>• Click on it to start recording your message <br>• When you're ready, click on the `Send` button <br>• Or leave the recording dialogue to cancel your message |
        |Start a voice call |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• Click on the `phone icon` in the top menu to launch a voice call |
        |Start a video call |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• Click on the `camera icon` in the top menu to launch a video call <br>• Screen sharing: while in a video call, click on the `Menu ‣ Screensharing` button |
        |Send a file |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• The `Send file` button sits in the menu, next to the composer <br>• Select a file to send <br>• Alternatively, simply drag & drop a file into the Jami window |

        ### Group chats, voice and video calls

        | Instructions | Description |
        | ------ | ------ |
        |Start a group chat |As of the time of writing, Jami is [still working on the implementation of group chat](https://jami.net/swarm-introducing-a-new-generation-of-group-conversations/) capability. These so-called "Swarms" are fully distributed and peer-to-peer text conversations, with a potentially unlimited number of participants. |
       |Start a group voice call |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• Click on the `phone icon` in the top menu to launch a voice call <br>• Once the call starts, tap the `Add more contacts` icon  |
        |Start a group video call |• Open the Jami app <br>• Open an existing chat, or start a new one <br>• Click on the `camera icon` in the top menu to launch a video call <br>• Once the call starts, tap the `Add more contacts` icon <br>• Screen sharing: while in a group video call, click on the `Screensharing` button |


        ### Backup

        | Instructions | Description |
        | ------ | ------ |
        |Create a backup |Your account information is stored locally on your device. It will be gone forever if you loose your device or uninstall the application. Make sure to regularly [create a backup](https://gofoss.today/backups/) of your account by going to `Settings ‣ Account ‣ Backup account`. You'll be prompted to provide your password, if you've set one up during the registration. Your backup will be saved to a `.gz` archive file. |
        |Restore a backup |After having re-installed Jami, click on `Restore an account from backup`. Navigate to the `.gz` archive file which contains your account backup. If required, provide your password and click on `Restore an account from backup`. |



<br>

<center> <img src="../../assets/img/separator_briar.svg" alt="Briar" width="150px"></img> </center>

## Briar

[Briar](https://briarproject.org/) is a secure and [open source](https://code.briarproject.org/briar/briar/tree/master/) messenger app for Android. It particularly appeals to people with an elevated threat model such as activists or journalists. Briar is independent from Google Cloud Messaging (GCM) and based on a peer-to-peer architecture, where users directly connect without relying on servers.

All communication between devices is end-to-end encrypted by default, and sent over the [Tor network](https://gofoss.today/tor/). Briar supports Perfect Forward Secrecy. Everything is stored on the local device, no registration via phone number or email required. The application can also work without a functioning Internet connection, via WLAN or Bluetooth. Note however that users must be online in order to chat with each other.


??? tip "Show me the step-by-step guide for Android"

    ### Installation

    | Instructions | Description |
    | ------ | ------ |
    |Install Briar |Simply download the app from: <br><br> • [Google's Play Store](https://play.google.com/store/apps/details?id=org.briarproject.briar.android/) <br> • [F-Droid](https://briarproject.org/fdroid/) <br> • [Aurora Store](https://auroraoss.com/) |
    |Tracker free |The app [contains 0 trackers and requires 11 permissions](https://reports.exodus-privacy.eu.org/reports/org.briarproject.briar.android/latest/). |
    |No GCM |Note that Briar's Android app can handle push notifications without Google's Cloud Messaging (GCM). This comes in handy if you are looking to [de-google your phone](https://gofoss.today/intro-free-your-phone#google-ios-free-phones/). |

    ### Registration

    | Instructions | Description |
    | ------ | ------ |
    |Create an account |Open the Briar app and provide a username. |
    |Set a password |Provide a [strong, unique password](https://gofoss.today/passwords/). Make sure to store this password safely: if you forget it, your account can't be recovered! |
    |Enable background connections |Tap the buttons to disable battery optimisation and to enable Briar to run in the background. |
    |Create account |Finally, hit the `Create account` button. |

    ### One-to-one chats

    | Instructions | Description |
    | ------ | ------ |
    |Meet in person |• Meet with your contact in person & open the Briar app <br>• Tap the `+` button & select `Add contact nearby` <br>• Tap `Continue` <br>• You both have to can the QR codes appearing on your screens <br>• This way, your communication is authenticated from the start <br>• Start chatting |
    |Get introduced |• If you can't meet in person, get introduced by a common friend you trust <br>• This friend needs to open Briar on his/her phone <br>• Then, he/she should tap your name in the contact list <br>• Select `Menu ‣ Make Introduction` <br>• Choose the other contact you want to be introduced to <br>• Finally, tap `Make Introduction` <br>• You will receive a request to accept the introduction <br>• Once both parties have accepted the introduction, you can start chatting |
    |Add contacts at a distance |• If you can't meet in person, add your contact at a distance <br>• Tap the `+` button & select `Add contact at a distance` <br>• Share the appearing link with your contact using a secure channel (e.g. encrypted email, Signal, etc.) <br>• Similarly, ask your contact to share his/her link with you <br> Verify the link is authentic before you enter it into the app (e.g. call, previously authenticated chat, meeting) <br>• Once both links have been added, tap `Continue`, choose a nickname for your new contact <br>• As soon as your contact accepts the invitation, you can start chatting |

    ### Group chats, forums, blogs & RSS feeds

    | Instructions | Description |
    | ------ | ------ |
    |Start a group chat |• Open the Briar app <br>• Tap `Menu ‣ Private Groups` <br>• Tap the `+` button to create a new group <br>• Choose a name, tap `Create Group` & invite contacts <br>• Start chatting |
    |Start a forum |• A forum is a public conversation <br>• Unlike in private group chats, anyone can  invite additional contacts <br>• Open the Briar app <br>• Tap `Menu ‣ Forums` <br>• Tap the `+` button to create a new forum <br>• Choose a name & tap `Create Forum` <br>• Open the forum and tap the `Sharing` icon to invite your contacts |
    |Start a blog |• The blog allows to share news & updates with your contacts <br>• Open the Briar app <br>• Tap `Menu ‣ Blogs` <br>• Tap the `Pen` button to write a new post, then tap `Publish` |
    |Read RSS feeds |• Follow blogs or news sites from within Briar! <br>• Open the Briar app <br>• Tap `Menu ‣ Blogs` <br>• Tap `Menu ‣ Import RSS Feed` <br>• Enter the URL and tap `Import` |


    ### Backup & screen lock

    | Instructions | Description |
    | ------ | ------ |
    |Backup |• For security reasons, your account will be permanently gone if you loose access to your device, forget your password or uninstall Briar <br>• This includes your identities, contacts & messages <br>• You'll have to add & verify your contacts again after re-installing the app <br>• Also, there's no possibility to migrate your account to another device |
    |Screen lock |• Briar can be locked without signing out <br>• Tap `Menu ‣ Settings ‣ Security` <br>• Enable `App lock` <br>• Briar will now lock automatically when not being used (e.g. after 5min) or when you tap `Menu ‣ Lock App` <br>• To unlock, enter your device's screen lock (PIN, pattern, password) |



<br>

<center> <img src="../../assets/img/separator_silence.svg" alt="Silence" width="150px"></img> </center>

## Silence

[Silence](https://silence.im/) is a secure, [open source](https://git.silence.dev/Silence/Silence-Android/) SMS and MMS app for Android. All messages are encrypted locally and messages to other Silence users are encrypted over the air. It's independent from Google Cloud Messaging (GCM).

??? tip "Show me the step-by-step guide for Android"

    | Instructions | Description |
    | ------ | ------ |
    |Install Silence |Simply download the app from: <br>• [Google's Play Store](https://play.google.com/store/apps/details?id=org.smssecure.smssecure/) <br>• [F-Droid](https://f-droid.org/en/packages/org.smssecure.smssecure/) <br>• [Aurora Store](https://auroraoss.com/) |
    |Tracker free |The app [contains 0 trackers and requires 19 permissions](https://reports.exodus-privacy.eu.org/en/reports/org.smssecure.smssecure/latest/). |
    |No GCM |Note that Silence's Android app can handle push notifications without Google's Cloud Messaging (GCM). This comes in handy if you are looking to [de-google your phone](https://gofoss.today/intro-free-your-phone#google-ios-free-phones/). |


??? warning "Silence is not encrypted by default & doesn't hide your metadata"

    Messages are only encrypted if both sides use the Silence app. You can still chat with contacts that don't have the app installed, but the SMS/MMS messages will remain unencrypted. Also, even when messages are encrypted, attackers can still figure out who you’re chatting with, when, or how often.


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Support" width="150px"></img> </center>

## Support

For further details or questions, refer to:

* [Signal's documentation](https://support.signal.org/hc/en-us/) or [Signal's community](https://community.signalusers.org/)
* [Element's FAQ](https://element.io/help) or [Element's community](https://teddit.net/r/elementchat/)
* [Jami's FAQ](https://jami.net/help/) or [Jami's community](https://forum.jami.net/)
* [Briar's manual](https://briarproject.org/manual/) or [Matrix chat](https://matrix.to/#/#freenode_#briar:matrix.org)
* [Silence's FAQ](https://silence.im/faq/) or [Silence's mattermost channel](https://chat.silence.dev/silence)


<br>
