---
template: main.html
title: How to create strong and unique passwords
description: Password strength. Password generator. Diceware word list. Two-factor authentication. Password manager. Keepass. Has my password been hacked?
---

# Choose secure passwords and two-factor authentication

<center>
<img align="center" src="../../assets/img/password.png" alt="Diceware" width ="450px"></img>
</center>

!!! level "This chapter is geared towards beginners. No particular tech skills are required."


## Diceware

[Diceware](https://theworld.com/~reinhold/diceware.html) is a popular method to create strong and unique, yet simple to remember passwords. All you need is a dice, a pen and a piece of paper. If you choose a password composed of at least 7 words, this is considered as virtually unbreakable by today's technology standard. Read on below to learn more.

??? tip "Show me step-by-step guide"

    <center>

    | Steps | Instructions |
    | :------: | ------ |
    | 1 |Select a diceware list. For example the [original list](https://theworld.com/~reinhold/dicewarewordlist.pdf), or the [list provided by the Electronic Frontier Foundation](https://www.eff.org/files/2016/07/18/eff_large_wordlist.txt/). There are many others to choose from, in several languages. |
    | 2 |Roll a dice 5 times and write down the numbers. |
    | 3 |Look up the corresponding word in the diceware list, and write it down. |
    | 4 |Repeat the previous steps until you have at least 6 words. Actually, 7 words are recommended to achieve an entropy of 90.3 bits. According to [Diceware's FAQ](https://theworld.com/~reinhold/dicewarefaq.html#howlong/), this is unbreakable with any known technology, but may be within the range of large organizations by around 2030. Eight words should be completely secure through 2050. |
    | 5 |The combination of these words is your secure password. Make sure to separate the words by a space.|

    </center>

??? tip "Show me a summary video"

    <center>
    <iframe src="https://archive.org/embed/how-to-make-a-super-secure-password" width="600" height="400" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

    *Courtesy of the [Electronic Frontier Foundation](https://www.eff.org/).*
    </center>


??? question "Has my account been hacked?"

    | Where you hacked? | Description |
    | ------ | ------ |
    | [Have I Been Pwned](https://haveibeenpwned.com/) | Reverse search engine to check your email or password against a huge list of stolen data. |
    | [Dehashed](https://www.dehashed.com/) | Search for IP addresses, emails, usernames, names, phone numbers and so on to gain insight on database breaches and account leaks. |


<br>

<center> <img src="../../assets/img/separator_https.svg" alt="Keepass" width="150px"></img> </center>

## Keepass

[Keepass](https://keepass.info/) is a free and open source password manager, available on almost all devices. It stores your passwords in an encrypted database, which itself is protected by a master password — one password to rule them all. Obviously, you should **never forget this master password**!

We also recommend to keep your password manager database offline. Store it locally on your devices, and keep two remote copies as backup. Finally, don't forget to regularly change your passwords.


### Keepass clients

=== "Android"

    [Keepass DX](https://www.keepassdx.com/) is a free, secure and [open source](https://github.com/Kunzisoft/KeePassDX) password manager for Android. More detailed instructions below.

    ??? tip "Show me the step-by-step guide for Android"

        Simply download the app from [Google's Play Store](https://play.google.com/store/apps/details?id=com.kunzisoft.keepass.free/), [F-Droid](https://www.f-droid.org/packages/com.kunzisoft.keepass.libre/) or [Aurora Store](https://auroraoss.com/). It [contains 0 trackers and requires 6 permissions](https://reports.exodus-privacy.eu.org/en/reports/com.kunzisoft.keepass.libre/latest/).


=== "iOS"

    At the time of writing, there was no free version of Keepass available for iOS. [Strongbox](https://github.com/strongbox-password-safe/Strongbox/) is a secure and open source Keepass client. More detailed instructions below.

    ??? tip "Show me the step-by-step guide for iOS"

        Simply download Strongbox from the [App Store](https://apps.apple.com/us/app/strongbox-keepass-pwsafe/id897283731/).


=== "Windows"

    [KeePass XC](https://keepassxc.org) is a cross-platform, community-driven, free and [open source](https://github.com/keepassxreboot/keepassxc/) password manager. More detailed instructions below.

    ??? tip "Show me the step-by-step guide for Windows"

        [Download the installer](https://keepassxc.org/download/#windows/), double click on the `.msi` file and follow the installation wizard.


=== "macOS"

    [KeePass XC](https://keepassxc.org/) is a cross-platform, community-driven, free and [open source](https://github.com/keepassxreboot/keepassxc/) password manager. More detailed instructions below.

    ??? tip "Show me the step-by-step guide for macOS"

        [Download the installer](https://keepassxc.org/download/#mac/), it should open by itself and mount a new volume containing the Keepass XC application. If not, open the downloaded `.dmg` file and drag the appearing Keepass XC icon on top of the Application folder. For easy access, open the Applications folder and drag the Keepass XC icon to the dock.


=== "Linux (Ubuntu)"

    [KeePass XC](https://keepassxc.org/) is a cross-platform, community-driven, free and [open source](https://github.com/keepassxreboot/keepassxc/) password manager. More detailed instructions below.

    ??? tip "Show me the step-by-step guide for Linux (Ubuntu)"

        Open the terminal with the `CTRL + ALT + T` shortcut, or click on the `Applications` button on the top left and search for `Terminal`. Run the following commands to install KeePassXC:

            ```bash
            sudo add-apt-repository ppa:phoerious/keepassxc
            sudo apt update
            sudo apt install keepassxc
            ```

<div style="    margin-top: -20px;">
</div>

??? tip "Show me a summary video"

    <center>
    <iframe src="https://archive.org/embed/using-password-managers-to-stay-safe-online" width="600" height="400" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

    *Courtesy of the [Electronic Frontier Foundation](https://www.eff.org/).*
    </center>

<br>

<center> <img src="../../assets/img/separator_networksecurity.svg" alt="Two-factor authentication" width="150px"></img> </center>

## Two-factor authentication

[Two-factor authentication](https://en.wikipedia.org/wiki/Help:Two-factor_authentication/) (2FA) provides an additional security layer. It requires more than just a password to access services or accounts. For example, a single-use verification code sent by SMS or generated by an authenticator app or key.

While two-factor authentication is generally considered to increase security, it offers additional surface for cyberattacks such as [Phishing](https://en.wikipedia.org/wiki/Phishing/), [identity theft](https://en.wikipedia.org/wiki/SIM_swap_scam/) (SIM swap) or [SMS hijacking](https://en.wikipedia.org/wiki/Signalling_System_No._7/) (SS7 attacks). It is also less convenient to the average user.

Choose for yourself if two-factor authentication brings additional benefits, depending on your [threat model](https://www.eff.org/document/surveillance-self-defense-threat-modeling/). If you go for it, don't forget to safely store the backup codes that some services provide. They can be life savers when you loose access to your phone or authentication program.


### 2FA clients

=== "Android"

    [AndOTP](https://github.com/andOTP/andOTP/) is a free and open source two-factor authenticator for Android. More detailed instructions below.

    ??? tip "Show me the step-by-step guide for Android"

        Simply download the app from [Google's Play Store](https://play.google.com/store/apps/details?id=org.shadowice.flocke.andotp&hl=en&gl=US/), [F-Droid](https://f-droid.org/en/packages/org.shadowice.flocke.andotp/) or [Aurora Store](https://auroraoss.com/). It [contains 0 trackers and requires 1 permission](https://reports.exodus-privacy.eu.org/reports/org.shadowice.flocke.andotp/latest/).

=== "iOS"

    [Tofu](https://www.tofuauth.com/) is a free and open source two-factor authenticator for iOS. More detailed instructions below.

    ??? tip "Show me the step-by-step guide for iOS"

        Simply download Tof from the [App Store](https://apps.apple.com/app/tofu-authenticator/id1082229305).


=== "Windows"

    [Yubico Authenticator](https://www.yubico.com/products/yubico-authenticator/) is a cross-platform and open source authenticator app. It requires a physical hardware key. More detailed instructions below.

    ??? tip "Show me the step-by-step guide for Windows"

        [Download the installer](https://www.yubico.com/products/yubico-authenticator/) and follow the installation wizzard.


=== "macOS"

    [Yubico Authenticator](https://www.yubico.com/products/yubico-authenticator/) is a cross-platform and open source authenticator app. It requires a physical hardware key. More detailed instructions below.

    ??? tip "Show me the step-by-step guide for macOS"

        [Download the installer](https://www.yubico.com/products/yubico-authenticator/), it should open by itself and mount a new volume containing the Yubico application. If not, open the downloaded `.dmg` file and drag the appearing Yubico icon on top of the Application folder. For easy access, open the Applications folder and drag the Yubico icon to the dock.


=== "Linux (Ubuntu)"

    [Yubico Authenticator](https://www.yubico.com/products/yubico-authenticator/) is a cross-platform and open source authenticator app. It requires a physical hardware key. More detailed instructions below.

    ??? tip "Show me the step-by-step guide for Linux (Ubuntu)"

        Open the terminal with the `CTRL + ALT + T` shortcut, or click on the `Applications` button on the top left and search for `Terminal`. Run the following commands to install Yubico Authenticator:

        ```bash
        sudo add-apt-repository ppa:yubico/stable
        sudo apt update
        sudo apt-get install yubioath-desktop
        ```

<div align="center">
<img src="https://imgs.xkcd.com/comics/password_strength.png" width="550px" alt="Password strength"></img>
</div>

<br>
