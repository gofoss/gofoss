---
template: main.html
title: Here's why you should backup your files
description: How to backup data? What is a backup routine? What are the 3 types of backups? What is the 3-2-1 backup method? How to install FreeFileSync? What is mirror synchronisation? What is two-way synchronisation?`
---

# 3-2-1 backup strategy

<div align="center">
<img src="../../assets/img/cloud_storage_2.png" alt="3-2-1 backup strategy" width="550px"></img>
</div>

!!! level "This chapter is geared towards beginners. No particular tech skills are required."

You think backups are boring? Rest assured, loosing your data is worse. Keeping copies makes you resilient agains data loss, theft, corruption or accidental deletion. Create a regular 3-2-1 backup strategy to keep at least three copies of your data:

* **Original file**: stored on your primary devices, such as your computer, laptop, mobile phone, camera, and so on.
* **Local copy**: backed up at regular intervals. Keep this copy on site, for example at home or your work place. In case of an incident affecting your primary devices, data can be quickly restored. For additional security, also consider [encrypting this backup with VeraCrypt](https://gofoss.today/encrypted-files/#veracrypt).
* **Remote copy**: backed up at regular intervals. Keep this copy off site, for example in the cloud or another remote location. In case of an incident affecting both your primary devices and local backup, data can still be recovered. Think theft, fire, water damage, and so on. For additional security, also consider [encrypting this backup with VeraCrypt](https://gofoss.today/encrypted-files/#veracrypt).


<br>

<center> <img src="../../assets/img/separator_backups.svg" alt="Backup" width="150px"></img> </center>

## FreeFileSync

[FreeFileSync](https://freefilesync.org/) is a free and [open-source](https://github.com/hkneptune/FreeFileSync/) folder synchronisation software. It creates and manages backup copies of your files by detecting differences between source and target folders. More detailed instructions below.

=== "Windows"

    ??? tip "Show me the step-by-step guide for Windows"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Install FreeFileSync | Download the latest [FreeFileSync installer for Windows](https://freefilesync.org/download.php), double click on the `.exe` file and follow the installation wizard. |
        | Choose folders | Start FreeFileSync and select the two folders to be compared: <br>• `source folder`: in the left panel, select the folder containing the data you want to back up <br>•  `target folder`: in the right panel, select the folder where the data should be backed up to |
        | Compare folders | Click on the button `Compare` to examine the differences between both folders, based on file modification time and file size. Note that you can also configure a slower, but more detailed comparison based on file content. Once the process is finished, you can visualize in the central column which files are identical between both folders, which files are different, and which files are only available in one of both folders.  |
        | Synchronise folders | There are two main ways to synchronise the source and target folders, carefully choose the one corresponding your needs: <br><br>**Mirror synchronisation**:<br><br>• proceed as described above to compare both folders <br>• select the `Mirror ->` synchronisation option <br>• the central column should indicate one single sync direction, from the source (left) folder to the target (right) folder <br>• you can visualize in the central column which files will be updated in the target folder, which files will be added to the target folder, and which files will be deleted from the target folder <br>• you can exclude files or entire sub-folders from the synchronisation process by using the right-click menu <br>• once you've selected all files which need to be synchronised, click on the `Synchronisation` button. FreeFileSync will now copy files from the source to the target folder, and erase files from the target folder, until both folders are exact copies. <br><br>**Two-way synchronisation**:<br><br>• proceed as described above to compare both folders <br>• select the `<- Two way ->` synchronisation option <br>• unlike the mirror option, the two-way synchronisation detects differences between both folders in both directions. The central column should therefore indicate two sync directions, both from the source (left) folder to the target (right) folder, and the other way round <br>• you can visualize in the central column which files will be updated in the source and/or target folders, which files will be added to the source and/or target folders, and which files will be deleted from the source and/or target folders. Conflicting versions between files will also be displayed <br>• you can change sync direction or entirely exclude files/sub-folders from the synchronisation process by using the right-click menu <br>• once you've defined how all files need to be synchronised, click on the `Synchronisation` button. FreeFileSync will now copy files from the between, or erase files from, both source and target folder until they are exact copies. |

        </center>


=== "macOS"

    ??? tip "Show me the step-by-step guide for macOS"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Install FreeFileSync | Download the latest [FreeFileSync installer for macOS](https://freefilesync.org/download.php), which should open by itself and mount a new volume containing the FreeFileSync application. If not, open the downloaded `.dmg` file and drag the appearing FreeFileSync icon on top of the Application folder. For easy access, open the Applications folder and drag the FreeFileSync icon to your dock. |
        | Choose folders | Start FreeFileSync and select the two folders to be compared: <br>• `source folder`: in the left panel, select the folder containing the data you want to back up <br>•  `target folder`: in the right panel, select the folder where the data should be backed up to |
        | Compare folders | Click on the button `Compare` to examine the differences between both folders, based on file modification time and file size. Note that you can also configure a slower, but more detailed comparison based on file content. Once the process is finished, you can visualize in the central column which files are identical between both folders, which files are different, and which files are only available in one of both folders.  |
        | Synchronise folders | There are two main ways to synchronise the source and target folders, carefully choose the one corresponding your needs: <br><br>**Mirror synchronisation**:<br><br>• proceed as described above to compare both folders <br>• select the `Mirror ->` synchronisation option <br>• the central column should indicate one single sync direction, from the source (left) folder to the target (right) folder <br>• you can visualize in the central column which files will be updated in the target folder, which files will be added to the target folder, and which files will be deleted from the target folder <br>• you can exclude files or entire sub-folders from the synchronisation process by using the right-click menu <br>• once you've selected all files which need to be synchronised, click on the `Synchronisation` button. FreeFileSync will now copy files from the source to the target folder, and erase files from the target folder, until both folders are exact copies. <br><br> **Two-way synchronisation**:<br><br>• proceed as described above to compare both folders <br>• select the `<- Two way ->` synchronisation option <br>• unlike the mirror option, the two-way synchronisation detects differences between both folders in both directions. The central column should therefore indicate two sync directions, both from the source (left) folder to the target (right) folder, and the other way round <br>• you can visualize in the central column which files will be updated in the source and/or target folders, which files will be added to the source and/or target folders, and which files will be deleted from the source and/or target folders. Conflicting versions between files will also be displayed <br>• you can change sync direction or entirely exclude files/sub-folders from the synchronisation process by using the right-click menu <br>• once you've defined how all files need to be synchronised, click on the `Synchronisation` button. FreeFileSync will now copy files from the between, or erase files from, both source and target folder until they are exact copies. |

        </center>


=== "Linux (Ubuntu)"

    ??? tip "Show me the step-by-step guide for Linux (Ubuntu)"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Install FreeFileSync | Download the latest [FreeFileSync installer for Linux](https://freefilesync.org/download.php), which should be called something similar to `FreeFileSync_XX.YY_Linux.tar.gz`. For the sake of this tutorial, let's assume the file was downloaded to the `/home/gofoss/Downloads` folder. Unpack and install the files with the following commands (adapt accordingly): <br><br>`cd /home/gofoss/Downloads` <br>`tar xfv FreeFileSync_XX.YY_Linux.tar.gz` <br>`./FreeFileSync_XX.YY_Install.run` <br><br> Then, follow the on-screen instructions to accept the license terms, select an installation directory and install the software. |
        | Choose folders | Start FreeFileSync and select the two folders to be compared: <br>• `source folder`: in the left panel, select the folder containing the data you want to back up <br>•  `target folder`: in the right panel, select the folder where the data should be backed up to |
        | Compare folders | Click on the button `Compare` to examine the differences between both folders, based on file modification time and file size. Note that you can also configure a slower, but more detailed comparison based on file content. Once the process is finished, you can visualize in the central column which files are identical between both folders, which files are different, and which files are only available in one of both folders.  |
        | Synchronise folders | There are two main ways to synchronise the source and target folders, carefully choose the one corresponding your needs: <br><br>**Mirror synchronisation**:<br><br>• proceed as described above to compare both folders <br>• select the `Mirror ->` synchronisation option <br>• the central column should indicate one single sync direction, from the source (left) folder to the target (right) folder <br>• you can visualize in the central column which files will be updated in the target folder, which files will be added to the target folder, and which files will be deleted from the target folder <br>• you can exclude files or entire sub-folders from the synchronisation process by using the right-click menu <br>• once you've selected all files which need to be synchronised, click on the `Synchronisation` button. FreeFileSync will now copy files from the source to the target folder, and erase files from the target folder, until both folders are exact copies. <br><br>**Two-way synchronisation**:<br><br>• proceed as described above to compare both folders <br>• select the `<- Two way ->` synchronisation option <br>• unlike the mirror option, the two-way synchronisation detects differences between both folders in both directions. The central column should therefore indicate two sync directions, both from the source (left) folder to the target (right) folder, and the other way round <br>• you can visualize in the central column which files will be updated in the source and/or target folders, which files will be added to the source and/or target folders, and which files will be deleted from the source and/or target folders. Conflicting versions between files will also be displayed <br>• you can change sync direction or entirely exclude files/sub-folders from the synchronisation process by using the right-click menu <br>• once you've defined how all files need to be synchronised, click on the `Synchronisation` button. FreeFileSync will now copy files from the between, or erase files from, both source and target folder until they are exact copies. |

        </center>


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="FreeFileSync support" width="150px"></img> </center>

## Support

For further details or questions, refer to [FreeFileSync's documentation](https://freefilesync.org/manual.php), [FreeFileSync's tutorials](https://freefilesync.org/tutorials.php) or ask [FreeFileSync's community](https://freefilesync.org/forum/) for help.


<br>
