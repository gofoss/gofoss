---
template: main.html
title: Why data security matters
description: Use Veracrypt or Cryptomator to encrypt your data. Is Veracrypt safe? How to use Veracrypt? Is Cryptomator safe? How to use Cryptomator? Cryptomator vs Veracrypt? Veracrypt vs Truecrypt?
---

# Secure your data with passwords, encryption and backups

<center>
<img align="center" src="../../assets/img/secure_data.png" alt="Data security" width ="250px"></img>
</center>

## We live in a digital age

Humanity generates more data every day. Digital data will have grown to [175 Zettabytes](https://www.forbes.com/sites/tomcoughlin/2018/11/27/175-zettabytes-by-2025/) by 2025. That's one hundred and fifty-nine billion one hundred and sixty-one million five hundred and seventy-two thousand eight hundred and ten Terabytes.

Every connected person engages with digital data about 4.900 times per day. That's once every 18 seconds. You are no exception. A significant amount of your life is digitised and stored all over the place. Your phone, computer, watch and cloud services might know more about you than your closest friends.


<br>

<center> <img src="../../assets/img/separator_firewall.svg" alt="Strong and unique passwords" width="150px"></img> </center>

## Protect your data

In this chapter, we'll discuss how to prevent unauthorised access to your data. Use strong and unique passwords. Avoid re-using passwords for several devices or accounts. Use a password manager. And consider two-factor authentication for additional security.


<br>

<center> <img src="../../assets/img/separator_davx5.svg" alt="3-2-1 backup strategy" width="150px"></img> </center>

## Backup your data

We'll also discuss how an effective backup strategy can shield you from data loss, theft, deletion, corruption, leaks, ransomware, or worse.


<br>

<center> <img src="../../assets/img/separator_https.svg" alt="VeraCrypt and Cryptomator" width="150px"></img> </center>

## Encrypt your data

Finally,  we'll discuss how to use encryption to make your data unreadable in case of a breach. For example if an attacker manages to hack your cloud storage account or gets unauthorised access to your computer.

<br>
