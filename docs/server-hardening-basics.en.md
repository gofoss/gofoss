---
template: main.html
title: Basic server hardening
description: Self-hosting (basic). Harden your server. What is a firewall? What is ufw? What is NTP? Linux Malware Detect. ClamAV. What is a core dump? What are kernel modules?
---

# Basic server hardening

!!! level "This chapter is geared towards advanced users. Solid tech skills are required."

!!! warning "Disclaimer"

    **No system is safe**. While this chapter presents measures to protect a server from most immediate threats, any skilled hacker or organisation with sufficient resources will probably find a way into the system.


## Firewall

<div align="center">
<img src="../../assets/img/firewall.png" alt="Hardened server" width="500px"></img>
</div>

A server permanently interacts with devices located inside and outside its network. It needs to be protected by a security system called *firewall*, which controls incoming and outgoing traffic. The Uncomplicted Firewall ([ufw](https://help.ubuntu.com/community/UFW/)) is a popular choice on Ubuntu servers, more details below.

??? tip "Show me the step-by-step guide"

    ### Installation

    Install the Uncomplicated Firewall, enable autostart after each reboot, and verify it's running:

    ```bash
    sudo apt install ufw
    sudo ufw enable
    sudo systemctl status ufw
    ```

    ### Firewall rules

    Deny all incoming and outgoing traffic by default:

    ```bash
    sudo ufw default deny outgoing
    sudo ufw default deny incoming
    ```

    Make sure the firewall only authorises required traffic. This is done by opening the respective communication endpoints, also called [ports](https://en.wikipedia.org/wiki/Port_(computer_networking)). For the purpose of this tutorial, we'll only open a few of them. Adjust the commands according to your specific needs:

    ```bash
    sudo ufw allow 80,443/tcp
    sudo ufw allow 22/tcp
    sudo ufw allow 123/udp
    sudo ufw allow in from any to any port 53
    sudo ufw allow out 80,443/tcp
    sudo ufw allow out 22/tcp
    sudo ufw allow out 123/udp
    sudo ufw allow out from any to any port 53
    ```

    Some additional information about those ports:

    <center>

    | Port | Description |
    | ------ | ------ |
    | `80` | HTTP requests: assigned to the commonly used internet communication protocol, Hypertext Transfer Protocol (HTTP). Your server might use it to send data to and receive data from the Web. |
    | `443` | HTTPS requests: standard port for all secured HTTP traffic (HTTPS). Your server might use it to send and receive encrypted Web traffic. |
    | `22` | SSH requests: usually used to run the Secure Shell (SSH) protocol. Your server might use it for remote logins. |
    | `123` | NTP synchronisation: allows to synchronise time between computers. |
    | `53` | DNS requests: used for domain name resolution. Your server might use it to turn human readable domain names into IP addresses. |

    </center>


    Finally, check if firefall rules are set correctly:

    ```bash
    sudo ufw status numbered
    ```

    Here's what the rules should look like:

    ```bash
    Status: active

            To                  Action          From
            --                  ------          ----
    [ 1]    80,443/tcp          ALLOW IN        Anywhere
    [ 2]    22/tcp              ALLOW IN        Anywhere
    [ 3]    123/udp             ALLOW IN        Anywhere
    [ 4]    53                  ALLOW IN        Anywhere
    [ 5]    80,443/tcp          ALLOW OUT       Anywhere
    [ 6]    22/tcp              ALLOW OUT       Anywhere
    [ 7]    123/udp             ALLOW OUT       Anywhere
    [ 8]    53                  ALLOW OUT       Anywhere
    [ 9]    80,443/tcp (v6)     ALLOW IN        Anywhere (v6)
    [10]    22/tcp  (v6)        ALLOW IN        Anywhere (v6)
    [11]    123/udp  (v6)       ALLOW IN        Anywhere (v6)
    [12]    53  (v6)            ALLOW IN        Anywhere (v6)
    [13]    80,443/tcp  (v6)    ALLOW OUT       Anywhere (v6)
    [14]    22/tcp  (v6)        ALLOW OUT       Anywhere (v6)
    [15]    123/udp  (v6)       ALLOW OUT       Anywhere (v6)
    [16]    53  (v6)            ALLOW OUT       Anywhere (v6)
    ```

    ### Router settings

    You might also want to check the router settings and make sure all unused ports are disabled. Refer to the router's manual for more information.


??? tip "Show me a summary video"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/16198a6e-e223-441d-9c49-e94726f5ed2a" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_ntp.svg" alt="Server time NTP" width="150px"></img> </center>

## Server time

Many security protocols rely on the system time. An incorrect time can have negative impacts on security. The Network Time Protocol ([NTP](https://en.wikipedia.org/wiki/Network_Time_Protocol/)) makes sure the server's time remains synchronised with reference computers. These so-called NTP servers are organised in hierarchal layers, or stratums. Read on below for more details on how to set up NTP on your server.

<div align="center">
<img src="../../assets/img/ntp.png" alt="Server time NTP" width="500px"></img>
</div>

<center>

| Layer | Description |
| ------ | ------ |
|`Stratum 0`| Hardware clocks, for example atomic clocks, GPS or radio time receivers. |
|`Stratum 1`| Computers with a direct connection to hardware clocks. |
|`Stratum 2`| Computers synchronised over a network with stratum 1 servers. |
|`NTP clients`| Computers periodically requesting the time from NTP servers. |

</center>


??? tip "Show me the step-by-step guide"

    ### Configuration

    Back up the configuration file:

    ```bash
    sudo cp --archive /etc/systemd/timesyncd.conf /etc/systemd/timesyncd.conf-COPY-$(date +"%Y%m%d%H%M%S")
    ```

    Open the file:

    ```bash
    sudo vi /etc/systemd/timesyncd.conf
    ```

    Modify the content to access public and open-source time servers operated by the [pool.ntp.org](https://www.ntppool.org/en/) project or [Ubuntu](https://ubuntu.com/server/docs/network-ntp):

    ```bash
    [Time]
    NTP=0.pool.ntp.org 1.pool.ntp.org
    FallbackNTP=ntp.ubuntu.com
    ```

    Save and close the file (`:wq!`). Finally, restart the `systemd-timesyncd` service and make sure it's running:

    ```bash
    sudo systemctl restart systemd-timesyncd
    sudo systemctl status systemd-timesyncd
    ```

    ### Time zone

    Let's configure the server's [time zone](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones/):

    ```bash
    timedatectl | grep Time
    ```

    Assuming you live near Budapest, the terminal should display something like:

    ```bash
    Time zone: Europe/Budapest (CEST, +0200)
    ```

    If not, set up the correct time zone (adjust accordingly):

    ```bash
    sudo timedatectl set-timezone Europe/Budapest
    ```


??? tip "Show me a summary video"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/80abd77b-e973-4333-bb9f-33cd1dc73731" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_ssh.svg" alt="Secure SSH" width="150px"></img> </center>

## Secure SSH

We've already explained how to [remotely connect to the server from another computer](https://gofoss.today/ubuntu-server/). Read on below for more details on how to secure the remote connection:

<center>

| Security feature | Description |
| ------ | ------ |
|SSH port |Change the SSH port from 22 to another value. For the purpose of this tutorial, we'll choose port `2222`; any other value will do, make sure to adjust the corresponding commands accordingly. |
|Login restrictions |Limit the number of login attempts. |
|Access restrictions |Authorise only one user to log in. In this case the administrator user `gofossadmin` (adjust accordingly). |
|Logging |Keep track of each login. |
|Root restrictions |Forbid remote SSH access to the root account. |
|Authentication |Use authentication keys with passphrases instead of password authentication. |
|Disconnection |Enable automatic disconnection after 5 minutes of inactivity. |
|Encryption keys |Remove short encryption keys for increased security. |
|Legal banner |Add a legal banner to warn unauthorised users. |

</center>

??? tip "Show me the step-by-step guide"

    ### SSH port & security settings

    Open the new SSH port 2222 (adjust accordingly):

    ```bash
    sudo ufw allow 2222/tcp
    sudo ufw allow out 2222/tcp
    ```

    Back up the SSH configuration file. If something goes wrong, you'll be able to recover the initial settings:

    ```bash
    sudo cp --preserve /etc/ssh/sshd_config /etc/ssh/sshd_config.$(date +"%Y%m%d%H%M%S")
    ```

    Open the SSH configuration file with the command:


    ```bash
    sudo vi /etc/ssh/sshd_config
    ```

    Delete the file content by typing `:%d`. Then enter or copy/paste the following content:

    ```bash
    # supported HostKey algorithms by order of preference:

    HostKey /etc/ssh/ssh_host_ed25519_key
    HostKey /etc/ssh/ssh_host_rsa_key
    HostKey /etc/ssh/ssh_host_ecdsa_key
    KexAlgorithms curve25519-sha256@libssh.org,ecdh-sha2-nistp521,ecdh-sha2-nistp384,ecdh-sha2-nistp256,diffie-hellman-group-exchange-sha256
    Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr
    MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,umac-128@openssh.com

    # various security settings:

    Port                            2222        # change from standard SSH port 22 (adjust accordingly)
    LogLevel                        VERBOSE     # log user's key fingerprint on login to have a clear audit track of which key was using to log in
    Protocol                        2           # only use the newer, more secure protocol
    PermitUserEnvironment           no          # don't let users set environment variables
    PermitRootLogin                 no          # don't allow root login
    PubkeyAuthentication            yes         # allow public key authentication
    PasswordAuthentication          no          # login with password not allowed
    PermitEmptyPasswords            no          # don't allow login if the account has an empty password
    MaxAuthTries                    3           # maximum allowed attempts to login
    MaxSessions                     2           # maximum number of open sessions
    X11Forwarding                   no          # disable X11 forwarding
    IgnoreRhosts                    yes         # ignore .rhosts and .shosts
    UseDNS                          no          # verify hostname matches IP
    ClientAliveCountMax             0           # maximum number of client alive messages sent without response
    ClientAliveInterval             300         # timeout in seconds before a response request
    AllowUsers                      gofossadmin # only allow user gofossadmin to login remotely (adjust accordingly)

    # disable port forwarding:

    AllowAgentForwarding            no
    AllowTcpForwarding              no
    AllowStreamLocalForwarding      no
    GatewayPorts                    no
    PermitTunnel                    no

    # log sftp level file access (read/write/etc.):

    Subsystem sftp  /usr/lib/openssh/sftp-server

    # other security settings:

    Compression                     no
    PrintMotd                       no
    TCPKeepAlive                    no
    ChallengeResponseAuthentication no
    UsePAM                          yes
    AcceptEnv LANG LC_*
    ```

    Save and close the SSH configuration file (`:wq!`). Restart the SSH server and verify that the new SSH port is set to 2222 (or whatever port you chose):

    ```bash
    sudo systemctl restart sshd
    sudo ss -tlpn | grep ssh
    ```

    ### Disconnect inactive connections

    Make sure all remote SSH connections are disabled after 5 minutes of inactivity:

    ```bash
    echo 'TMOUT=300' >> .bashrc
    tail .bashrc
    ```

    ### Remove short encryption keys

    SSH uses the [Diffie-Hellman algorithm](https://en.wikipedia.org/wiki/Diffie%E2%80%93Hellman_key_exchange/) to establish a secure connection. For security reasons, it's recommended to use keys which are at least 3072 bits long.

    Back up the SSH configuration file. If something goes wrong, you'll be able to recover the initial settings:

    ```bash
    sudo cp --archive /etc/ssh/moduli /etc/ssh/moduli-COPY-$(date +"%Y%m%d%H%M%S")
    ```

    Remove keys shorter than 3072 bits:

    ```bash
    sudo awk '$5 >= 3071' /etc/ssh/moduli | sudo tee /etc/ssh/moduli.tmp
    sudo mv /etc/ssh/moduli.tmp /etc/ssh/moduli
    ```

    ### Legal banner

    A warning message should be displayed to any user trying to remotely log into the server. Open the following file:

    ```bash
    sudo vi /etc/issue.net
    ```

    Add a legal banner, such as this one for example:

    ```bash
    This system is for the use of authorised users only.

    Individuals using this computer system without authority, or in excess of their authority, are subject to having all of their activities on this system monitored and recorded by system personnel.

    In the course of monitoring individuals improperly using this system, or in the course of system maintenance, the activities of authorised users may also be monitored.

    Anyone using this system expressly consents to such monitoring and is advised that if such monitoring reveals possible evidence of criminal activity, system personnel may provide the evidence of such monitoring to law enforcement officials.
    ```

    ### Test configuration & close port 22

    That's it! From now on you can securely use your client machine to log into the server. Just open a terminal, switch to the administrator account and connect to the server using your passphrase. Don't forget to adjust the username, IP address and SSH port as required:

    ```bash
    su - gofossadmin
    ssh -p 2222 gofossadmin@192.168.1.100
    ```

    Try it a couple of times. If everything works as expected, close the unused port 22:

    ```bash
    sudo ufw delete allow 22/tcp
    sudo ufw delete allow out 22/tcp
    ```

??? tip "Show me a summary video"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/841da26c-a57a-4be1-be4b-d36d4965dfd0" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_mysql.svg" alt="Secure MySQL" width="150px"></img> </center>

## Secure mySQL

MySQL databases run as a back-end to many web services. To protect the information maintained by MySQL from unauthorised access. Instructions on how to secure MySQL are outlined below.


??? tip "Show me the step-by-step guide"

    ### MySQL hardening

    Run the MySQL hardening script:

    ```bash
    sudo mysql_secure_installation
    ```

    Follow the on-screen instructions:

    * provide a [strong, unique root password](https://gofoss.today/passwords/)
    * remove the anonymous user
    * forbid root to login remotely
    * remove the test database
    * reload the privileges table


    ### Prevent unauthorised access

    Back up the MySQL configuration file. If something goes wrong, you'll be able to recover the initial settings:

    ```bash
    sudo cp --archive /etc/mysql/mysql.conf.d/mysqld.cnf /etc/mysql/mysql.conf.d/mysqld.cnf-COPY-$(date +"%Y%m%d%H%M%S")
    ```

    Open the configuration file:

    ```bash
    sudo vi /etc/mysql/mysql.conf.d/mysqld.cnf
    ```

    Disable remote MySQL access and prevent unauthorised access to local files by adding or adjusting the following two lines:

    ```bash
    bind-address = 127.0.0.1
    local-infile = 0
    ```

    Save and close the MySQL configuration file (`:wq!`).


    ### User permissions

    Note that it's best practice to create a dedicated user with limited privileges for each application requiring MySQL access. For example the [cloud storage](https://gofoss.today/cloud-storage/) or [photo gallery](https://gofoss.today/photo-gallery/) we'll set up at a later stage will each have their own MySQL user. This way, applications remain isolated and can only access databases on a need-to-know basis.


??? tip "Show me a summary video"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/9ac7ab65-47f6-459b-ae08-ca98ad2bfd73" frameborder="0" allowfullscreen></iframe>
    </center>



<br>

<center> <img src="../../assets/img/separator_apache.svg" alt="Secure Apache" width="150px"></img> </center>

## Secure Apache

Apache is the world's most used web server. Configure it properly to enhance security. More details below.

??? tip "Show me the step-by-step guide"

    ### Enable security modules

    Install the following two [Apache modules](https://en.wikipedia.org/wiki/List_of_Apache_modules/):

    ```bash
    sudo apt install libapache2-mod-security2 libapache2-mod-evasive
    ```

    Enable the security modules, as well as other common modules required for this tutorial, and restart Apache:

    ```bash
    sudo a2enmod security2
    sudo a2enmod evasive
    sudo a2enmod rewrite
    sudo a2enmod headers
    sudo a2enmod ssl
    sudo a2enmod proxy
    sudo a2enmod proxy_html
    sudo a2enmod proxy_http
    sudo a2enmod proxy_wstunnel
    sudo a2enmod xml2enc
    sudo a2enmod expires
    sudo systemctl restart apache2
    ```

    Some additional information about those modules:

    <center>

    | Module | Description |
    | ------ | ------ |
    | `Security2` | Protects your server from various attacks, such as [SQL injection](https://en.wikipedia.org/wiki/SQL_injection/), [session hijacking](https://en.wikipedia.org/wiki/Session_hijacking/), [cross-site scripting](https://en.wikipedia.org/wiki/Cross-site_scripting/), [bad user agents](https://en.wikipedia.org/wiki/User_agent/), etc. |
    | `Evasive` | Provides evasive actions in the event of a [Denial of Service (DoS)](https://en.wikipedia.org/wiki/Denial-of-service_attack/) or [Distributed Denial of Service (DDoS)](https://en.wikipedia.org/wiki/Denial-of-service_attack/) attack, or a [brute force attack](https://en.wikipedia.org/wiki/Brute-force_attack/). |
    | `Rewrite` | Allows to rewrite URLs to enable redirections, for example from `http://` to `https://` |
    | `Headers` | Allows to control and modify HTTP requests and response headers. |
    | `SSL` | Enables encryption via [SSL](https://en.wikipedia.org/wiki/SSL/) and [TLS](https://en.wikipedia.org/wiki/Transport_Layer_Security/). |
    | `Proxy, proxy_html, proxy_http` | Create a proxy/gateway for your server, ensure that links work for users outside the proxy, and proxy HTTP and HTTPS requests. |
    | `Xml2enc` | Provides enhanced internationalisation support. |
    | `Expires` | Increases page load time by determining how long an image will be stored by the browser. |

    </center>

    ### Hide sensitive information

    Per default, the server sends [HTTP headers](https://en.wikipedia.org/wiki/List_of_HTTP_header_fields/) containing information on the Apache version, modules, operating system, and so on. This data can be used to exploit vulnerabilities. Create a custom Apache configuration file to hide such sensitive information:

    ```bash
    sudo vi /etc/apache2/conf-available/custom.conf
    ```

    Add the following content:

    ```bash
    ServerTokens        Prod
    ServerSignature     Off
    TraceEnable         Off
    Options all -Indexes
    Header always unset X-Powered-By
    ```

    Save and close the file (`:wq!`).

    Apply the custom configuration, check for syntax errors (the terminal should prompt `OK`) and restart Apache:

    ```bash
    sudo a2enconf custom.conf
    sudo apachectl configtest
    sudo systemctl restart apache2
    ```

    Some additional information about those settings:

    <center>

    | Setting | Description |
    | ------ | ------ |
    | `ServerTokens Prod` |Only return "Apache" in the server header. |
    | `ServerSignature Off` |Hide the server version on pages generated by Apache. |
    | `TraceEnable Off` |Disable the HTTP TRACE method, which can expose your server to [cross-site scripting](https://en.wikipedia.org/wiki/Cross-site_scripting/) attacks. |
    | `Options all -Indexes` |Disable directory views. |
    | `Header always unset X-Powered-By` |Hide the information "X-Powered-By ..." and avoid displaying which software is used. |

    </center>


    ### Configure ModSecurity

    [Modsecurity](https://modsecurity.org/) is an open-source web application firewall. This tool can be configured to protect your server from various attacks. Open the configuration file:

    ```bash
    sudo mv /etc/modsecurity/modsecurity.conf-recommended /etc/modsecurity/modsecurity.conf
    sudo vi /etc/modsecurity/modsecurity.conf
    ```

    Modify/add the following parameters:

    ```bash
    SecRuleEngine                   On
    SecResponseBodyAccess           Off
    SecRequestBodyLimit             8388608
    SecRequestBodyNoFilesLimit      131072
    SecRequestBodyInMemoryLimit     262144
    ```

    Save and close the file (`:wq!`).

    Some additional information about those settings:

    <center>

    | Setting | Description |
    | ------ | ------ |
    | `SecRuleEngine On` |Enable ModSecurity using the basic default rules. |
    | `SecResponseBodyAccess Off` |Disable to use less RAM and CPU. |
    | `SecRequestBodyLimit 8388608` |Set the maximum request body size accepted for buffering to 8 MB. |
    | `SecRequestBodyNoFilesLimit 131072` |Set the maximum data size accepted for buffering to 128 KB. |
    | `SecRequestBodyInMemoryLimit 262144` |Set the maximum request body size stored in RAM to 256 KB. |

    </center>


    Enable the latest OWASP ModSecurity Core Rule Set (CRS), a set of generic attack detection rules [maintained on GitHub](https://github.com/SpiderLabs/owasp-modsecurity-crs/):

    ```bash
    sudo rm -rf /usr/share/modsecurity-crs
    sudo apt install git
    sudo git clone https://github.com/SpiderLabs/owasp-modsecurity-crs.git /usr/share/modsecurity-crs
    cd /usr/share/modsecurity-crs
    sudo mv crs-setup.conf.example crs-setup.conf
    ```

    Back up the ModSecurity configuration file. If something goes wrong, you'll be able to recover the initial settings:

    ```bash
    sudo cp --archive /etc/apache2/mods-enabled/security2.conf /etc/apache2/mods-enabled/security2.conf-COPY-$(date +"%Y%m%d%H%M%S")
    ```

    Open the configuration file:

    ```bash
    sudo vi /etc/apache2/mods-enabled/security2.conf
    ```

    Delete the content with the command `:%d` and add or copy/paste the following lines:

    ```bash
    <IfModule security2_module>
        SecDataDir /var/cache/modsecurity
        IncludeOptional /etc/modsecurity/*.conf
        IncludeOptional /usr/share/modsecurity-crs/*.conf
        IncludeOptional /usr/share/modsecurity-crs/rules/*.conf
    </IfModule>
    ```

    Save and close the file (`:wq!`). Check for syntax errors (the terminal should prompt `OK`) and restart Apache to apply the new configuration:

    ```bash
    sudo apachectl configtest
    sudo systemctl restart apache2
    ```

    Back up the ModEvasive configuration file. If something goes wrong, you'll be able to recover the initial settings:

    ```bash
    sudo cp --archive /etc/apache2/mods-enabled/evasive.conf /etc/apache2/mods-enabled/evasive.conf-COPY-$(date +"%Y%m%d%H%M%S")
    ```

    Configure ModEvasive to protect your server from [Denial of Service (DoS)](https://en.wikipedia.org/wiki/Denial-of-service_attack/), [Distributed Denial of Service (DDoS)](https://en.wikipedia.org/wiki/Denial-of-service_attack/) and [brute force attacks](https://en.wikipedia.org/wiki/Brute-force_attack/).

    Open the configuration file:

    ```bash
    sudo vi /etc/apache2/mods-enabled/evasive.conf
    ```

    Modify or add the following parameters:

    ```bash
    <IfModule mod_evasive20.c>
        DOSPageCount        5
        DOSSiteCount        50
        DOSPageInterval     1
        DOSSiteInterval     1
        DOSBlockingPeriod   600
        DOSLogDir           "/var/log/mod_evasive"
    </IfModule>
    ```

    Save and close the file (`:wq!`). The following commands create a directory for the log files, check for syntax errors (the terminal should prompt `OK`), restart Apache and verify if the status is `Active`:


    ```bash
    sudo mkdir /var/log/mod_evasive
    sudo chown -R www-data: /var/log/mod_evasive
    sudo apachectl configtest
    sudo systemctl restart apache2
    sudo systemctl status apache2
    ```

??? tip "Show me a summary video"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/9b1b3559-e391-45aa-95be-23a5c0ef28ce" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_php.svg" alt="Secure PHP" width="150px"></img> </center>

## Secure PHP

PHP is a widely-used programming language for web services. Configure it properly to enhance security. More details below.

??? tip "Show me the step-by-step guide"

    ### PHP version

    [PHP supports various versions](https://www.php.net/supported-versions.php). At the time of writing, PHP 7.4 is included by default in Ubuntu's 20.04 repositories. Check which version is installed on your system:

    ```bash
    php -v
    ```

    ### PHP modules

    Install some common PHP modules required for this tutorial, applying the same version number as above:

    ```bash
    sudo apt install php7.4-common php7.4-mbstring php7.4-xmlrpc php7.4-gd php7.4-xml php7.4-intl php7.4-mysql php7.4-cli php7.4-zip php7.4-curl php7.4-json php7.4-pgsql php7.4-opcache php7.4-sqlite3
    ```

    ### PHP security settings

    The `php.ini` configuration files contain all relevant PHP security settings. First, let's find out where these files are stored:

    ```bash
    sudo find / -name php.ini
    ```

    Depending on the PHP version, the command might return something like

    ```bash
    /etc/php/7.4/apache2/php.ini
    /etc/php/7.4/cli/php.ini
    ```

    The `/etc/php/7.4/apache2/php.ini` file is used by the Apache server, while the `/etc/php/7.4/cli/php.ini` file is used by the CLI PHP program.

    Back up both configuration files. If something goes wrong, you'll be able to recover the initial settings:

    ```bash
    sudo cp --archive /etc/php/7.4/apache2/php.ini /etc/php/7.4/apache2/php.ini-COPY-$(date +"%Y%m%d%H%M%S")
    sudo cp --archive /etc/php/7.4/cli/php.ini /etc/php/7.4/cli/php.ini-COPY-$(date +"%Y%m%d%H%M%S")
    ```

    Open the first configuration file:

    ```bash
    sudo vi /etc/php/7.4/apache2/php.ini
    ```

    Modify or add the following parameters:

    ```bash
    expose_php          =   Off
    allow_url_fopen     =   Off
    allow_url_include   =   Off
    display_errors      =   Off
    mail.add_x_header   =   Off
    disable_functions   =   show_source,system,passthru,phpinfo,proc_open,allow_url_fopen,curl_exec
    max_execution_time  =   90
    max_input_time      =   90
    memory_limit        =   1024M
    ```

    Save and close the file (`:wq!`).

    Open the second configuration file:

    ```bash
    sudo vi /etc/php/7.4/cli/php.ini
    ```

    Modify or add the following parameters:

    ```bash
    expose_php = Off
    allow_url_fopen = Off
    ```

    Save and close the file (`:wq!`).

    Some additional information about those settings:

    <center>

    | Setting | Description |
    | ------ | ------ |
    | `expose_php = Off` |Hides the PHP version in headers. |
    | `allow_url_fopen = Off` |Disables remote PHP code execution to reduce [code injection vulnerabilities](https://en.wikipedia.org/wiki/Code_injection/). |
    | `allow_url_include = Off` |Disables remote PHP code execution to reduce [code injection vulnerabilities](https://en.wikipedia.org/wiki/Code_injection/). |
    | `display_errors = Off` |Disables error display. |
    | `mail.add_x_header = Off` |Removes PHP header from emails. |
    | `disable_functions = show_source, system, passthru, phpinfo, proc_open, allow_url_fopen, curl_exec` |Disables potentially harmful PHP functions. The following three functions have not been disabled, as this [breaks Pihole v5.2](https://gofoss.today/secure-domain/): `exec`, `shell_exec`, `popen`. |
    | `max_execution_time = 90` |Sets the maximum time a script is allowed to run (in seconds). |
    | `max_input_time = 90` |Sets the maximum time a script can parse data (in seconds). |
    | `memory_limit = 1024M` |Sets the maximum amount of memory allocated to a script. |

    </center>

??? tip "Show me a summary video"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/5ba7da80-c09e-4432-a231-a9823f5cd9a2" frameborder="0" allowfullscreen></iframe>
    </center>

<br>

<center> <img src="../../assets/img/separator_davx5.svg" alt="Security updates" width="150px"></img> </center>

## Security updates

The server needs to be up-to-date to reduce security vulnerabilities. Enable automatic updates of the most critical security patches, as detailed below.

??? tip "Show me the step-by-step guide"

    Enable automatic updates:

    ```bash
    sudo apt install unattended-upgrades
    ```

    Create a configuration file:

    ```bash
    sudo vi /etc/apt/apt.conf.d/51myunattended-upgrades
    ```

    Add the following content to configure unattended upgrades:

    ```bash
    // Enable the update/upgrade script (0=disable)
    APT::Periodic::Enable "1";

    // Do "apt-get update" automatically every n-days (0=disable)
    APT::Periodic::Update-Package-Lists "1";

    // Do "apt-get upgrade --download-only" every n-days (0=disable)
    APT::Periodic::Download-Upgradeable-Packages "1";

    // Do "apt-get autoclean" every n-days (0=disable)
    APT::Periodic::AutocleanInterval "7";

    // Send report mail to root
    //     0:  no report             (or null string)
    //     1:  progress report       (actually any string)
    //     2:  + command outputs     (remove -qq, remove 2>/dev/null, add -d)
    //     3:  + trace on    APT::Periodic::Verbose "2";
    APT::Periodic::Unattended-Upgrade "1";

    // Automatically upgrade packages from these
    Unattended-Upgrade::Origins-Pattern {
        "o=Ubuntu,a=stable";
        "o=Ubuntu,a=stable-updates";
        "origin=Ubuntu,codename=${distro_codename},label=Ubuntu-Security";
    };

    // You can specify your own packages to NOT automatically upgrade here
    Unattended-Upgrade::Package-Blacklist {
    };

    // Run dpkg --force-confold --configure -a if a unclean dpkg state is detected to true to ensure that updates get installed even when the system got interrupted during a previous run
    Unattended-Upgrade::AutoFixInterruptedDpkg "true";

    //Perform the upgrade when the machine is running because we wont be shutting our server down often
    Unattended-Upgrade::InstallOnShutdown "false";

    // Send an email to this address with information about the packages upgraded.
    Unattended-Upgrade::Mail "root";

    // Always send an e-mail
    Unattended-Upgrade::MailOnlyOnError "false";

    // Remove all unused dependencies after the upgrade has finished
    Unattended-Upgrade::Remove-Unused-Dependencies "true";

    // Remove any new unused dependencies after the upgrade has finished
    Unattended-Upgrade::Remove-New-Unused-Dependencies "true";

    // DO NOT automatically reboot WITHOUT CONFIRMATION if the file /var/run/reboot-required is found after the upgrade.
    Unattended-Upgrade::Automatic-Reboot "false";

    // DO NOT automatically reboot even if users are logged in.
    Unattended-Upgrade::Automatic-Reboot-WithUsers "false";
    ```

    Save and close the file (`:wq!`). Perform a test run to make sure everything is working:

    ```bash
    sudo unattended-upgrade -d --dry-run
    ```

??? tip "Show me a summary video"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/8a8ad992-8a36-433c-b2b4-17908993a86b" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<div align="center">
<img src="https://imgs.xkcd.com/comics/server_problem.png" width="550px" alt="Server security"></img>
</div>

<br>