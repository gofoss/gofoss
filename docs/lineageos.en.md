---
template: main.html
title: How to install LineageOS
description: Degoogle your phone. What is Lineageos? Is Lineageos legal? What is microg? Is microg legal? Lineageos vs Calyxos? Lineageos vs Grapheneos?
---

# LineageOS, the most popular custom mobile OS

<center>
<img align="center" src="../../assets/img/lineageosmicrog.png" alt="LineageOS for microG" width ="600px"></img>
</center>

<br>

!!! level "This chapter is geared towards advanced users. Solid tech skills are required."

## Compatibility

If you don't own a Pixel phone, [LineageOS for microG](https://lineage.microg.org) might be a suitable Android mobile operating system for you. The project is a fork from the free and open-source Android distribution [LineageOS](https://lineageos.org/) and builds on [microG](https://microg.org/) to replace Google's proprietary libraries with free and open-source code. Lineage for microG currently supports hundreds of phone models. Make sure your *exact* handset model is featured in the [device list](https://wiki.lineageos.org/devices/).

Before switching to LineageOS, keep in mind that while almost everything works just fine, [some apps don't play nice](https://github.com/microg/android_packages_apps_GmsCore/wiki/Implementation-Status/). Including a few apps from Google, such as Android Wear, Google Fit, Google Cast or Android Auto. Fortunately, there are great [FOSS alternatives](https://gofoss.today/foss-apps/) available. Finally, mind that using paid apps without Google's Play Store can be a little tricky.


<br>

<center> <img src="../../assets/img/separator_backups.svg" alt="LineageOS backup" width="150px"></img> </center>

## Backup

All data will be erased from your device during the installation process. Don't take any chances, [back up your phone](https://gofoss.today/backups/)!

<br>

<center> <img src="../../assets/img/separator_bug.svg" alt="LineageOS debugging" width="150px"></img> </center>

## USB Debugging

USB debugging needs to be enabled to allow the Android Debug Bridge (ADB) to connect your phone to the computer. More detailed instructions below.

=== "Windows"

    ??? tip "Show me the step-by-step guide for Windows"

        ### On your Android phone

        | Steps | Description |
        | :------: | ------ |
        | 1 | Open `Settings ► System ► About phone`. |
        | 2 | Tap seven times on `Build number` to enable developer options. |
        | 3 | Go back, and select the `Developer options` menu. |
        | 4 | Scroll down, and check the `Android debugging` or `USB debugging` boxes. |
        | 5 | Plug your Android device into the Windows computer. |
        | 6 | Change the USB mode to `file transfer (MTP)`. |

        ### On your Windows computer

        | Steps | Description |
        | :------: | ------ |
        | 1 | [Download ADB for Windows](https://dl.google.com/android/repository/platform-tools-latest-windows.zip/). |
        | 2 | Extract the content of the `.zip` file. |
        | 3 | Open the Windows Explorer and browse to the folder containing the extracted ADB files. |
        | 4 | Right click within this folder and select the entry `Open command window here` or `Open PowerShell window here`. |
        | 5 | Run the following command to launch the ADB daemon:<br> `adb devices` |
        | 6 | A dialog should pop up on your Android phone, asking you to allow USB debugging. Check the box `Always allow`, and click on `OK`. |



=== "macOS"

    ??? tip "Show me the step-by-step guide for macOS"

        ### On your Android phone

        | Steps | Description |
        | :------: | ------ |
        | 1 | Open `Settings ► System ► About phone`. |
        | 2 | Tap seven times on `Build number` to enable developer options. |
        | 3 | Go back, and select the `Developer options` menu. |
        | 4 | Scroll down, and check the `Android debugging` or `USB debugging` boxes. |
        | 5 | Plug your Android device into the macOS device. |
        | 6 | Change the USB mode to `file transfer (MTP)`. |

        ### On your macOS device

        | Steps | Description |
        | :------: | ------ |
        | 1 | [Download ADB for macOS](https://dl.google.com/android/repository/platform-tools-latest-darwin.zip/). |
        | 2 | Extract the content of the `.zip` file. |
        | 3 | Open the terminal and browse to the folder containing the extracted ADB files. |
        | 4 | Run the following command to launch the ADB daemon:<br> `adb devices` |
        | 5 | A dialog should pop up on your Android phone, asking you to allow USB debugging. Check the box `Always allow`, and click on `OK`. |


=== "Linux (Ubuntu)"

    ??? tip "Show me the step-by-step guide for Linux (Ubuntu)"

        ### On your Android phone

        | Steps | Description |
        | :------: | ------ |
        | 1 | Open `Settings ► System ► About phone`. |
        | 2 | Tap seven times on `Build number` to enable developer options. |
        | 3 | Go back, and select the `Developer options` menu. |
        | 4 | Scroll down, and check the `Android debugging` or `USB debugging` boxes. |
        | 5 | Plug your Android device into the Linux (Ubuntu) device. |
        | 6 | Change the USB mode to `file transfer (MTP` |

        ### On your Linux (Ubuntu) device

        | Steps | Description |
        | :------: | ------ |
        | 1 | Open the terminal with the shortcut `CTRL + ALT + T`, or click on the `Applications` button on the top left and search for `Terminal`. |
        | 2 |Run the following commands to update your system packages:<br> `sudo apt update` <br> `sudo apt upgrade` |
        | 3 | Run the following commands to install ADB Fastboot:<br> `sudo apt install android-tools-adb` <br> `sudo apt install android-tools-fastboot`|
        | 4 | Run the following command to start the ADB server (if it hasn't autostarted yet):<br> `sudo adb start-server`|
        | 5 | Run the following command to launch the ADB daemon:<br> `adb devices` |
        | 6 | A dialog should pop up on your Android phone, asking you to allow USB debugging. Check the box `Always allow`, and click on `OK`. |


<br>

<center> <img src="../../assets/img/separator_ssh.svg" alt="LineageOS bootloader" width="150px"></img> </center>

## Unlocked Bootloader

!!! warning "Caution! Here be dragons."

    Unlocking the bootloader will erase all data on your phone. If not already the case, [back up your phone](https://gofoss.today/backups/)! Also, unlocking the bootloader will void your phone's warranty and lower it's security. [An attacker with physical access could potentially install software without decrypting the phone](https://en.wikipedia.org/wiki/Evil_Maid_attack/), such as a password logger. Assess this potential security risk in light of your [threat model](https://ssd.eff.org/en/module/your-security-plan/).

The bootloader is the first piece of code that executes when powering on a phone. Many manufacturers lock the bootloader to avoid any modification to the phone. In order to install LineageOS for microG, the bootloader needs to be unlocked. **This is the trickiest part**, as the process differs from phone to phone, and requires a little bit of research:

* Check out the [LineageOS wiki](https://wiki.lineageos.org/devices/). Look for your *exact* phone model, navigate to the `Installation` instructions and read the chapter `Unlocking the bootloader`.
* If you need more guidance or support, head over to the [LineageOS community](https://lineageos.org/community/) or the [LineageOS Reddit community](https://teddit.net/r/LineageOS/).


<br>

<center> <img src="../../assets/img/separator_twrp.svg" alt="LineageOS TWRP" width="150px"></img> </center>

## Custom Recovery

Android phones come with a recovery software provided by Google, also called *stock recovery*. It's used to restore factory settings or update the operating system. To be able to install LineageOS for microG, this stock recovery needs to be replaced with a *custom recovery*.

The [Team Win Recovery Project](https://twrp.me/) (TWRP) is a popular custom recovery. Beyond enabling the installation of LineageOS for microG, TWRP also allows to create backups and change system settings. More detailed instructions below.


=== "Windows"

    ??? tip "Show me the step-by-step guide for Windows"

        ### On your Android phone

        | Steps | Description |
        | :------: | ------ |
        | 1 | Plug your Android device into the Windows computer. |
        | 2 | Change the USB mode to `file transfer (MTP)`. |

        ### On your Windows machine

        | Steps | Description |
        | :------: | ------ |
        | 1 | Browse to the [TWRP website](https://twrp.me/Devices/), select the correct phone model and download the appropriate TWRP version. For the purpose of this tutorial, let's assume the file has been downloaded to `/Downloads/twrp-x.x.x.img`. |
        | 2 | Open the Windows Explorer and browse to the folder containing the downloaded `.img` file. |
        | 3 | Right click within this folder and select the entry `Open command window here` or `Open PowerShell window here`. |
        | 4 | Run the following command to reboot the phone into *bootloader* mode:<br> `adb reboot bootloader` <br><br> Alternatively, reboot the phone into *bootloader* mode by powering it off, and then holding down both `Volume DOWN` and `POWER` buttons. Release them once the word `FASTBOOT` appears on the screen. The combination of buttons might change from device to device. More information [here](https://wiki.lineageos.org/devices).|
        | 5 | Run the following command to make sure the computer can detect your phone:<br> `fastboot devices`|
        | 6 | The terminal should prompt something similar to this:<br> `$ 0077fxe89p12  fastboot`|
        | 7 | Run the following commands to navigate to the folder containing the custom recovery file (adjust accordingly) and flash the custom recovery to your phone: <br>`cd /Downloads` <br>`fastboot flash recovery twrp-x.x.x.img`|
        | 8 | The terminal should prompt something similar to this:<br> `$ sending ‘recovery’ (8450 KB)…` <br>`$ OKAY [ 0.730s]` <br>`$ writing ‘recovery’…` <br>`$ OKAY [ 0.528s]` <br>`$ finished. total time: 1.258s`|
        | 9 | Run the following command to boot directly into TWRP:<br> `fastboot boot recovery twrp-x.x.x.img`|


=== "macOS"

    ??? tip "Show me the step-by-step guide for macOS"

        ### On your Android phone

        | Steps | Description |
        | :------: | ------ |
        | 1 | Plug your Android device into the Windows computer. |
        | 2 | Change the USB mode to `file transfer (MTP)`. |

        ### On your macOS device

        | Steps | Description |
        | :------: | ------ |
        | 1 | Browse to the [TWRP website](https://twrp.me/Devices/), select the correct phone model and download the appropriate TWRP version. For the purpose of this tutorial, let's assume the file has been downloaded to `/Downloads/twrp-x.x.x.img`. |
        | 2 | Open the terminal. |
        | 3 | Run the following command to reboot the phone into *bootloader* mode:<br> `adb reboot bootloader` <br><br> Alternatively, reboot the phone into *bootloader* mode by powering it off, and then holding down both `Volume DOWN` and `POWER` buttons. Release them once the word `FASTBOOT` appears on the screen. The combination of buttons might change from device to device. More information [here](https://wiki.lineageos.org/devices/).|
        | 4 | Run the following command to make sure the computer can detect your phone:<br> `sudo fastboot devices`|
        | 5 | The terminal should prompt something similar to this:<br> `$ 0077fxe89p12  fastboot`|
        | 6 | Run the following commands to navigate to the folder containing the custom recovery file (adjust accordingly) and flash the custom recovery to your phone:<br>`cd /Downloads` <br>`sudo fastboot flash recovery twrp-x.x.x.img`|
        | 7 | The terminal should prompt something similar to this:<br> `$ sending ‘recovery’ (8450 KB)…` <br>`$ OKAY [ 0.730s]` <br>`$ writing ‘recovery’…` <br>`$ OKAY [ 0.528s]` <br>`$ finished. total time: 1.258s`|
        | 8 | Run the following command to boot directly into TWRP:<br> `sudo fastboot boot recovery twrp-x.x.x.img`|


=== "Linux (Ubuntu)"

    ??? tip "Show me the step-by-step guide for Linux (Ubuntu)"

        ### On your Android phone

        | Steps | Description |
        | :------: | ------ |
        | 1 | Plug your Android device into the Windows computer. |
        | 2 | Change the USB mode to `file transfer (MTP)`. |

        ### On your Linux (Ubuntu) machine

        | Steps | Description |
        | :------: | ------ |
        | 1 | Browse to the [TWRP website](https://twrp.me/Devices/), select the correct phone model and download the appropriate TWRP version. For the purpose of this tutorial, let's assume the file has been downloaded to `/home/gofoss/Downloads/twrp-x.x.x.img`. |
        | 2 |  Open the terminal with the shortcut `CTRL + ALT + T`, or click on the `Applications` button on the top left and search for `Terminal`. |
        | 3 | Run the following command to reboot the phone into *bootloader* mode:<br> `adb reboot bootloader` <br><br> Alternatively, reboot the phone into *bootloader* mode by powering it off, and then holding down both `Volume DOWN` and `POWER` buttons. Release them once the word `FASTBOOT` appears on the screen. The combination of buttons might change from device to device. More information [here](https://wiki.lineageos.org/devices/).|
        | 4 | Run the following command to make sure the computer can detect your phone:<br> `sudo fastboot devices`|
        | 5 | The terminal should prompt something similar to this:<br> `$ 0077fxe89p12 fastboot`|
        | 6 | Run the following commands to navigate to the folder containing the custom recovery file (adjust accordingly) and flash the custom recovery to your phone:<br>`cd /home/gofoss/Downloads` <br>`sudo fastboot flash recovery twrp-x.x.x.img`|
        | 7 | The terminal should prompt something similar to this:<br> `$ sending ‘recovery’ (8450 KB)…` <br>`$ OKAY [ 0.730s]` <br>`$ writing ‘recovery’…` <br>`$ OKAY [ 0.528s]` <br>`$ finished. total time: 1.258s`|
        | 8 | Run the following command to boot directly into TWRP:<br> `sudo fastboot boot recovery twrp-x.x.x.img`|



<br>

<center> <img src="../../assets/img/separator_settings.svg" alt="LineageOS installation" width="150px"></img> </center>

## Installation

Once the phone successfully booted into recovery (TWRP), LineageOS for microG can be installed. More detailed instructions below.

=== "Windows"

    ??? tip "Show me the step-by-step guide for Windows"

        ### On your Windows machine

        | Steps | Description |
        | :------: | ------ |
        | 1 | [Find out which codename is associated with your phone model on the LineageOS website](https://wiki.lineageos.org/devices/). For example, the Fairphone's codename is "FP2", the Nexus 5X is called "bullhead", the Samsung Galaxy S9 is called "starlte", and so on. |
        | 2 | [Look for the same codename on the LineageOS for microG website](https://download.lineage.microg.org/). Click on the corresponding folder and download the latest version of LineageOS for microG. For the purpose of this tutorial, let's assume it has been downloaded to the location `/Downloads/lineage-XX.X-202XXXXX-microG-CODENAME.zip`. |
        | 3 | Open the Windows Explorer and browse to the folder containing the downloaded `.zip` file. |
        | 4 | Right click within this folder and select the entry `Open command window here` or `Open PowerShell window here`. |
        | 5 | Run the following command to reboot the phone into *recovery* mode:<br> `adb reboot recovery`<br><br> Alternatively, reboot the phone into *recovery* mode by powering it off, then holding down both `Volume UP` and `POWER` buttons. Release them once the logo appears on the screen. The combination of buttons might change from device to device. More information [here](https://www.xda-developers.com/how-to-boot-to-recovery/). |

        ### On your Android phone

        | Steps | Description |
        | :------: | ------ |
        | 1 | TWRP custom recovery should now load. Swipe the slider to enter the main screen. |
        | 2 | Tap the `Wipe` button and then the `Format Data` button. **Caution!** This will remove encryption and delete all files stored in the internal storage. [Make sure you backed up your data](https://gofoss.today/backups/)! |
        | 3 | Return to the previous menu, tap the `Wipe` button and this time tap the `Advanced Wipe` button. |
        | 4 | Select `Cache`, `System` and `Data` and swipe the slider `Swipe to Wipe` at the bottom of the screen. |

        ### On your Windows machine

        | Steps | Description |
        | :------: | ------ |
        | 1 | To make sure the computer can detect your phone, type the following terminal command:<br> `fastboot devices` |
        | 2 | The terminal should prompt something similar to this:<br><br>`$ 0077fxe89p12  fastboot`|
        | 3 | Run the following commands to navigate to the folder containing the LineageOS for microG file (adjust accordingly) and copy the .`.zip` file to the internal storage of your phone:<br> `cd /Downloads`<br>`adb push lineage-XX.X-202XXXXX-microG-CODENAME.zip /sdcard/`<br><br>Alternatively, copy the `.zip` file manually from your computer to your phone, using the file manager.|
        | 4 | Run the following command to reboot the phone into *recovery* mode:<br> `adb reboot recovery` <br><br>Alternatively, reboot the phone into *recovery* mode by powering it off, then holding down both `Volume UP` and `POWER` buttons. Release them once the logo appears on the screen. The combination of buttons might change from device to device. More information [here](https://www.xda-developers.com/how-to-boot-to-recovery/).|

        ### On your Android phone

        | Steps | Description |
        | :------: | ------ |
        | 1 | Navigate to TWRP's main screen and tap on the `Install` button. Choose the LineageOS file `lineage-XX.X-202XXXXX-microG-CODENAME.zip`. |
        | 2 | Swipe the slide `Swipe to confirm Flash`, wait until LineageOS for microG is installed and reboot the phone (this time, a "normal" reboot, not a "bootloader" or "recovery" reboot). |
        | 3 | The first start can take a while. Congrats, LineageOS for microG is installed on your phone! |



=== "macOS"

    ??? tip "Show me the step-by-step guide for macOS"

        ### On your macOS machine

        | Steps | Description |
        | :------: | ------ |
        | 1 | [Find out which codename is associated with your phone model on the LineageOS website](https://wiki.lineageos.org/devices/). For example, the Fairphone's codename is "FP2", the Nexus 5X is called "bullhead", the Samsung Galaxy S9 is called "starlte", and so on. |
        | 2 | [Look for the same codename on the LineageOS for microG website](https://download.lineage.microg.org/). Click on the corresponding folder and download the latest version of LineageOS for microG. For the purpose of this tutorial, let's assume it has been downloaded to the location `/Downloads/lineage-XX.X-202XXXXX-microG-CODENAME.zip`. |
        | 3 | Open the terminal. |
        | 4 | Run the following command to reboot the phone into *recovery* mode:<br> `adb reboot recovery`<br><br> Alternatively, reboot the phone into *recovery* mode by powering it off, then holding down both `Volume UP` and `POWER` buttons. Release them once the logo appears on the screen. The combination of buttons might change from device to device. More information [here](https://www.xda-developers.com/how-to-boot-to-recovery/). |

        ### On your Android phone

        | Steps | Description |
        | :------: | ------ |
        | 1 | TWRP custom recovery should now load. Swipe the slider to enter the main screen. |
        | 2 | Tap the `Wipe` button and then the `Format Data` button. **Caution!** This will remove encryption and delete all files stored in the internal storage. [Make sure you backed up your data](https://gofoss.today/backups/)! |
        | 3 | Return to the previous menu, tap the `Wipe` button and this time tap the `Advanced Wipe` button. |
        | 4 | Select `Cache`, `System` and `Data` and swipe the slider `Swipe to Wipe` at the bottom of the screen. |

        ### On your macOS machine

        | Steps | Description |
        | :------: | ------ |
        | 1 | To make sure the computer can detect your phone, type the following terminal command:<br> `sudo fastboot devices` |
        | 2 | The terminal should prompt something similar to this:<br>`$ 0077fxe89p12  fastboot`|
        | 3 | Run the following commands to navigate to the folder containing the LineageOS for microG file (adjust accordingly) and copy the `.zip` file to the internal storage of your phone:<br> `cd /Downloads`<br>`sudo adb push lineage-XX.X-202XXXXX-microG-CODENAME.zip /sdcard/`<br><br>Alternatively, copy the `.zip` file manually from your computer to your phone, using the file manager.|
        | 4 | Run the following command to reboot the phone into *recovery* mode:<br>`adb reboot recovery` <br><br>Alternatively, reboot the phone into *recovery* mode by powering it off, then holding down both `Volume UP` and `POWER` buttons. Release them once the logo appears on the screen. The combination of buttons might change from device to device. More information [here](https://www.xda-developers.com/how-to-boot-to-recovery/).|

        ### On your Android phone

        | Steps | Description |
        | :------: | ------ |
        | 1 | Navigate to TWRP's main screen and tap on the `Install` button. Choose the LineageOS file `lineage-XX.X-202XXXXX-microG-CODENAME.zip`. |
        | 2 | Swipe the slide `Swipe to confirm Flash`, wait until LineageOS for microG is installed and reboot the phone (this time, a "normal" reboot, not a "bootloader" or "recovery" reboot). |
        | 3 | The first start can take a while. Congrats, LineageOS for microG is installed on your phone! |


=== "Linux (Ubuntu)"

    ??? tip "Show me the step-by-step guide for Linux (Ubuntu)"

        ### On your Linux (Ubuntu) machine

        | Steps | Description |
        | :------: | ------ |
        | 1 | [Find out which codename is associated with your phone model on the LineageOS website](https://wiki.lineageos.org/devices/). For example, the Fairphone's codename is "FP2", the Nexus 5X is called "bullhead", the Samsung Galaxy S9 is called "starlte", and so on. |
        | 2 | [Look for the same codename on the LineageOS for microG website](https://download.lineage.microg.org/). Click on the corresponding folder and download the latest version of LineageOS for microG. For the purpose of this tutorial, let's assume it has been downloaded to the location `/home/gofoss/Downloads/lineage-XX.X-202XXXXX-microG-CODENAME.zip`. |
        | 3 | Open the terminal with the shortcut `CTRL + ALT + T`, or click on the `Applications` button on the top left and search for `Terminal`. |
        | 4 | Run the following command to reboot the phone into *recovery* mode:<br> `adb reboot recovery`<br><br> Alternatively, reboot the phone into *recovery* mode by powering it off, then holding down both `Volume UP` and `POWER` buttons. Release them once the logo appears on the screen. The combination of buttons might change from device to device. More information [here](https://www.xda-developers.com/how-to-boot-to-recovery/). |


        ### On your Android phone

        | Steps | Description |
        | :------: | ------ |
        | 1 | TWRP custom recovery should now load. Swipe the slider to enter the main screen. |
        | 2 | Tap the `Wipe` button and then the `Format Data` button. **Caution!** This will remove encryption and delete all files stored in the internal storage. [Make sure you backed up your data](https://gofoss.today/backups/)! |
        | 3 | Return to the previous menu, tap the `Wipe` button and this time tap the `Advanced Wipe` button. |
        | 4 | Select `Cache`, `System` and `Data` and swipe the slider `Swipe to Wipe` at the bottom of the screen. |

        ### On your Linux (Ubuntu) machine

        | Steps | Description |
        | :------: | ------ |
        | 1 | To make sure the computer can detect your phone, type the following terminal command:<br> `sudo fastboot devices` |
        | 2 | The terminal should prompt something similar to this:<br>`$ 0077fxe89p12  fastboot`|
        | 3 | Run the following commands to navigate to the folder containing the LineageOS for microG file (adjust accordingly) and copy the `.zip` file to the internal storage of your phone:<br> `cd /home/gofoss/Downloads`<br>`sudo adb push lineage-XX.X-202XXXXX-microG-CODENAME.zip /sdcard/`<br><br>Alternatively, copy the `.zip` file manually from your computer to your phone, using the file manager.|
        | 4 | Run the following command to reboot the phone into *recovery* mode:<br>`adb reboot recovery` <br><br>Alternatively, reboot the phone into *recovery* mode by powering it off, then holding down both `Volume UP` and `POWER` buttons. Release them once the logo appears on the screen. The combination of buttons might change from device to device. More information [here](https://www.xda-developers.com/how-to-boot-to-recovery/).|

        ### On your Android phone

        | Steps | Description |
        | :------: | ------ |
        | 1 | Navigate to TWRP's main screen and tap on the `Install` button. Choose the LineageOS file `lineage-XX.X-202XXXXX-microG-CODENAME.zip`. |
        | 2 | Swipe the slide `Swipe to confirm Flash`, wait until LineageOS for microG is installed and reboot the phone (this time, a "normal" reboot, not a "bootloader" or "recovery" reboot). |
        | 3 | The first start can take a while. Congrats, LineageOS for microG is installed on your phone! |



<br>

<center> <img src="../../assets/img/separator_microg.svg" alt="LineageOS microG" width="150px"></img> </center>

## microG

LineageOS for microG obviously ships with... [microG](https://microg.org/). It's an open source replacement for Google's Play Services, which allows you to use features like push notifications or location without continually uploading your data to Google's servers.


=== "Location"

    To tell your phone to establish its location based on a local database with cell tower information and be completely independent from third party providers, follow the instructions detailed below.

    ??? tip "Show me the step-by-step guide"

        <center>

        | Steps | Description |
        | :------: | ------ |
        | 1 | Open F-droid and install an app called `Local GSM location`. |
        | 2 | Head over to the microG settings app. Navigate to `Location modules` and enable `GSM Location Service` as well as `Nominatim`. |
        | 3 | Navigate to `Self-Check` and verify whether everything is set up properly. |

        </center>


=== "Push notifications"

    LineageOS for microG avoids using Google's services as much as possible. There's one exception: many apps rely on Google Cloud Messaging (GCM), a proprietary system developed by Google, to push notifications to your device. microG can provide access to push notifications by enabling (limited) use of the GCM service. More details below.

    ??? tip "Show me the step-by-step guide"

        <center>

        | Steps | Description |
        | :------: | ------ |
        | 1 | Open the microG settings app. |
        | 2 | Navigate to `Google device registration` and register your device. |
        | 3 | Navigate to `Google Cloud Messaging` and activate push notifications. |

        </center>

    ??? question "How can I trust LineageOS if it uses Google Cloud Messaging?"

        Registering your device and enabling push notifications is optional. Doing so might provide limited data to Google, such as a unique ID. microG however makes sure to strip away as much identifying bits as possible. Activating push notifications can also enable Google to (partly) read the content of your notifications, depending on how apps use Google Cloud Messaging.


=== "F-Droid"

    [F-Droid](https://f-droid.org/) is an app store exclusively hosting free and open-source applications, as develop in a [previous chapter on FOSS apps](https://gofoss.today/foss-apps/). Make sure to enable microG's repository in F-Droid, as detailed below.

    ??? tip "Show me the step-by-step guide"

        <center>

        | Steps | Description |
        | :------: | ------ |
        |1 |Launch F-Droid. |
        |2 |Navigate to `Settings ► Repositories`. |
        |3 |Enable the microG repository. |

        </center>


=== "Paid apps"

    Installing paid apps without Google's Play Store can be a little tricky. Here a workaround:

    ??? tip "Show me the step-by-step guide"

        <center>

        | Steps | Description |
        | :------: | ------ |
        |1 |[Browse to Google's online play store](https://play.google.com/store/). |
        |2 |Buy apps with an old or disposable Google account. |
        |3 |Log into [Aurora store](https://f-droid.org/en/packages/com.aurora.store/) with the same Google credentials. |
        |4 |Download the purchased apps. |

        </center>


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="LineageOS support" width="150px"></img> </center>

## Support

For further details or questions, refer to [LineageOS's documentation](https://lineage.microg.org/) or ask [LineageOS's community](https://lineageos.org/community/) for help.

<div align="center">
<img src="https://imgs.xkcd.com/comics/xkcd_phone_2.png" alt="LineageOS"></img>
</div>

<br>
