---
template: main.html
title: How to install Ubuntu
description: What is Ubuntu? Is Ubuntu better than Windows? What's the meaning of Ubuntu? What is Ubuntu LTS? How to install Ubuntu?
---

# Ubuntu, a beginner-friendly Linux distribution

<div align="center">
<img src="../../assets/img/ubuntu_desktop.png" alt="How to install Ubuntu" width="600px"></img>
</div>


## Discover Ubuntu

The easiest way to discover [Ubuntu](https://ubuntu.com/) is to run it alongside your current operating system, for example using a Live USB drive or VirtualBox. If Ubuntu isn't to your liking, you can simply switch back to Windows or macOS.

=== "Live USB or DVD"

    To give Ubuntu a quick spin, run it off a Live DVD or USB drive. More details below.

    ??? tip "Show me a step-by-step guide"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Download |Download the latest [long-term support (LTS) version of Ubuntu](https://ubuntu.com/download/desktop). At the time of writing, the latest LTS was Ubuntu 20.04. Check out the [most recent release cycle](https://ubuntu.com/about/release-cycle) for more information.|
        | Burn |Use a [Windows](https://ubuntu.com/tutorials/burn-a-dvd-on-windows#1-overview), [macOS](https://tutorials.ubuntu.com/tutorial/tutorial-burn-a-dvd-on-macos) or [Ubuntu Linux](https://ubuntu.com/tutorials/tutorial/tutorial-burn-a-dvd-on-ubuntu/) machine to burn the downloaded `.iso` file to a DVD. Alternatively, use a [Windows](https://ubuntu.com/tutorials/tutorial/tutorial-create-a-usb-stick-on-windows/), [macOS](https://ubuntu.com/tutorials/create-a-usb-stick-on-macos#1-overview) or [Ubuntu](https://tutorials.ubuntu.com/tutorial/tutorial-create-a-usb-stick-on-ubuntu) machine to create a bootable USB drive. |
        | Restart |Insert the bootable DVD or USB drive and restart the computer. Most machines will boot automatically from the DVD or USB drive. If that's not the case, try repeatedly hitting `F12`, `ESC`, `F2` or `F10` when the computer starts up. This should provide access to the boot menu, where you can select the DVD or USB drive. |
        | Test Ubuntu |Select `Try Ubuntu without installing` from the menu. |

        </center>


=== "VirtualBox"

    [VirtualBox](https://www.virtualbox.org/) is another option to discover Ubuntu while keeping your current operating system. The program creates so-called virtual machines which run inside your Windows or macOS machine. More detailed instructions below.

    ??? tip "Show me a step-by-step guide"

        ### Install VirtualBox

        === "Windows"

            Make sure your computer meets the minimum requirements: 2GHz dual core processor or better, 2 GB RAM or more, 25 GB of free drive space. Download and run the latest [VirtualBox Platform Package for Windows hosts](https://www.virtualbox.org/wiki/Downloads). Open the downloaded `.exe` file and follow the installation wizzard.

        === "macOS"

            Make sure your computer meets the minimum requirements: 2GHz dual core processor or better, 2 GB RAM or more, 25 GB of free drive space. Download the [VirtualBox Platform Package for OS X hosts](https://www.virtualbox.org/wiki/Downloads). Open the downloaded `.dmg` file and drag the VirtualBox icon on top of the Application folder. For easy access, open the Applications folder and drag the VirtualBox icon to your dock.


        ### Configure VirtualBox

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Download Ubuntu | Download the latest [long-term support (LTS) version of Ubuntu Desktop](https://ubuntu.com/download/desktop). At the time of writing, the latest LTS was Ubuntu 20.04. Check out the [most recent release cycle](https://ubuntu.com/about/release-cycle/) for more information. |
        | Create a Virtual Machine | Start VirtualBox and click on the button `New`. |
        | Name and operating system | Give your Virtual Machine (VM) a name, for example `Ubuntu`. Also select the Operating System (`Linux`) and the version, for example `Ubuntu (64-bit)`. |
        | Memory size | Choose how much RAM  to allocate to Ubuntu. 2 GB are recommended, 3-4 GB are even better. |
        | Hard disk |  Select `Create a virtual hard disk now` to add a virtual hard disk. |
        | Hard disk file type | Choose the `VDI` format for the hard disk file type. |
        | Storage on physical hard disk | Choose `Dynamically allocated` for the hard disk file size. |
        | File location and size | Choose where to create the virtual disk. Also, decide how much disk space to allocate to Ubuntu. 10 GB are recommended, more is even better. |
        | Optical Disk Selector |Back on the main screen, click on `Start` to launch the Virtual Ubuntu Machine. In the pop-up dialogue, click on the icon to choose a virtual optical disk file. Click on `Add` and navigate to the location of the downloaded LTS Ubuntu `.iso` file. Click on `Open`, `Choose` and `Start`. After the boot phase, the Ubuntu installation wizzard will show up. |

        </center>


        ### Install Ubuntu in VirtualBox

        <center>

        | Instructions | Description |
        | ------ | ------ |
        |Welcome |Select a language and click on `Install Ubuntu`. |
        |Keyboard layout |Select a keyboard layout. |
        |Updates and other software |Choose between a `normal` or `minimal` installation, depending on how many apps you would like to install from the start. Optionally, check the box `Download updates while installing Ubuntu` to speed up the setup after the installation, and the box `Install third-party software` to benefit from (proprietary) drivers for graphics, WiFi, media files, and so on. Click on `Continue`. |
        |Installation type & encryption |This screen allows to choose whether to delete the existing operating system and replace it with Ubuntu, or whether to install Ubuntu alongside the existing operating system (so-called dual boot). <br><br> Since we're installing Ubuntu in VirtualBox, no other operating system is present. Just choose `Erase disk and install Ubuntu`, click on `Advanced features` and select `Use LVM with the new Ubuntu installation` as well as `Encrypt the new Ubuntu installation`. Then click on `OK` and `Install Now`. |
        |Choose a security key |Choose a [strong and unique security key](https://gofoss.today/passwords/). It will be required to decrypt the disk each time the computer starts up. Also check the box `Overwrite empty disk space` for more security. Click on `Install Now`. Confirm the pop-up window by clicking on `Continue`. <br><br> *Caution*: If you lose the security key, all data will be lost. Store the security key safely. |
        |Where are you? |Choose a time zone and location. Click on `Continue`. |
        |Who are you? |Provide login information, such as a username and [strong, unique password](https://gofoss.today/passwords/). Select `Require my password to log in`. Click on `Continue`.|
        |Restart & login for the first time |Click on `Restart Now` after the successful installation. The Virtual Machine will reboot. You can now log in to your first Ubuntu session by providing the correct security key and password. |
        |Discover Ubuntu| Walk through the initial setup options, and start discovering Ubuntu. Close the session once you've finished. Going forward, you can launch Ubuntu by clicking on the `Start` button on VirtualBox's main screen.|

        </center>


    ??? tip "Show me a summary video"

        <center>
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/5ef6ada2-116b-416d-8d53-10a817d95827" frameborder="0" allowfullscreen></iframe>
        </center>

<div style="margin-top:-20px">
</div>

!!! level "This chapter is geared towards intermediate users. Some tech skills are required."


<br>

<center> <img src="../../assets/img/separator_linux.svg" alt="Ubuntu desktop" width="150px"></img> </center>

## The Ubuntu Desktop

After signing in for the first time, the so-called [GNOME Desktop](https://www.gnome.org/) will load:

<center>

| Gnome Desktop Elements | Description |
| ------ | ------ |
| Top bar | Displays date, time, notifications, battery life, network connections, volume, and so on. |
| Dock | Located on the left, displays shortcuts to your favorite applications, and currently open applications. The dock's location can be changed. |
| App drawer | Located on the bottom left of the dock, provides access to all installed apps. |
| Overview screen | Accessible via the `Activities` button on the top bar, displays all open windows. |
| Search bar | Accessible via the `Activities`, allows to look for apps, files, settings, commands, and so on. |

</center>


<br>

<center> <img src="../../assets/img/separator_php.svg" alt="Ubuntu terminal" width="150px"></img> </center>

## The terminal

The terminal is an interface to execute text based commands. Many new users are put off by the terminal, as it's often associated with some obscure code hacking. In fact, using the terminal is not that hard and can be much faster than navigating the graphical interface. Open the terminal with the shortcut `CTRL + ALT + T`, or click on the `Activities` button on the top left and search for `Terminal`.

??? info "Tell me more about the terminal"

    The terminal is sometimes called shell, console, command line or prompt. It takes some time to get used to, but is quite efficient once you've learned a few tricks. For example, putting `sudo` in front of a command means it's run with administrator privileges. Similar to the security pop-ups in Windows and macOS, which require an administrator password before installing a program or modifying a setting. Here some more reading material to become a terminal-ninja:

    * [Cheat sheet with most used terminal commands](https://drive.google.com/open?id=1d_TEG5M8cbDhSmptYmyXXeNGSyzCbaeC/)
    * [Another cheat sheet with most used terminal commands](https://drive.google.com/open?id=1chCfI9dKEk5xn1EhsuBepCiudX6nKlng/)
    * [Yet another cheat sheet with most used terminal commands](https://drive.google.com/open?id=1qy7feVhsVA1vkuNhsdfFwrWmRE84mZQe/)
    * [Couple of useful Ubuntu terminal commands](https://drive.google.com/open?id=1mU-eGaoTzyeYnLNJUetkNVI4_H-V28sX/)
    * [Couple of useful terminal commands](https://drive.google.com/open?id=1erAJl0C8ypFN3QjhTqR0VGCr4sjenJmp/)
    * [Extensive collection of terminal commands for advanced users](https://drive.google.com/open?id=1ZiX6Leq45ijTEr8MPuWsPtesUkNMqXzm/)
    * [Full book on the Linux Command Line](https://drive.google.com/open?id=1VJa_LGtTaZmOy9H4unzVqFZFCB_CC14B/)



<br>

<center> <img src="../../assets/img/separator_ubuntu.svg" alt="Ubuntu installation" width="150px"></img> </center>

## Install Ubuntu

Start to like Ubuntu? Want to permanently install it on your computer? Great! Just perform some preliminary checks and follow the installation instructions below.


??? tip "Show me the list of preliminary checks"

    <center>

    | Checklist | Description |
    | ------ | ------ |
    | Is my device compatible with Linux? |• [Test it](https://gofoss.today/ubuntu#discover-ubuntu/) with a Live USB or VirtualBox <br>• Check the [compatibility database](https://ubuntu.com/certified) <br>• [Ask](https://search.disroot.org/) the Internet <br>• Buy a [Linux compatible computer](https://linuxpreloaded.com/) |
    | Does my device fulfill the minimum requirements? |• 2 GHz dual core processor <br>• 4 GB system memory (RAM) <br>• 25 GB of free storage space (Ubuntu takes approx. 5 GB, keep at least 20 GB for your data) |
    | Is my device plugged in? | If you install Ubuntu on a mobile device such as a laptop, make sure it's plugged in. |
    | Is the installation medium accessible? | Check if your computer has either a DVD drive or a free USB port. |
    | Is my device connected to the Internet? | Check if the Internet connection is up and running. |
    | Have I backed up my data? | [Back up your data](https://gofoss.today/backups/), since there is a (small but real) risk of data loss during the installation process! |
    | Have I downloaded the latest Ubuntu version? | Download the [latest long-term support (LTS) version of Ubuntu](https://ubuntu.com/download/desktop), which is supported for 5 years, including security and maintenance updates. At the time of writing, the latest LTS was Ubuntu 20.04. Check out the [most recent release cycle](https://ubuntu.com/about/release-cycle/) for more information. |
    | Have I prepared a bootable device? | Use a [Windows](https://ubuntu.com/tutorials/burn-a-dvd-on-windows#1-overview), [macOS](https://tutorials.ubuntu.com/tutorial/tutorial-burn-a-dvd-on-macos) or [Ubuntu Linux](https://ubuntu.com/tutorials/tutorial/tutorial-burn-a-dvd-on-ubuntu/) machine to burn the downloaded `.iso` file to a DVD. Alternatively, use a [Windows](https://ubuntu.com/tutorials/tutorial/tutorial-create-a-usb-stick-on-windows/), [macOS](https://ubuntu.com/tutorials/create-a-usb-stick-on-macos#1-overview) or [Ubuntu](https://tutorials.ubuntu.com/tutorial/tutorial-create-a-usb-stick-on-ubuntu) machine to create a bootable USB drive. |

    </center>


??? tip "Show me the step-by-step guide"

    <center>

    | Instruction | Description |
    | ------ | ------ |
    |Boot |Insert the bootable DVD or USB drive and restart the computer. Most machines will boot automatically from the DVD or USB drive. If that's not the case, try repeatedly hitting `F12`, `ESC`, `F2` or `F10` when the computer starts up. This should provide access to the boot menu, where you can select the DVD or USB drive. |
    |Welcome |Once the device has booted, you will be presented with the installation wizzard. Select a language and click on `Install Ubuntu`. |
    |Keyboard layout |Select a keyboard layout. |
    |Updates and other software |Choose between a `normal` or `minimal` installation, depending on how many apps you would like to install from the start. Optionally: <br><br>• Check the box `Download updates while installing Ubuntu` to speed up the setup after the installation <br>• Check the box `Install third-party software` to benefit from (proprietary) drivers for graphics, WiFi, media files, and so on.<br><br> Next, click on `Continue`. |
    |Installation type & encryption |Choose whether to: <br>• Delete the existing operating system and replace it with Ubuntu. *Caution!*: this will erase all data on your hard drive! Make sure you [backed up your data](https://gofoss.today/backups/)! <br>• Or install Ubuntu alongside the existing operating system (so-called dual boot). This should have no incidence on the existing configuration of your computer. Still, make sure [backed up your data](https://gofoss.today/backups/), you never know... <br><br> To encrypt Ubuntu, click on `Advanced features` and select: <br>• `Use LVM with the new Ubuntu installation` <br>• `Encrypt the new Ubuntu installation` <br><br>Then click on `OK` and `Install Now`. |
    |Provide a security key |Enter a [strong, unique security key](https://gofoss.today/passwords/). It will be required to decrypt the disk each time the computer starts up. Then click on `Continue`. <br><br>*Caution*: If you lose this security key, all data will be lost. Store it safely. |
    |Where are you? |Choose a time zone and location. Click on `Continue`. |
    |Who are you? |Provide login information, such as a user name and a [strong, unique password](https://gofoss.today/passwords/). Select `Require my password to log in` for more security. Then click on `Continue`.|
    |Restart & login for the first time |That's it. Wait for the installation to finish, remove the USB key and click on `Restart Now` when prompted. After the reboot, log into Ubuntu with your security key and password. |

    </center>


??? tip "Show me a summary video"

    <center>

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/b8252dab-255f-42c7-885e-711d9a24da85" frameborder="0" allowfullscreen></iframe>

    </center>


<br>

<center> <img src="../../assets/img/separator_davx5.svg" alt="Ubuntu system updates" width="150px"></img> </center>

## System updates

Keep Ubuntu up to date and regularly install the latest security patches, bug fixes and application upgrades. More detailed instructions below.

??? tip "Show me a step-by-step guide"

    Open the terminal with the `CTRL + ALT + T` shortcut, or click on the `Activities` button on the top left and search for `Terminal`. Then run the following command:

    ```bash
    sudo apt update && sudo apt upgrade -y
    ```

    The first part of the command `sudo apt update` checks if new software versions are available. The second part of the command `sudo apt upgrade` installs the newest updates. The `-y` at the end of the command authorises the installation of new packages.

    If you prefer not to use the terminal, click on the `Activities` button on the top left of the screen, and search for `Software Updater`. It will check if there are updates available and suggest to install them.

    Once the system is up to date, clean up and remove unnecessary old packages with the following terminal commands:

    ```bash
    sudo apt autoremove
    sudo apt autoclean
    sudo apt clean
    ```

<br>

<center> <img src="../../assets/img/separator_bug.svg" alt="Ubuntu bug reports" width="150px"></img> </center>

## Bug reports

[Apport](https://wiki.ubuntu.com/Apport/) is Ubuntu's bug reporting system. It intercepts crashes and files bug reports. You might want to turn it off for privacy reasons. More detailed instructions below.

??? tip "Show me a step-by-step guide"

    Open the terminal with the `CTRL + ALT + T` shortcut, or click on the `Activities` button on the top left and search for `Terminal`. Then run the following commands to completely remove the bug report functionality:

    ```bash
    sudo apt purge apport
    sudo rm /etc/cron.daily/apport
    ```

    If you prefer not to use the terminal, open the Ubuntu Software Centre, search for `Apport` and click `Remove`. To merely disable the automatic bug report while still keeping the functionality, open a terminal and type `sudo systemctl disable apport.service`. Then open the configuration file with the command `sudo gedit /etc/default/apport` and set the value `enabled` to zero, i.e. `enabled=0`.


<br>

<center> <img src="../../assets/img/separator_cpuinfo.svg" alt="Ubuntu codecs" width="150px"></img> </center>

## Codecs

Codecs tell your computer how to read video or audio files. For legal and ethical reasons, some Linux distributions don't include all multimedia codecs. Sometimes it's therefore necessary to install additional codecs to view particular file formats. More detailed instructions below.

??? tip "Show me a step-by-step guide"

    Open the terminal with the `CTRL + ALT + T` shortcut, or click on the `Activities` button on the top left and search for `Terminal`. Run the following commands to fully enjoy your multimedia experience:

    ```bash
    sudo add-apt-repository "deb http://archive.canonical.com/ubuntu $(lsb_release -cs) partner"
    sudo apt update
    sudo apt install ubuntu-restricted-extras adobe-flashplugin browser-plugin-freshplayer-pepperflash libavcodec-extra libdvd-pkg
    ```

<br>

<center> <img src="../../assets/img/separator_aurora.svg" alt="Ubuntu software center" width="150px"></img> </center>

## Software Center

As of Ubuntu 20.04, the traditional software center has been replaced with Snap, a new technology to deliver applications bundled inside one single file. If you prefer reverting back to the Software Center, follow the detailed instructions below.

??? tip "Show me a step-by-step guide"

    Open the terminal with the `CTRL + ALT + T` shortcut, or click on the `Activities` button on the top left and search for `Terminal`. Then run the following commands:

    ```bash
    sudo apt remove snap-store
    sudo apt install gnome-software
    ```

    If you want the Software Center to support Snap as well as Flatpak packages, run the following commands:

    ```bash
    sudo apt install gnome-software-plugin-snap
    sudo apt install gnome-software-plugin-flatpak
    ```

<br>

<center> <img src="../../assets/img/separator_gimp.svg" alt="Ubuntu Gnome Tweaks" width="150px"></img> </center>

## Look & feel

Ubuntu allows to customise almost every visual aspect of the operating system, such as fonts, window styles, animations, themes, icons, and so on. More detailed instructions below.

??? tip "Show me a step-by-step guide"

    Open the terminal with the `CTRL + ALT + T` shortcut, or click on the `Activities` button on the top left and search for `Terminal`. Run the following command to install [Gnome Tweaks](https://wiki.gnome.org/Apps/Tweaks/) and get full control over Ubuntu's visuals:

    ```bash
    sudo apt install gnome-tweaks
    ```

    Alternatively, click on the `Activities` button on the top left and search for `Software`. Now look for `Gnome Tweaks` and click on `Install`.


<br>

<center> <img src="../../assets/img/separator_settings.svg" alt="Ubuntu graphic drivers" width="150px"></img> </center>

## Graphic drivers

The vast majority of Linux distributions ships with open source graphic drivers out of the box. While this is sufficient to run standard applications, it's often not enough for gaming. To install proprietary graphic drivers, click on the `Activities` button on the top left of the screen and type `Additional Drivers`. Choose the correct graphical driver (most of the time, it's the default option) and restart your system.


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Ubuntu support" width="150px"></img> </center>

## Support

For further details or questions, refer to [Ubuntu's documentation](https://help.ubuntu.com/), [Ubuntu's tutorials](https://ubuntu.com/tutorials/), [Ubuntu's wiki](https://wiki.ubuntu.com/) or ask [Ubuntu's beginner-friendly community](https://askubuntu.com/) for help.

<div align="center">
<img src="https://imgs.xkcd.com/comics/sandwich.png" alt="Ubuntu Desktop"></img>
</div>

<br>
