---
template: main.html
title: A list of 75 privacy respecting cloud services
description: Privacy respecting cloud providers. Alternative cloud providers. Framasoft, Disroot, Riseup, Systemausfall, Colibris.
---

# 75 privacy respecting cloud services

<div align="center">
<img src="../../assets/img/cloud_storage_2.png" alt="Alternative cloud providers" width="550px"></img>
</div>

!!! level "This chapter is geared towards beginners. No particular tech skills are required."

## Alternative providers

=== "Framasoft"

    <img src="../../assets/img/framasoft.png" alt="Framasof" width="80px" align="left"></img> [Framasoft](https://framasoft.org/en/) is a French non-profit, created in 2001. It promotes FOSS, [offers various online services](https://degooglisons-internet.org/en/list/) and [recommends privacy-respecting software](https://degooglisons-internet.org/en/alternatives/). [Personal data collected by Framasoft is subject to French law](https://framasoft.org/en/legals/), servers are located in Germany.

=== "Kitten"

    <img src="../../assets/img/chatons.png" alt="Chatons" width="70px" align="left"></img> [Kitten (or Chatons)](https://www.chatons.org/) is a French collective of alternative cloud provides. It promotes transparent, open, neutral and sustainable [online services](https://www.chatons.org/en/search/by-service/). [Personal data collected by Chatons is subject to French law](https://www.chatons.org/mentions-legales/). Chaton's servers are located in France and Germany.


=== "Disroot"

    <img src="../../assets/img/disroot.png" alt="Disroot" width="70px" align="left"></img> [Disroot](https://disroot.org/) is a Dutch volunteer-run project, created in 2015. It offers [various online services](https://disroot.org/en/#services), most of which are free to use. [Disroot claims to be GDPR compliant](https://disroot.org/en/privacy_policy/), servers are located in the Netherlands. Disroot's [Android app](https://f-droid.org/en/packages/org.disroot.disrootapp/) provides mobile access to most of its services.


=== "Digitalcourage"

    <img src="../../assets/img/digitalcourage.png" alt="Digitalcourage" width="70px" align="left"></img> [Digitalcourage](https://digitalcourage.de/en) is a German association, created in 1987. It offers [various online services](https://digitalcourage.de/swarm-support), most of which are free to use. [Digitalcourage claims to be GDPR compliant](https://digitalcourage.de/datenschutz-bei-digitalcourage), servers are managed by the Ireland-based company [freistil.it](https://www.freistil.it/).


=== "RiseUp"

    <img src="../../assets/img/riseup.png" alt="RiseUp" width="80px" align="left"></img> [RiseUp](https://riseup.net/) is a volunteer-run collective, based in Seattle. It was created in 1999 and provides various services and computer resources. [RiseUp publishes a transparent privacy policy](https://riseup.net/en/privacy-policy).

=== "Systemausfall"

    <img src="../../assets/img/systemausfall.png" alt="Systemausfall" width="178px" align="left"></img> [Systemausfall](https://systemausfall.org/) is a volunteer-run collective, based in Germany. It was created in 2003. It provides various services and computer resources. [Systemausfall publishes a transparent privacy policy](https://systemausfall.org/wikis/hilfe/Datensicherheit/).

=== "Colibris"

    <img src="../../assets/img/colibris.png" alt="Colibris" width="70px" align="left"></img> [Colibris](https://www.colibris-lemouvement.org/) is a French non-profit, created in 2007. It promotes a more ecological and solidarity-based way of life. Colibris also [offers various online services](https://www.colibris-outilslibres.org/). [Personal data collected by Colibri is subject to French law](https://www.colibris-outilslibres.org/conditions-generales-d-utilisation/). Colibri's servers are located in Germany.

=== "Picasoft"

    <img src="../../assets/img/picasoft.png" alt="Picasoft" width="70px" align="left"></img> [Picasoft](https://picasoft.net/) is a French non-profit from the Technical University of Compiègne, created in 2016. It promotes a free, inclusive and privacy-minded way of life. Picasoft offers various online services. [Personal data collected by Picasoft is subject to French law](https://picasoft.net/co/cgu.html). Picasoft's servers are located in [France](https://wiki.picasoft.net/doku.php?id=technique:resume).




## Office suite

=== "Text Editor"

    <img src="../../assets/img/editor.svg" alt="Online text editor" width="50px" align="left"></img> Replace Google Docs, Word365 and the like:

    |FOSS Alternative |Description |
    | ------ | ------ |
    |[Framapad](https://framapad.org/)   |Collaborative online text editor, powered by [etherpad](https://etherpad.org/)|
    |[Disroot pad](https://pad.disroot.org/) |Collaborative online text editor, powered by [etherpad](https://etherpad.org/) |
    |[Disroot cryptpad](https://cryptpad.disroot.org/pad/) |Collaborative & encrypted online text editor, powered by [cryptpad](http://cryptpad.fr) |
    |[Digitalcourage cryptpad](https://cryptpad.digitalcourage.de/) |Collaborative & encrypted online text editor, powered by [cryptpad](http://cryptpad.fr) |
    |[Riseup pad](https://pad.riseup.net/)|Collaborative online text editor, powered by [etherpad](https://etherpad.org/) |
    |[Colibri pad](https://pad.colibris-outilslibres.org/)|Collaborative online text editor, powered by [etherpad](https://etherpad.org/) |
    |[Picapad](https://pad.picasoft.net/)|Collaborative online text editor, powered by [etherpad](https://etherpad.org/)|
    |[Zaclys cloud docs](https://www.zaclys.com/cloud-2/) |Collaborative online text editor, powered by [nextcloud](https://nextcloud.com/) & [onlyoffice](https://www.onlyoffice.com) |
    |[Kitten services](https://www.chatons.org/en/search/by-service?service_type_target_id=135&title=&field_software_target_id=All&field_is_shared_value=All) |Various collaborative online text editors, powered by [cryptpad](http://cryptpad.fr), [etherpad](https://etherpad.org/) or [hedgedoc](https://hedgedoc.org/) |


=== "Spreadsheet"

    <img src="../../assets/img/spreadsheet.svg" alt="Online spreadsheet" width="50px" align="left"></img> Replace Google Spreadsheet, Excel365 and the like:

    |FOSS Alternative |Description |
    | ------ | ------ |
    |[Framacalc](https://framacalc.org/) |Collaborative online spreadsheet editor, powered by [ethercalc](https://github.com/audreyt/ethercalc/) |
    |[Disroot calc](https://calc.disroot.org/) |Collaborative online spreadsheet editor, powered by [ethercalc](https://github.com/audreyt/ethercalc/) |
    |[Disroot sheet](https://cryptpad.disroot.org/sheet/) |Collaborative & encrypted online spreadsheet editor, powered by [cryptpad](http://cryptpad.fr) |
    |[Digitalcourage sheet](https://cryptpad.digitalcourage.de/) |Collaborative & encrypted online spreadsheet editor, powered by [cryptpad](http://cryptpad.fr) |
    |[Zaclys cloud sheets](https://www.zaclys.com/cloud-2/) |Collaborative online spreadsheet editor, powered by [nextcloud](https://nextcloud.com/) & [onlyoffice](https://www.onlyoffice.com/) |
    |[Kitten services](https://www.chatons.org/en/search/by-service?service_type_target_id=136&title=&field_software_target_id=All&field_is_shared_value=All) |Various collaborative online spreadsheet editors, powered by [ethercalc](https://github.com/audreyt/ethercalc/) |


=== "Slides"

    <img src="../../assets/img/slide.svg" alt="Online slides" width="50px" align="left"></img> Replace Google Slides, Powerpoint365 and the like:

    |FOSS Alternative |Description |
    | ------ | ------ |
    |[Disroot slides](https://cryptpad.disroot.org/slide/) |Collaborative & encrypted online slide editor, powered by [cryptpad](http://cryptpad.fr) |
    |[Digitalcourage cryptpad](https://cryptpad.digitalcourage.de/) |Collaborative & encrypted online slide editor, powered by [cryptpad](http://cryptpad.fr) |
    |[Zaclys cloud slides](https://www.zaclys.com/cloud-2/) |Collaborative online slide editor, powered by [nextcloud](https://nextcloud.com/) & [onlyoffice](https://www.onlyoffice.com/) |

=== "Calendar"

    <img src="../../assets/img/simplecalendar.svg" alt="Online calendar" width="50px" align="left"></img> Replace Google Calendar, Outlook365 and the like:

    |FOSS Alternative |Description |
    | ------ | ------ |
    |[Framagenda](https://framagenda.org/login/) |Collaborative online calendar, powered by [nextcloud](https://nextcloud.com/) |
    |[Cryptpad calendar](https://cryptpad.fr/calendar/) |Collaborative & encrypted online calendar, powered by [cryptpad](http://cryptpad.fr) |
    |[Zaclys cloud calendar](https://www.zaclys.com/cloud-2/)|Collaborative online calendar, powered by [nextcloud](https://nextcloud.com/) |

=== "Meeting Scheduling"

    <img src="../../assets/img/doodle.svg" alt="Meeting scheduling" width="50px" align="left"></img> Replace Doodle and the like:

    |FOSS Alternative |Description |
    | ------ | ------ |
    |[Framadate](https://framadate.org/) |Online service for planning & decision-making, powered by [studs](https://sourcesup.cru.fr/projects/studs/) |
    |[Disroot poll](https://poll.disroot.org/) |Online service for planning & decision-making, powered by [studs](https://sourcesup.cru.fr/projects/studs/) |
    |[Digitalcourage poll](https://nuudel.digitalcourage.de/) |Online service for planning & decision-making, powered by [studs](https://sourcesup.cru.fr/projects/studs/) |
    |[Colibris poll](https://date.colibris-outilslibres.org/) |Online service for planning & decision-making, powered by [studs](https://sourcesup.cru.fr/projects/studs/) |
    |[Kitten services](https://www.chatons.org/en/search/by-service?service_type_target_id=139&title=&field_software_target_id=All&field_is_shared_value=All) |Various online services for planning & decision-making, powered by [studs](https://sourcesup.cru.fr/projects/studs/) |


## File storage & sync

=== "Cloud Drive"

    <img src="../../assets/img/mysql.svg" alt="Drive" width="50px" align="left"></img> Replace Google Drive, Dropbox, iCloud, OneDrive and the like:

    |FOSS Alternative |Description |
    | ------ | ------ |
    |[Disroot cloud](https://cloud.disroot.org/) |Cloud storage solution, powered by [nextcloud](https://nextcloud.com/) |
    |[Systemausfall drive](https://speicher.systemausfall.org/accounts/login/?next=) |Cloud storage solution, powered by [seafile](https://www.seafile.com/en/home/) |
    |[Zaclys cloud](https://www.zaclys.com/cloud-2/) |Cloud storage solution, powered by [nextcloud](https://nextcloud.com/) |
    |[Kitten services](https://www.chatons.org/en/search/by-service?service_type_target_id=147&title=&field_software_target_id=All&field_is_shared_value=All) |Various cloud storage solutions, powered by [nextcloud](https://nextcloud.com/) or [seafile](https://www.seafile.com/)|
    |[Disroot cryptdrive](https://cryptpad.disroot.org/drive/)|Encrypted cloud storage solution, powered by [cryptpad](http://cryptpad.fr) |
    |[Digitalcourage cryptdrive](https://cryptpad.digitalcourage.de/drive/)|Encrypted cloud storage solution, powered by [cryptpad](http://cryptpad.fr) |

=== "File Sharing"

    <img src="../../assets/img/davx5.svg" alt="File Sharing" width="50px" align="left"></img> Replace WeTransfer and the like:

    |FOSS Alternative |Description |
    | ------ | ------ |
    |[Disroot upload](https://upload.disroot.org/) |Encrypted file sharing solution, powered by [lufi](https://framagit.org/fiat-tux/hat-softwares/lufi/)|
    |[Riseup share](https://share.riseup.net/) |File sharing solution, powered by [up1](https://github.com/Upload/Up1/)|
    |[Systemausfall share](https://teilen.systemausfall.org/login/)|Encrypted file sharing solution, powered by [lufi](https://framagit.org/fiat-tux/hat-softwares/lufi/)|
    |[PicaDrop](https://drop.picasoft.net/) |Encrypted file sharing solution, powered by [lufi](https://framagit.org/fiat-tux/hat-softwares/lufi/)|
    |[Standardnotes filesend](https://filesend.standardnotes.com)|File sharing solution, powered by [standardnotes](https://github.com/standardnotes/filesend)|
    |[Zaclys share](https://www.zaclys.com/envoi/) |File sharing solution |
    |[Kitten services](https://www.chatons.org/en/search/by-service?service_type_target_id=148&title=&field_software_target_id=All&field_is_shared_value=All) |Various file sharing solutions, powered by [lufi](https://framagit.org/fiat-tux/hat-softwares/lufi/), [plik](https://github.com/root-gg/plik/), [file2link](https://framagit.org/kepon/file2link/) or [firefox send (fork)](https://forge.april.org/Chapril/drop.chapril.org-firefoxsend/) |


## Chats & forums

=== "Video & Voice Calls"

    <img src="../../assets/img/jitsi.svg" alt="Jitsi" width="50px" align="left"></img> Replace Skype, Google Hangouts, Zoom and the like:

    |FOSS Alternative |Description |
    | ------ | ------ |
    |[Framatalk](https://framatalk.org/accueil/en/) |Secure video & audio conferencing solution, powered by [jitsi meet](https://meet.jit.si/)|
    |[Disroot calls](https://calls.disroot.org/) |Secure video & audio conferencing solution, powered by [jitsi meet](https://meet.jit.si/) |
    |[Colibris visio](https://visio.colibris-outilslibres.org/) |Secure video & audio conferencing solution, powered by [jitsi meet](https://meet.jit.si/) |
    |[Picasoft voice](https://framatalk.org/accueil/en/)|Audio conferencing solution, powered by [mumble](https://www.mumble.info/)|
    |[Kitten services](https://www.chatons.org/en/search/by-service?service_type_target_id=117&title=&field_software_target_id=All&field_is_shared_value=All) |Various secure video & audio conferencing solutions, powered by [jitsi meet](https://meet.jit.si/), [nextcloud](https://nextcloud.com/) or [big blue button](https://bigbluebutton.org/) |

    *Remark*: note that Jitsi supports [end-to-end encrypted video calls](https://jitsi.org/e2ee-in-jitsi/)

=== "Web Chats"

    <img src="../../assets/img/signal.svg" alt="Web chats" width="50px" align="left"></img> Replace Teams, Facebook Groups, Slack and the like:

    |FOSS Alternative |Description |
    | ------ | ------ |
    |[Framateam](https://framateam.org/login/)  |Team chat solution, powered by [mattermost](https://mattermost.com/) |
    |[Disroot web chat](https://webchat.disroot.org/) |Team chat solution, powered by [xmpp](https://conversejs.org/) |
    |[Systemausfall chat](https://klax.systemausfall.org/) |Team chat solution, powered by [matrix](https://matrix.org/) |
    |[Colibri chat](https://tchat.colibris-outilslibres.org/) |Team chat solution, powered by [mattermost](https://mattermost.com/) |
    |[Picateam](https://team.picasoft.net/login)|Team chat solution, powered by [mattermost](https://mattermost.com/) |
    |[Kitten services](https://www.chatons.org/en/search/by-service?service_type_target_id=118&title=&field_software_target_id=All&field_is_shared_value=All) |Various team chat solutions, powered by [matrix](https://matrix.org/), [mattermost](https://mattermost.com/) or [rocket chat](https://rocket.chat/) |

=== "Mailing Lists"

    <img src="../../assets/img/pidgin.svg" alt="Mailing lists" width="50px" align="left"></img> Replace Google Groups and the like:

    |FOSS Alternative |Description |
    | ------ | ------ |
    |[Framalistes](https://framalistes.org/sympa/) |Mailing list service, powered by [sympa](https://www.sympa.org/)|
    |[Riseup lists](https://lists.riseup.net/) |Mailing list service, powered by [sympa](https://www.sympa.org/)    |
    |[Systemausfall lists](https://wat.systemausfall.org/) |Mailing list service, powered by [sympa](https://www.sympa.org/) |
    |[Kitten services](https://www.chatons.org/en/search/by-service?service_type_target_id=114&title=&field_software_target_id=All&field_is_shared_value=All) |Various mailing list services, powered by [sympa](https://www.sympa.org/) |


## Social networks

=== "Social Networks"

    <img src="../../assets/img/permissions.svg" alt="Fediverse" width="50px" align="left"></img> Replace Facebook and the like:

    |FOSS Alternative |Description |
    | ------ | ------ |
    |[Diaspora](https://diasporafoundation.org/) |Decentralized, free & private social network. Powered by [activitypub](https://activitypub.rocks/), part of the [fediverse](https://gofoss.today/fediverse/)|
    |[Friendica](https://friendi.ca/) |Decentralized, private & interoperable social network. Powered by [activitypub](https://activitypub.rocks/), part of the [fediverse](https://gofoss.today/fediverse/) |
    |[GNU Social](https://gnusocial.network/)|Free & open source social network. Powered by [activitypub](https://activitypub.rocks/), part of the [fediverse](https://gofoss.today/fediverse/) |
    |[Minds](https://www.minds.com/) |Free & open-source, encrypted and reward-based social network |

=== "Microblogging"

    <img src="../../assets/img/tusky.svg" alt="Mastodon" width="50px" align="left"></img> Replace Twitter and the like:

    |FOSS Alternative |Description |
    | ------ | ------ |
    |[Mastodon](https://joinmastodon.org/) |Decentralized microblogging platform to share text posts, pictures, audio, video or polls. Powered by [activitypub](https://activitypub.rocks/), part of the [fediverse](https://gofoss.today/fediverse/) |
    |[Nitter](https://nitter.net/)|Free & open source front-end to Twitter. No ads, no tracking, no Twitter |

=== "Blogging"

    <img src="../../assets/img/apache.svg" alt="Mastodon" width="50px" align="left"></img> Replace Medium, WordPress and the like:

    |FOSS Alternative |Description |
    | ------ | ------ |
    |[WriteFreely](https://writefreely.org/)|Decentralized & open source blogging platform. Powered by [activitypub](https://activitypub.rocks/), part of the [fediverse](https://gofoss.today/fediverse/) |

=== "Events"

    <img src="../../assets/img/simplecalendar.svg" alt="Mobilizon" width="50px" align="left"></img> Replace Facebook events, groups and the like:

    |FOSS Alternative |Description|
    | ------ | ------ |
    |[Mobilizon](https://joinmobilizon.org/en/) |Create, find & organize events. Powered by [activitypub](https://activitypub.rocks/), part of the [fediverse](https://gofoss.today/fediverse/) |

=== "Forums"

    <img src="../../assets/img/redreader.svg" alt="Lemmy" width="50px" align="left"></img> Replace Reddit and the like:

    |FOSS Alternative |Description |
    | ------ | ------ |
    |[Lemmy](https://join.lemmy.ml/) |Online discussion forums. Powered by [activitypub](https://activitypub.rocks/), part of the [fediverse](https://gofoss.today/fediverse/) |
    |[Aether](https://getaether.net/) |Online discussion forums. Open source, peer-to-peer & ephemeral    |
    |[Teddit](https://teddit.net/)|Free & open source front-end to Reddit. No ads, no tracking, no Reddit |


## Social media

=== "Videos"

    <img src="../../assets/img/totem.svg" alt="Peertube" width="50px" align="left"></img> Replace Youtube, Vimeo, Dailymotion and the like:

    |FOSS Alternative |Description |
    | ------ | ------ |
    |[Peertube](https://joinpeertube.org/) |Decentralized, free & open source video sharing platform. Powered by [activitypub](https://activitypub.rocks/), part of the [fediverse](https://gofoss.today/fediverse/) |
    |[Dtube](https://d.tube/) |Decentralized, free & open source video sharing platform |
    |[Invidious](https://invidious.io/)|Free & open source front-end to YouTube. No ads, no tracking, no Google |

=== "Photos"

    <img src="../../assets/img/opencamera.svg" alt="Pixelfed" width="50px" align="left"></img> Replace Instagram, Flickr, Google Photos, Img.ur and the like:

    |FOSS Alternative |Description |
    | ------ | ------ |
    |[Pixelfed](https://pixelfed.org/) |Free & ethical photo sharing platform. Powered by [activitypub](https://activitypub.rocks/), part of the [fediverse](https://gofoss.today/fediverse/) |
    |[Bibliogram](https://bibliogram.art/) |Free & open source front-end to Instagram. No ads, no tracking, no Facebook|
    |[Zaclys album](https://www.zaclys.com/album/) |Create private & public photo albums, share them with your friends |
    |[Kitten services](https://www.chatons.org/en/search/by-service?service_type_target_id=149&title=&field_software_target_id=All&field_is_shared_value=All) |Various photo sharing solutions, powered by [lutim](https://github.com/ldidry/lutim/)|

=== "Music"

    <img src="../../assets/img/audacity.svg" alt="Spotify" width="50px" align="left"></img> Replace Spotify, Youtube, Last.fm and the like:

    |FOSS Alternative |Description|
    | ------ | ------ |
    |[Funkwhale](https://funkwhale.audio/) |Free & open source decentralized platform to listen to & share music. Powered by [activitypub](https://activitypub.rocks/), part of the [fediverse](https://gofoss.today/fediverse/) |
    |[Libre.fm](https://libre.fm/)|Free & open source platform to listen to & share music |
    |Internet Radio |Listen to online radio stations, for example with [RadioDroid](https://f-droid.org/en/packages/net.programmierecke.radiodroid2/) |
    |Podcasts |Listen to free podcasts, for example with [AntennaPod](https://f-droid.org/en/packages/de.danoeh.antennapod/) |

=== "Books"

    <img src="../../assets/img/books.svg" alt="Bookwyrm" width="50px" align="left"></img> Replace Goodreads and the like:

    |FOSS Alternative |Description|
    | ------ | ------ |
    |[Bookwyrm](https://bookwyrm.social/)|Free & open source decentralized platform to talk about books, track your reading & share with friends. Powered by [activitypub](https://activitypub.rocks/), part of the [fediverse](https://gofoss.today/fediverse/) |

## Other cloud services

=== "Search"

    <img src="../../assets/img/search.svg" alt="Online search" width="50px" align="left"></img> Replace Google Search, Bing and the like:

    |FOSS Alternative |Description|
    | ------ | ------ |
    |[SearX](https://searx.space/) |Free metasearch engine, aggregates results from over 70 search services. No ads, no tracking, no Google, no Microsoft. Select your favourite instance, such as [searx.be](https://searx.be/) or [disroot search](https://search.disroot.org/) |
    |[DuckDuckGo](https://duckduckgo.com/) |Privacy focussed search engine. No tracking. **Caution**: serves ads from the Yahoo-Bing search alliance network & through affiliate relationships with Amazon and eBay.|

=== "Maps"

    <img src="../../assets/img/maps.svg" alt="Online maps" width="50px" align="left"></img> Replace Google Maps and the like:

    |FOSS Alternative |Description|
    | ------ | ------ |
    |[Openstreetmap](https://www.openstreetmap.org/) |Free & open source, community maintained online world map |
    |[Framacarte](https://framacarte.org/en/) |Free online service to create your own maps. Powered by [umap](https://github.com/umap-project/umap/) |
    |[GoGoCarto](https://gogocarto.fr/projects) |Free online service to create your own maps |

=== "Translation"

    <img src="../../assets/img/empathy.svg" alt="Deepl" width="50px" align="left"></img> Replace Google Translations and the like:

    |FOSS Alternative |Description|
    | ------ | ------ |
    |[LibreTranslate](https://libretranslate.com/)|Free & open source translation service. No ads, no Google|
    |[Simply Translate](https://translate.metalune.xyz/)|Free & open source front-end to Google Translate. No ads, no tracking, no Google |
    |[Deepl](https://www.deepl.com/translator)|Free translation service. **Caution**: not open source |

=== "URL Shortener"

    <img src="../../assets/img/fossbrowser.svg" alt="URL shortener" width="50px" align="left"></img> Replace Bit.ly, Goo.gl and the like:

    |FOSS Alternative |Description|
    | ------ | ------ |
    |[Let's shorten that URL](https://lstu.fr/)|Free & open source URL shortener. Features personalized link names |
    |[Colibris link](https://colibris.link/accueil) |Free & open source URL shortener. Features personalized link names & QR codes. Powered by [polr](https://github.com/cydrobolt/polr/) |
    |[Kitten services](https://www.chatons.org/en/search/by-service?service_type_target_id=125&title=&field_software_target_id=All&field_is_shared_value=All) |Various free & open source URL shorteners, powered by [lstu](https://lstu.fr/), [polr](https://github.com/cydrobolt/polr/) or [yourls](https://github.com/YOURLS/YOURLS/) |

=== "Pastebins"

    <img src="../../assets/img/editor.svg" alt="Cryptocurrency" width="50px" align="left"></img> Replace Pastebin and the like:

    |FOSS Alternative |Description|
    | -------------------------------- | ------ |
    |[Hastebin](https://hastebin.com/) |Free & open source online pastebin service |
    |[PicaPaste](https://paste.picasoft.net/)|Free & open source, encrypted online pastebin service. Powered by Privatebin, click here for more [Privatebin instances](https://github.com/PrivateBin/PrivateBin/wiki/PrivateBin-Instances-Directory)|
    |[GhostBin](https://ghostbin.com/) |Free & open source online pastebin service |

=== "Online Payment"

    <img src="../../assets/img/money.svg" alt="Cryptocurrency" width="50px" align="left"></img> Replace Paypal, Google Wallet, Apple Pay and the like:

    |FOSS Alternative |Description|
    | --------------------------------- | ------ |
    |[LiberaPay](https://liberapay.com/)|Open source donations platform |
    |[Bitcoin](https://bitcoin.org/en/) |Decentralized digital cryptocurrency, launched in 2009 |
    |[Ethereum](https://ethereum.org/en/) |Decentralized digital cryptocurrency, launched in 2015 |
    |[Litecoin](https://litecoin.org/) |Decentralized digital cryptocurrency, launched in 2011 |

=== "Email Forwarding"

    <img src="../../assets/img/simpleemail.svg" alt="Email forwarding" width="50px" align="left"></img> Forward emails anonymously:

    |FOSS Alternative |Description|
    | ------ | ------ |
    |[Anon Addy](https://anonaddy.com/) |Open source anonymous email forwarding service |
    |[Simple Login](https://simplelogin.io/) |Open source anonymous email forwarding service |

=== "Disposable Emails"

    <img src="../../assets/img/simpleemail.svg" alt="Disposable emails" width="50px" align="left"></img> Use disposable email addresses:

    |FOSS Alternative |Description|
    | ------ | ------ |
    |[Spam Gourmet](http://www.spamgourmet.com/) |Free online service to create disposable email addresses |
    |[Guerrillamail](https://www.guerrillamail.com/) |Free online service to create disposable email addresses |
    |[Anonbox.net](https://anonbox.net/) |Free online service to create disposable email addresses |
    |[Jetable.org](https://jetable.org/) |Free online service to create disposable email addresses |

<br>
