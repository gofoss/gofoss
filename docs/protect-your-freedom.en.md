---
template: main.html
title: How FOSS protects your privacy
description: What is FOSS? What's a threat model? And what's the difference between privacy, anonymity and security?
---

# Make technology work for you

GoFOSS wants to make privacy respecting, free and open source software accessible to all.

Decide for yourself if you should bother to continue reading:

* Do you own an iPhone or Android device?
* Does your computer run Windows, macOS or ChromeOS?
* Do you browse the web with Chrome, Safari or Edge?
* Do you socialise on Facebook, Twitter, Whatsapp, Tiktok or Snapchat?

<div align="center">
<img src="../../assets/img/closed_ecosystem.png" alt="Walled garden" width="550px"></img>
</div>

Most likely you answered *yes* to at least one of those questions. Are you aware that all of these services are run by companies which are part of the so-called [Surveillance Capitalism](https://en.wikipedia.org/wiki/Surveillance_capitalism/)? That's a term coined by Harvard professor Shoshana Zuboff and further developed by tech activist Cory Doctorow. In a nutshell, it describes an economic system centred around tech monopolies which harvest private user data to maximize profit.

Don't see any issues with that? Well, Surveillance Capitalism actually threatens the very core of democracy. It gives rise to mass surveillance, polarizes the political debate, interferes with electoral processes, drives uniformity of thought and facilitates censorship. Tackling those issues requires comprehensive changes to our legal systems and social conventions.

While waiting for better laws, there's a lot you can do to protect your privacy. Free and open source software can help you browse the Internet, chat with your loved ones or share memories without anyone collecting, recording, monetising or censoring your data.


## What's in this guide?

<center><img src="../../assets/img/foss_ecosystem.png" alt="Free and open source software" width="550px"></img></center>

This guide lays out seven steps to replace Big Tech with privacy-respecting, free and open source software, also referred to as [FOSS](https://www.gnu.org/philosophy/open-source-misses-the-point.en.html/):

1. [Get involved](https://gofoss.today/nothing-to-hide/). There is much to be said and learned about online privacy, data exploitation, filter bubbles, surveillance and censorship. Get involved and spread the word.

2. [Choose a safe and private browser](https://gofoss.today/intro-browse-freely/). Switch to Firefox. Block trackers, cookies and ads. Use privacy-respecting search engines. Possibly encrypt your traffic with Tor or VPN.

3. [Keep your conversations private](https://gofoss.today/intro-speak-freely/). Use end-to-end encryption to secure your emails, messages and calls. Ditch classic social media and discover the Fediverse, a federated family of various online services.

4. [Protect your data](https://gofoss.today/intro-store-safely/). Use long and unique passphrases. Choose a different one for each of your accounts and devices. Keep them safe in an encrypted password manager. Consider using two-factor authentication. Create a regular backup routine. And encrypt sensitive data.

5. [Free your computer](https://gofoss.today/intro-free-your-computer/). Switch to GNU/Linux and favor free and open source apps. Depending on your needs, choose a beginner friendly distribution like Linux Mint or Ubuntu. For more experienced users, pick Debian, Manjaro, openSUSE, Fedora or Gentoo Linux. And for privacy buffs, have a look at Qubes OS, Whonix or Tails.

6. [Free your phone](https://gofoss.today/intro-free-your-phone/). Switch to a custom mobile operating system like LineageOS, CalyxOS, GrapheneOS or /e/. Favor tracker-free open source apps from community maintained app stores.

7. [Free your cloud](https://gofoss.today/intro-free-your-cloud/). Choose privacy-respecting cloud providers. Or set up your own secure server and self host services such as cloud storage, photo galleries, task and contact management, or media streaming.


## Who is this guide for?

Pretty much anyone. This guide is meant to be fun, educational, customisable and accessible. At each step, we'll point out which audience is likely to be the most interested:

<center>

| Audience | Description |
| :------: | ------ |
|Normal users <br>(aka Beginners)<br><img src="../../assets/img/users.png" alt="beginners" width="30px"></img> |These chapters are for users with no particular tech skills, limited time or no inclination to tinker. |
|Intermediate users <br>(aka Tinkerers)<br><img src="../../assets/img/geeks.png" alt="geeks" width="30px"></img> |These chapters are for users with some tech skills under the belt and keen to learn more about privacy and technology. |
|Advanced users <br>(aka Techies)<br><img src="../../assets/img/techies.png" alt="techies" width="75px"></img> |These chapters are for users which are familiar with the tricks of the trade and have solid tech skills. |

</center>


## How does it work?

There is no one-click solution for online privacy. Replacing Big Tech with free and open source software is a process. Some changes are simple – like installing Firefox or Signal. Other methods require more time and skills. Most importantly, we want you to feel comfortable, stay in control and understand what software you are using. At each step of the way, you'll be able to take a look under the hood and adopt, reject or customise any software presented in this guide. Some key principles we follow:

<center>

| Principle | Description |
| :------: | ------ |
| #1 | Take one step at a time. |
| #2 |Start with the basics before diving into more advanced stuff. |
| #3 |Illustrate each step with concrete, hands-on examples. |
| #4 |Favour free and open source software. |
| #5 |Pick quality software which excels at one specific job over all-in-one bloatware. |
| #6 |Provide customisable solutions which leave the user in control. |
| #7 |Stay close to the source code, and away from one-click deployments or containerised solutions. |

</center>


??? question "Privacy, anonymity or security — what's the difference"

    <center>

    | Concept | Description |
    | ------ | ------ |
    | Privacy |It's about keeping content private. Others can know who you are — but not what you think, say or do. |
    | Anonymity |It's about concealing your identity. Others can know what you think, say or do — but not who you are. |
    | Security |It's about putting tools in place to protect your privacy and anonymity. |

    </center>

    While more security often means more privacy or anonymity, it also means less convenience. [The right tradeoff depends on your individual threat model](https://www.eff.org/files/2015/11/24/3mod_threat-modeling-ssd_9-3-15.pdf/). This guide should be helpful if your objective is a reasonable level of privacy. Not so much if you plan on becoming a spy, hiding from powerful adversaries or infringing laws.


??? question "Why is FOSS better for privacy than proprietary solutions?"

    Free and open source software is often referred to as FOSS, FLOSS, Libre Software or Free Software. It includes a whole range of licenses that give users the right to use, study, share and improve the software. Contrary to proprietary software, FOSS makes the source code freely available to all. Of course, that's not a silver bullet to rule out vulnerabilities and bugs. The code needs active auditing to ensure safety and privacy. Still, FOSS remains preferable to closed source code. Because it's encouraging developers to audit the code, fix issues and ensure nothing shady goes on in the background. FOSS is not only about technology. It’s about social, political and economic emancipation.

<br>