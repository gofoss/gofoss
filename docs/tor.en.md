---
template: main.html
title: How to use the Tor Browser
description: How to use Tor browser? Is using Tor illegal? Can tor be traced? What about Tor browser for Android?
---

# Stay anonymous online with Tor

<div align="center">
<img src="../../assets/img/tor_connection.png" alt="Tor Browser" width="500px"></img>
</div>

!!! level "This chapter is geared towards beginners. No particular tech skills are required."

Need to remain anonymous online and hide your identity from Internet Service Providers (ISPs), websites, advertisers or surveillance? Use the [Tor Browser](https://www.torproject.org/)!

Tor was released in 2002 and is used by millions of people worldwide, including journalists and activists. Before reaching its destination, the Tor browser's web traffic is routed through a world-wide network made up of thousands of computers (so-called relays). Since everything is encrypted and no records are kept, nobody has a clue about the traffic's origin, destination or content. The only thing the destination website will see is the IP address of the exit node, which is the last relay in the network. Detailed installation instructions below.

=== "Windows"

    ??? tip "Show me the step-by-step guide for Windows"

         Download and run the [Tor installer for Windows](https://www.torproject.org/download/).


=== "macOS"

    ??? tip "Show me the step-by-step guide for macOS"

        Download the [Tor installer for OS X](https://www.torproject.org/download/). Open the `.dmg` file and drag the Tor Browser icon on top of the Application folder. For easy access, open the Applications folder and drag the Tor Browser icon to your dock.


=== "Linux (Ubuntu)"

    ??? tip "Show me the step-by-step guide for Linux (Ubuntu)"

        Open the terminal with the shortcut `CTRL + ALT + T`, or click on the `Applications` button on the top left and search for "Terminal". The following command installs Tor:

        ```bash
        sudo apt install torbrowser-launcher
        ```

        Check if Tor has been correctly installed by verifying the version number:

        ```bash
        tor --version
        ```

        That's it, you successfully installed Tor! To start the browser, click on "Activities" and search for the "Tor Browser", or run the terminal command `torbrowser-launcher`.


=== "Android"

    ??? tip "Show me the step-by-step guide for Android"

        Download the Tor Browser from [Google's App Store](https://play.google.com/store/apps/details?id=org.torproject.torbrowser&hl=en_US/). There are also ways to download or update the Tor Browser without using a Google account. Visit the [Tor Browser download page](https://www.torproject.org/download/) from your Android device, tap on the `Android` icon and download the `.apk` file. Tor Browser is also available on [F-Droid](https://support.torproject.org/tormobile/tormobile-7/) or [Aurora Store](https://auroraoss.com/), two alternative app stores. More on this later, in [another chapter](https://gofoss.today/foss-apps/)!


=== "iOS"

    ??? tip "Show me the step-by-step guide for iOS"

        Download the Onion Browser from the [App Store](https://apps.apple.com/us/app/onion-browser/id519296448).

        **Caution**: please be aware that Apple requires browsers on iOS to use something called Webkit, which prevents Onion Browser from having the same privacy protections as the Tor Browser.


<div style="margin-top:-20px">
</div>

??? warning "Tor is great, but it's not perfect"

    Although the Tor Browser has numerous benefits, it also comes with a few drawbacks. Browsing is slower than with a regular Internet connection. Some major websites block Tor users. In some countries, Tor is illegal or blocked by the government. Also, using Tor doesn't necessarily mean being anonymous or secure. Your device still can be exposed to man-in-the-middle attacks, correlation attacks or similar exploits.





<br>

<center> <img src="../../assets/img/separator_tor.svg" alt="Tor installation" width="150px"></img> </center>

## Anonymity check

Once Tor is installed, browse to the following websites and check whether any identifying information, such as your real IP address, is leaked:

* [torproject.org](https://check.torproject.org/)
* [ipleak.net](https://ipleak.net/)
* [panopticlick.eff.org](https://panopticlick.eff.org/)
* [amiunique.org](https://amiunique.org/)
* [browserleaks.com](https://browserleaks.com/)

<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Tor support" width="150px"></img> </center>

## Support

For further details or questions, refer to [Tor's support portal](https://support.torproject.org/) or ask [Reddit's Tor community](https://teddit.net/r/TOR/) for help.

<div align="center">
<img src="https://imgs.xkcd.com/comics/incognito_mode.png" alt="Tor browser"></img>
</div>

<br>
