---
template: main.html
title: How to install FOSS apps
description: Degoogle. What is F-Droid? How do I install F-droid? Is F-Droid safe? F-droid best apps. What is Aurora Store? How do I install Aurora Store?
---

# List of 50 tracker-free FOSS apps

<div align="center">
<img src="../../assets/img/mobile_3.png" alt="F-Droid" width="500px"></img>
</div>


## F-Droid

If you are the proud owner of an Android phone, have a look at [F-Droid](https://f-droid.org/)! It's an app store exclusively hosting free and open-source applications. Installing and keeping F-Droid up to date should be pretty straight forward. More detailed instructions below. For additional details or questions, refer to [F-Droid's documentation](https://f-droid.org/en/docs/) or ask the [F-Droid community](https://forum.f-droid.org/).

!!! level "This chapter is geared towards intermediate users. Some tech skills are required."

??? tip "Show me the step-by-step guide"

    <center>

    | Steps | Instructions |
    | ------ | ------ |
    | Download |Open up your phone's browser, navigate to [F-Droid's website](https://f-droid.org/) and download the `.apk` file. |
    | Allow installation from unknown sources |Open the downloaded `.apk` file. Your phone should prompt something like *"For your security, your phone is not allowed to install unknown apps from this source"*. To temporarily allow installation from unknown sources, click on `Settings ‣ Allow from this source`. |
    | Install F-Droid | Leave the settings page and click on `Install`. |
    | Forbid installation from unknown sources |Browse to your phone's `Settings` to disable installation from unknown sources. Depending on your phone, this option might be located in `Settings ‣ Apps & notifications ‣ Advanced ‣ Special app access ‣ Install unknown apps`. Select your browser and disable the option `Allow from this source`.|
    | Launch F-Droid |After the first launch, F-Droid will update the software repository. That's the list of all the software packages available on F-Droid. This update can take a while, be patient. |
    | Enable automatic updates |Navigate to F-Droid's `Settings` page and enable the options `Automatically install updates` as well as `Show available updates`. |
    | Manually update repositories |Back on F-Droid's home screen, swipe down to refresh all repositories.|

    </center>


??? question "Can I install F-Droid or FOSS apps on my iPhone?"

    Owners of iOS devices are out of luck. Apple's walled garden makes it [practically incompatible with mobile FOSS apps](https://www.fsf.org/blogs/community/why-free-software-and-apples-iphone-dont-mix/). Under the pretext of security, Apple's use of [copyright lock](https://www.law.cornell.edu/uscode/text/17/1201) and [access control](https://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=CELEX:32001L0029:EN:HTML/) forecloses any tinkering, jailbreaking, repairing or diagnosis on iOS devices. Apple — not the user — gets to decide what content can be accessed, what apps can be installed and when the device needs to be thrown away.


<br>

<center> <img src="../../assets/img/separator_aurora.svg" alt="Aurora Store" width="150px"></img> </center>

## Aurora Store

Not all useful Android apps are FOSS, tracker-free or available on F-Droid. That doesn't necessarily mean they are bad for your privacy. It also doesn't mean you have to use Google's Play Store to get them. Install [Aurora store](https://f-droid.org/en/packages/com.aurora.store/) instead, an alternative app store where you can download, update or search for apps without a Google account.


<br>

<center> <img src="../../assets/img/separator_fdroid.svg" alt="Tracker-free apps" width="150px"></img> </center>

## Tracker-free apps (F-Droid)

<center>

| FOSS application | |Description |
| :------: | ------ | ------ |
| <img src="../../assets/img/fossbrowser.svg" alt="FOSS browser" width="50px"></img> | [FOSS browser](https://f-droid.org/en/packages/de.baumann.browser/) | Simple and light weight browser. |
| <img src="../../assets/img/webapps.svg" alt="Webapps" width="50px"></img> | [Webapps](https://f-droid.org/en/packages/com.tobykurien.webapps/) | Turns your favourite mobile websites into secure apps. |
| <img src="../../assets/img/tutanota.svg" alt="Tutanota" width="50px"></img> | [Tutanota](https://f-droid.org/en/packages/de.tutao.tutanota/) | Open source email application. Dark theme, encryption, instant push notifications, auto-sync, full-text search, swipe gestures, etc. |
| <img src="../../assets/img/libremmail.svg" alt="Librem mail" width="50px"></img> | [Librem mail](https://f-droid.org/en/packages/one.librem.mail/) | Open source email application. Encryption, multiple accounts, fork of K-9 Mail. |
| <img src="../../assets/img/simpleemail.svg" alt="Simple email" width="50px"></img> | [Simple email](https://f-droid.org/en/packages/org.dystopia.email/) | Open source email application. Privacy friendly, encryption, multiple accounts, two way sync, offline storage, dark theme, search on server, simple design. |
| <img src="../../assets/img/k9mail.svg" alt="K-9 mail" width="50px"></img> | [K-9 mail](https://f-droid.org/en/packages/com.fsck.k9/) |Open source email application. Supports POP3 and IMAP, but only supports push mail for IMAP. |
| <img src="../../assets/img/silence.svg" alt="Silence" width="50px"></img> | [Silence](https://f-droid.org/en/packages/org.smssecure.smssecure/) | Encryted SMS and MMS application to protect your privacy. |
| <img src="../../assets/img/signal.svg" alt="Element" width="50px"></img> | [Element](https://f-droid.org/en/packages/im.vector.app/) | Open source client for the matrix protocol. Matrix is an network for secure, end-to-end encrypted, decentralised communication. Can be run off self-hosted servers. |
| <img src="../../assets/img/silence.svg" alt="Conversations" width="50px"></img> | [Conversations](https://f-droid.org/packages/eu.siacs.conversations/) | Open source client for the XMPP protocol. Secure, end-to-end encrypted, decentralised communication. Can be run off self-hosted servers. |
| <img src="../../assets/img/signal.svg" alt="Briar" width="50px"></img> | [Briar](https://f-droid.org/packages/org.briarproject.briar.android/) | Open source messenger. Peer-to-peer (does not rely on central servers), end-to-end encryption, minimal exposure of information. Uses Tor. |
| <img src="../../assets/img/tusky.svg" alt="Tusky" width="50px"></img> | [Tusky](https://f-droid.org/en/packages/com.keylesspalace.tusky/) | Lightweight client for Mastodon, a free and open-source social network to replace Twitter or Facebook. |
| <img src="../../assets/img/redreader.svg" alt="RedReader" width="50px"></img> | [RedReader](https://f-droid.org/en/packages/org.quantumbadger.redreader/) | Client for reddit.com. |
| <img src="../../assets/img/feeder.svg" alt="Feeder" width="50px"></img> | [Feeder](https://f-droid.org/en/packages/com.nononsenseapps.feeder/) | Open source RSS feed reader. No tracking, no need for an account, offline reading, background synchronisation, notifications. |
| <img src="../../assets/img/feeder.svg" alt="Flym" width="50px"></img> | [Flym](https://f-droid.org/packages/net.frju.flym/) |Open source RSS reader. |
| <img src="../../assets/img/maps.svg" alt="Osmand" width="50px"></img> | [Osmand](https://f-droid.org/en/packages/net.osmand.plus/) |Open source app for online & offline maps and navigation. |
| <img src="../../assets/img/maps.svg" alt="Organic Maps" width="50px"></img> | [Organic Maps](https://f-droid.org/en/packages/app.organicmaps/) |Open source app for offline maps and navigation. Maps.me fork, without trackers. |
| <img src="../../assets/img/opencamera.svg" alt="Open camera" width="50px"></img> | [Open camera](https://f-droid.org/en/packages/net.sourceforge.opencamera/) |Feature rich camera app, including auto-stabilisation, multizoom touch, flash, face detection, timer, burst mode, silenceable shutter, and many more. |
| <img src="../../assets/img/simplegallery.svg" alt="Simple gallery" width="50px"></img> | [Simple gallery](https://f-droid.org/en/packages/com.simplemobiletools.gallery.pro/) |Highly customizable offline gallery, requires no Internet access to protect your privacy. Organise and edit photos, recover deleted files, protect and hide files and view a huge variety of different photo and video formats including RAW, SVG and much more. |
| <img src="../../assets/img/simplemusic.svg" alt="Simple music player" width="50px"></img> | [Simple music player](https://f-droid.org/en/packages/com.simplemobiletools.musicplayer/) |Music player, easily controllable from the status bar, the home screen widget or by hardware buttons on your headset. Fully open source, contains no ads or unnecessary permissions. Fully customizable colors. |
| <img src="../../assets/img/simplecalculator.svg" alt="Simple calculator" width="50px"></img> | [Simple calculator](https://f-droid.org/en/packages/com.simplemobiletools.calculator/) |Fully open source calculator, without ads or unnecessary permissions. Customizable colors. |
| <img src="../../assets/img/editor.svg" alt="Simple notes" width="50px"></img> | [Simple notes](https://f-droid.org/en/packages/com.simplemobiletools.notes.pro/) |Fully open source note taking app, without ads or unnecessary permissions. Customizable colors and widget. |
| <img src="../../assets/img/editor.svg" alt="Notepad" width="50px"></img> | [Notepad](https://f-droid.org/packages/com.farmerbb.notepad/) |Simple open source note taking app. |
| <img src="../../assets/img/editor.svg" alt="Carnet" width="50px"></img> | [Carnet](https://f-droid.org/packages/com.spisoft.quicknote/) |Powerful open source note taking app, with sync capabilities (incl. NextCloud) and online editor. |
| <img src="../../assets/img/editor.svg" alt="Markor" width="50px"></img> | [Markor](https://f-droid.org/packages/net.gsantner.markor/) |Open source text editor with markdown support. |
| <img src="../../assets/img/simpleclock.svg" alt="Simple clock" width="50px"></img> | [Simple clock](https://f-droid.org/en/packages/com.simplemobiletools.clock/) |Clock, alarm, stopwatch, timer. Fully open source, no ads or unnecessary permissions. Customizable colors. |
| <img src="../../assets/img/alarm.svg" alt="Simple Alarm clock" width="50px"></img> | [Simple Alarm clock](https://play.google.com/store/apps/details?id=com.better.alarm/) |Open source alarm clock with powerful features and clean interface. |
| <img src="../../assets/img/simplecontacts.svg" alt="Simple contacts" width="50px"></img> | [Simple contacts](https://f-droid.org/en/packages/com.simplemobiletools.contacts.pro/) |Simple app for creating or managing contacts. Fully open source, no ads or unnecessary permissions. Customizable colors. Contacts can be stored locally on devices only, or synchronised with the cloud. If you [host your own server](https://gofoss.today/intro-free-your-cloud/), this app can be used to [manage and synchronise contacts](https://gofoss.today/contacts-calendars-tasks/). |
| <img src="../../assets/img/simplecontacts.svg" alt="Open contacts" width="50px"></img> | [Open contacts](https://f-droid.org/en/packages/opencontacts.open.com.opencontacts/) |Open source contact app. |
| <img src="../../assets/img/simplecalendar.svg" alt="Simple calendar" width="50px"></img> | [Simple calendar](https://f-droid.org/en/packages/com.simplemobiletools.calendar.pro/) |Fully customizable offline calendar to organise single or recurring events, birthdays, anniversaries, business meetings, appointments, etc. Daily, weekly and monthly views available. Fully open source, no ads or unnecessary permissions. Customizable colors. If you [host your own server](https://gofoss.today/intro-free-your-cloud/), this app can be used to [manage and synchronise calendars](https://gofoss.today/contacts-calendars-tasks/).|
| <img src="../../assets/img/doodle.svg" alt="Etar" width="50px"></img> | [Etar](https://f-droid.org/en/packages/ws.xsoh.etar/) |Material designed open source calendar. Works with online calendars. Free, open source and without ads. |
| <img src="../../assets/img/tasks.svg" alt="OpenTasks" width="50px"></img> | [OpenTasks](https://f-droid.org/en/packages/org.dmfs.tasks/) |Open source task manager. If you [host your own server](https://gofoss.today/intro-free-your-cloud/), this app can be used to [manage and synchronise tasks](https://gofoss.today/contacts-calendars-tasks/). |
| <img src="../../assets/img/davx5.svg" alt="Davx5" width="50px"></img> | [Davx5](https://f-droid.org/en/packages/at.bitfire.davdroid/) | Open source client to synchronise contacts, calendars and task lists. Can be used with a [self-hosted server](https://gofoss.today/contacts-calendars-tasks/) or a trusted hoster. |
| <img src="../../assets/img/antennapod.svg" alt="AntennaPod" width="50px"></img> | [AntennaPod](https://f-droid.org/en/packages/de.danoeh.antennapod/) |Advanced podcast manager and player. Provides instant access to millions of free and paid podcasts, from independent podcasters to large publishing houses such as the BBC, NPR and CNN. |
| <img src="../../assets/img/radiodroid.svg" alt="RadioDroid" width="50px"></img> | [RadioDroid](https://f-droid.org/en/packages/net.programmierecke.radiodroid2/) |Listen to online radio stations. |
| <img src="../../assets/img/documentviewer.svg" alt="Document viewer" width="50px"></img> | [Document viewer](https://f-droid.org/en/packages/org.sufficientlysecure.viewer/) |View various file formats, including pdf, djvu, epub, xps and comic books (cbz, fb2). |
| <img src="../../assets/img/libreofficeviewer.svg" alt="Libre Office viewer" width="50px"></img> | [Libre Office viewer](https://f-droid.org/en/packages/org.documentfoundation.libreoffice/) |View docx, doc, xlsx, xls, pptx, ppt, odt, ods and odp files. |
| <img src="../../assets/img/https.svg" alt="Keepass DX" width="50px"></img> | [Keepass DX](https://f-droid.org/en/packages/com.kunzisoft.keepass.libre/) |Secure and open source password manager. |
| <img src="../../assets/img/letsencrypt.svg" alt="andOTP" width="50px"></img> | [andOTP](https://f-droid.org/en/packages/org.shadowice.flocke.andotp/) |Free and open-source application for two-factor authentication. |
| <img src="../../assets/img/letsencrypt.svg" alt="Free OTP" width="50px"></img> | [Free OTP](https://f-droid.org/packages/org.liberty.android.freeotpplus/) | Open source two-factor authenticator. |
| <img src="../../assets/img/letsencrypt.svg" alt="Aegis" width="50px"></img> | [Aegis](https://f-droid.org/en/packages/com.beemdevelopment.aegis/) |Free, secure and open source two-factor authenticator. |
| <img src="../../assets/img/openvpn.svg" alt="Open VPN" width="50px"></img> | [Open VPN](https://f-droid.org/packages/de.blinkt.openvpn/) |If you [host your own server](https://gofoss.today/intro-free-your-cloud/), this app can be used to establish a VPN connection. |
| <img src="../../assets/img/openvpn.svg" alt="Proton VPN" width="50px"></img> | [Proton VPN](https://f-droid.org/en/packages/ch.protonvpn.android/) |Secure and free VPN. Claims not to log user activity. Features encryption, Swiss privacy laws, DNS leak protection, kill switch and more. |
| <img src="../../assets/img/simplefilemanager.svg" alt="Simple file manager" width="50px"></img> | [Simple file manager](https://f-droid.org/en/packages/com.simplemobiletools.filemanager.pro/) |Browse and edit files. Open source, no ads, customizable colors.|
| <img src="../../assets/img/seafile.svg" alt="Seafile" width="50px"></img> | [Seafile](https://f-droid.org/en/packages/com.seafile.seadroid2/) |If you [host your own server](https://gofoss.today/intro-free-your-cloud/), this app can be used to [manage and synchronise cloud files](https://gofoss.today/cloud-storage/). |
| <img src="../../assets/img/anysoft.svg" alt="Anysoft keyboard" width="50px"></img> | [Anysoft keyboard](https://f-droid.org/packages/com.menny.android.anysoftkeyboard/) |Open source keyboard, multi-language support, voice input, gesture support, night mode, theme support, privacy friendly. |
| <img src="../../assets/img/anysoft.svg" alt="Simple keyboard" width="50px"></img> | [Simple keyboard](https://f-droid.org/packages/rkr.simplekeyboard.inputmethod/) |Open source keyboard. |
| <img src="../../assets/img/anysoft.svg" alt="Open board" width="50px"></img> | [Open board](https://f-droid.org/en/packages/org.dslul.openboard.inputmethod.latin/) |Open source keyboard. |
| <img src="../../assets/img/anysoft.svg" alt="Hacker's keyboard" width="50px"></img> | [Hacker's keyboard](https://f-droid.org/en/packages/org.pocketworkstation.pckeyboard/) |Open source keyboard with separate number keys, punctuation in the usual places, and arrow keys. | 
| <img src="../../assets/img/totem.svg" alt="Newpipe" width="50px"></img> | [Newpipe](https://f-droid.org/packages/org.schabi.newpipe/) | Lightweight Youtube app, without proprietary API or Google's play services. Also supports PeerTube. |
| <img src="../../assets/img/totem.svg" alt="Thorium" width="50px"></img> | [Thorium](https://f-droid.org/en/packages/net.schueller.peertube/) |Peer tube is a decentralised video hosting network, based on FOSS. |
| <img src="../../assets/img/gimp.svg" alt="Quick dic" width="50px"></img> | [Quick dic](https://f-droid.org/en/packages/de.reimardoeffinger.quickdic/) |Offline dictionary. |
| <img src="../../assets/img/exodus.svg" alt="Exodus" width="50px"></img> | [Exodus](https://f-droid.org/en/packages/org.eu.exodus_privacy.exodusprivacy/) |Find out which trackers and permissions are embedded in your apps. |
| <img src="../../assets/img/lawnchair.svg" alt="Zim launcher" width="50px"></img> | [Zim launcher](https://f-droid.org/packages/org.zimmob.zimlx/) |Free, open source launcher app, without ads. |

</center>


<br>

<center> <img src="../../assets/img/separator_aurora.svg" alt="Aurora Store" width="150px"></img> </center>

## Privacy-respecting apps (Aurora)

<center>

| Application | |Description |
| :------: | ------ | ------ |
| <img src="../../assets/img/firefox.svg" alt="Firefox" width="50px"></img> | [Firefox](https://www.mozilla.org/en-US/firefox/mobile/) | Fast, secure and private browser. *Caution*: [3 trackers (Adjust, LeanPlum, Google Analytics)](https://reports.exodus-privacy.eu.org/reports/org.mozilla.firefox/latest/). |
| <img src="../../assets/img/tor.svg" alt="Tor browser" width="50px"></img> | [Tor browser](https://play.google.com/store/apps/details?id=org.torproject.torbrowser&hl=en_US/) |Tor browser for Android. Blocks trackers, defends against surveillance, resists fingerprinting, encrypts internet traffic. *Caution*: [3 trackers (Adjust, LeanPlum, Google Analytics)](https://reports.exodus-privacy.eu.org/en/reports/org.torproject.torbrowser/latest/). |
| <img src="../../assets/img/protonmail.svg" alt="Protonmail" width="50px"></img> | [Protonmail](https://play.google.com/store/apps/details?id=ch.protonmail.android&hl=en_US/) |The world's largest secure email service, developed by CERN and MIT scientists. Open source and protected by Swiss privacy law. No trackers. |
| <img src="../../assets/img/signal.svg" alt="Signal" width="50px"></img> | [Signal](https://play.google.com/store/apps/details?id=org.thoughtcrime.securesms&hl=en_US/) |Fast, simple and secure messaging. End-to-end encrypted text, voice, videos, documents, pictures. Open source, no ads. No trackers. *Caution*: requires your phone number. |
| <img src="../../assets/img/lawnchair.svg" alt="Lawnchair" width="50px"></img> | [Lawnchair 2](https://play.google.com/store/apps/details?id=ch.deletescape.lawnchair.plah) |Free and open source launcher. Customise icon size, labels, rows, columns, icon packs, notifications, and much more. No trackers. |

</center>


<br>

<center> <img src="../../assets/img/separator_unzip.svg" alt="FOSS apps" width="150px"></img> </center>

## Other FOSS apps

<center>

| FOSS application | |Description |
| :------: | ------ | ------ |
| <img src="../../assets/img/firefox.svg" alt="Icecat" width="50px"></img> | [Icecat](https://f-droid.org/en/packages/org.gnu.icecat/) |GNU version of Firefox. It's main advantage is an ethical one: it is entirely free software. While the Firefox source code is free software, Mozilla distributes and recommends nonfree software, such as plug-ins and addons. *Caution*: [1 tracker (Mozilla Telemetry)](https://reports.exodus-privacy.eu.org/reports/org.gnu.icecat/latest/). |
| <img src="../../assets/img/fossbrowser.svg" alt="Fennec" width="50px"></img> | [Fennec F-Droid](https://f-droid.org/en/packages/org.mozilla.fennec_fdroid/) |Privacy-focused version of Firefox. It's focused on removing any proprietary bits found in official Mozilla's builds. *Caution*:  [2 trackers (Adjust, LeanPlum)](https://reports.exodus-privacy.eu.org/en/reports/org.mozilla.fennec_fdroid/latest/).  |
| <img src="../../assets/img/firefox.svg" alt="Mull browser" width="50px"></img> | [Mull](https://f-droid.org/packages/us.spotco.fennec_dos/) | Hardened fork of Firefox for Android, developped by the DivestOS team and based on the Tor uplift and arkenfox-user.js projects. *Caution*:  [2 trackers (Adjust, Mozilla Telemetry)](https://reports.exodus-privacy.eu.org/reports/us.spotco.fennec_dos/latest/).|
| <img src="../../assets/img/fossbrowser.svg" alt="Bromite" width="50px"></img> | [Bromite](https://www.bromite.org/) |Chromium browser, including ad blocking and enhanced privacy. Not available on F-Droid. |
| <img src="../../assets/img/alarm.svg" alt="Insane alarm" width="50px"></img> | [Insane alarm](https://github.com/RIAEvangelist/insane-alarm/releases/) |Open source alarm, will wake you up no matter what! Not available on F-Droid. |
| <img src="../../assets/img/tasks.svg" alt="Tasks.org" width="50px"></img> | [Tasks.org](https://f-droid.org/en/packages/org.tasks/) |Open source task manager. If you [host your own server](https://gofoss.today/intro-free-your-cloud/), this app can be used to [manage and synchronise tasks](https://gofoss.today/contacts-calendars-tasks/). *Caution*: [2 trackers (Google CrashLytics, Google Analytics).](https://reports.exodus-privacy.eu.org/reports/org.tasks/latest/) |
| <img src="../../assets/img/simplefilemanager.svg" alt="DroidFS" width="50px"></img> | [DroidFS](https://f-droid.org/en/packages/sushi.hardcore.droidfs/) |Store and access your files securely. Open source tool to store files in encrypted volumes. Available on F-Droid, no information on trackers.|
| <img src="../../assets/img/editor.svg" alt="Standard notes" width="50px"></img> | [Standard notes](https://f-droid.org/en/packages/com.standardnotes/) |Free, open source and completely encrypted notes app. *Caution*: [1 tracker (bugsnag)](https://reports.exodus-privacy.eu.org/en/reports/com.standardnotes/latest/) |
| <img src="../../assets/img/maps.svg" alt="Magic earth" width="50px"></img> | [Magic earth](https://play.google.com/store/apps/details?id=com.generalmagic.magicearth/) |Uses OpenStreetMap data, not really open source though. Not available on F-Droid. Claims not to track users. Includes Dashcam, navigation, traffic info, public transport schedules and weather.|
| <img src="../../assets/img/doodle.svg" alt="Transportr" width="50px"></img> | [Transportr](https://f-droid.org/packages/de.grobox.liberario/) |Open source app for public transport time schedules in Europe and overseas. *Caution*: [1 tracker (mapbox)](https://reports.exodus-privacy.eu.org/reports/de.grobox.liberario/latest/)|
| <img src="../../assets/img/totem.svg" alt="Free tube" width="50px"></img> | [Free tube](https://github.com/FreeTubeApp/FreeTube/) |Open source Youtube app for privacy. Not available on F-Droid. |
| <img src="../../assets/img/aurora.svg" alt="Fossdroid" width="50px"></img> | [Fossdroid](https://fossdroid.com/) |Marketplace similar to F-Droid, promoting free and open source apps on the Android platform. |

</center>


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Support" width="150px"></img> </center>

## Support

For further details or questions, refer to [F-Droid's community](https://forum.f-droid.org/) or ask [the Reddit community](https://teddit.net/r/fossdroid/) for help.

<br>
