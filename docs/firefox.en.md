---
template: main.html
title: How to harden Firefox
description: Mozilla Firefox vs Chrome. Is Firefox safer than Google? How do I install Firefox? What about Firefox on Android? What is a hardened Firefox?
---

# Firefox, a safe and private browser

<center>
<img align="center" src="../../assets/img/firefox_logo.png" alt="Mozilla Firefox browser" width ="150px"></img>
</center>

!!! level "This chapter is geared towards beginners & intermediate users. Some tech skills may be required."

[Firefox](https://www.mozilla.org/en-US/firefox/new/) is the preferred browser when it comes to privacy, security and convenience. Firefox was first released back in 2004 by the Mozilla community. The browser is free and open source, highly customisable, blocks cookies & trackers and runs smoothly on virtually any device. Detailed installation instructions below.

=== "Windows"

    ??? tip "Show me the step-by-step guide for Windows"

        Download and run the [Firefox installer](https://www.mozilla.org/en-US/firefox/windows/).


=== "macOS"

    ??? tip "Show me the step-by-step guide for macOS"

        Download the [Firefox disk image](https://www.mozilla.org/en-US/firefox/mac/), open it and drag the Firefox icon on top of the Application folder. For easy access, open the Applications folder and drag the Firefox icon to your dock.


=== "Linux (Ubuntu)"

    ??? tip "Show me the step-by-step guide for Linux (Ubuntu)"

        Open the terminal with the `Ctrl+Alt+T` shortcut or click on the `Applications` button on the top left and search for `Terminal`. Then run the following command:

        ```bash
        sudo apt install firefox
        ```

=== "Android"

    ??? tip "Show me the step-by-step guide for Android"

        Download Firefox from the [App Store](https://play.google.com/store/apps/details?id=org.mozilla.firefox&hl=en&gl=US/), or visit the [Firefox download page](https://www.mozilla.org/en-US/firefox/mobile/) from your Android device. There's also a way to download and update Firefox without using a Google account: [Aurora Store](https://auroraoss.com/)! We'll explain how to use alternative app stores in [a later chapter](https://gofoss.today/foss-apps/).


=== "iOS"

    ??? tip "Show me the step-by-step guide for iOS"

        Download Firefox from the [App Store](https://apps.apple.com/us/app/firefox-private-safe-browser/id989804926/).

<div style="margin-top:-20px">
</div>

??? question "Are there other privacy respecting browsers?"

    There are, some of them listed in the table below. Please mind that we don't recommend using Chrome or [Chromium](https://www.chromium.org/) based browsers from a privacy point of view. They run on code ultimately controlled by Google, and fortify Google's browser monopoly. Some of them are also susceptible to connect to Google's services. Also, some of Google's recent proposals aren't exactly comforting: the tech giant plans on [using algorithms to profile users based on their Chrome history](https://www.eff.org/deeplinks/2021/03/googles-floc-terrible-idea/) so that advertisers can better target them.

    <center>

    | Browser | Description |
    | ------ | ------ |
    | [Librewolf](https://librewolf-community.gitlab.io/) | Free and privacy focused fork of Firefox. Available for desktop environments. Comes with uBlock Origin and privacy-conscious search providers out-of-the-box. Uses DuckDuckGo as standard search engine, this can be changed. Open source, no telemetry, no tracking.|
    | [Icecat](https://www.gnu.org/s/icecat/) | GNU version of the Firefox browser, meaning it's entirely free and open source software. No proprietary plugins or add-ons, no trademark licensing. Icecat also includes security features not found in Firefox. Available for desktop and mobile environments. Unfortunately, release cycles are a tad bit long. |
    | [FOSS browser](https://github.com/scoute-dich/browser/) | Simple and light weight browser for Android. Open source, no telemetry, no tracking. Uses Startpage as standard search engine, this can be changed. |
    | [Mull](https://github.com/Divested-Mobile/mull/) | Mull is a hardened fork of Firefox for Android. The open source project is developed by the DivestOS team and based on the Tor uplift and arkenfox-user.js projects. |
    | [Fennec](https://f-droid.org/en/packages/org.mozilla.fennec_fdroid/) | Privacy-focused version of Firefox for Android. It's open source and focused on removing any proprietary bits found in official Mozilla's builds. Uses Google as standard search engine, this can be changed. Contains some trackers. |
    | [Ungoogled Chromium](https://github.com/Eloston/ungoogled-chromium/) | Ungoogled Chromium is Google Chromium, sans dependency on Google web services. It also features some tweaks to enhance privacy, control and transparency. Open source, no telemetry, no tracking. Available for desktop and mobile environments. |
    | [Brave](https://brave.com) | Chromium-based browser available for desktop and mobile environments. The open source project is maintained by a for profit, VC backed company. Features an (opt-in) ad system, and faced criticism in the past for adding affiliate links that it profits from. Transmits telemetry data to an analytics service by default, this can be opted-out. Uses Google as standard search engine, this can be changed. |
    | [Bromite](https://www.bromite.org/) | Chromium-based browser for Android, including ad blocking and enhanced privacy. Open source, no telemetry, no tracking. Uses Google as standard search engine, this can be changed. |

    </center>



<br>

<center> <img src="../../assets/img/separator_ublockorigin.svg" alt="uBlock Origin" width="150px"></img> </center>

## Privacy extensions

=== "Windows, macOS & Linux"

    Add [uBlock Origin](https://en.wikipedia.org/wiki/UBlock_Origin/) to your fresh Firefox install. It's a free and open source content filter — and a good one at that! [Head over to Mozilla's addon store](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/), and click on the button `Add to Firefox`. Various settings allow to increase your privacy, as detailed in the instructions below.

    ??? tip "Show me the step-by-step guide for Windows, macOS & Linux (Ubuntu)"

        * Click on the uBlock Origin icon in Firefox's toolbar
        * Make sure uBlock Origin is enabled, the large power button needs to be blue
        * Click on the Dashboard button
        * Open the tab `Filter lists`
        * Select the checkboxes shown in the table below and click on `Apply changes`

        <center>

        | Section | Check box |
        | ------ | ------ |
        | Ads | ☑ AdGuard Base <br> ☑ AdGuard Mobile Ads <br> ☑ EasyList <br> |
        | Privacy |  ☑ AdGuard Tracking Protection <br> EasyPrivacy <br> ☑ Fanboy's Enhanced Tracking List <br> |
        | Annoyances |  ☑ AdGuard Annoyances <br> ☑ AdGuard Social Media <br> ☑ Anti-Facebook <br> ☑ EasyList Cookie <br> ☑ Fanboy's Annoyance <br>  ☑ Fanboy's Social <br> ☑ uBlock filters - Annoyances <br> |

        </center>


=== "Android"

    Add [HTTPS Everywhere](https://www.eff.org/https-everywhere/) to your fresh Firefox install. It's a privacy extension which enables Hypertext Transfer Protocol Secure ([HTTPS](https://en.wikipedia.org/wiki/HTTPS/)) by default, a form of encryption to protect web traffic. Navigate to `Menu ‣ Add-ons` and click on the `+` symbol next to HTTPS Everywhere.

    Also add [uBlock Origin](https://en.wikipedia.org/wiki/UBlock_Origin/), a free and open source content filter — and a good one at that! Navigate to `Menu ‣ Add-ons` and click on the `+` symbol next to uBlock Origin. Various settings allow to increase your privacy, as detailed in the instructions below.

    ??? tip "Show me the step-by-step guide for Android"

        * Navigate to `Menu ‣ Add-ons ‣ uBlock Origin`
        * Make sure uBlock Origin is enabled, the large power button needs to be blue
        * Click on the Dashboard button
        * Open the tab `Filter lists`
        * Select the checkboxes shown in the table below and click on `Apply changes`

        <center>

        | Section | Check box |
        | ------ | ------ |
        | Ads | ☑ AdGuard Base <br> ☑ AdGuard Mobile Ads <br> ☑ EasyList <br> |
        | Privacy |  ☑ AdGuard Tracking Protection <br> EasyPrivacy <br> ☑ Fanboy's Enhanced Tracking List <br> |
        | Annoyances |  ☑ AdGuard Annoyances <br> ☑ AdGuard Social Media <br> ☑ Anti-Facebook <br> ☑ EasyList Cookie <br> ☑ Fanboy's Annoyance <br>  ☑ Fanboy's Social <br> ☑ uBlock filters - Annoyances <br> |

        </center>

<div style="margin-top:-20px">
</div>

??? tip "Show me a summary video"

    <center>

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/ae911b43-b4bd-4059-8447-891e82bd2367" frameborder="0" allowfullscreen></iframe>

    </center>

??? info "Tell me more about privacy extensions"

    **Use extensions with parsimony**. They facilitate [fingerprinting](https://en.m.wikipedia.org/wiki/Device_fingerprint/), a practice used to collect information, track browsing habits and deliver targeted advertising. The more extensions, the more unique your fingerprint, and the larger the attack surface. Want to know how easy it is to identify and track your browser? Head over to [EFF's Cover Your Tracks](https://coveryourtracks.eff.org/).

    **Use extensions with caution**. Some might break websites. Add new extensions progressively and disable them in case of negative impacts. It can be challenging to strike the right balance between privacy and usability.

    **Finally, some advice for social network users**. Don't check boxes in uBlock Origin's filter list section `Annoyances` if you use social sharing buttons from Facebook, Twitter and the like.


    | Extension | Description |
    | ------ | ------ |
    | [Clear URLs](https://addons.mozilla.org/en-US/firefox/addon/clearurls/) | Remove tracking elements from URLs. |
    | [Cookie autodelete](https://addons.mozilla.org/en-US/firefox/addon/cookie-autodelete/) | Automatically delete unused cookies when closing tabs. |
    | [I don't care about cookies](https://addons.mozilla.org/en-US/firefox/addon/i-dont-care-about-cookies/) | Get rid of cookie warnings. |
    | [Miner block](https://addons.mozilla.org/en-US/firefox/addon/minerblock-origin/) | Block cryptocurrency miners. |
    | [Cloud firewall](https://addons.mozilla.org/en-US/firefox/addon/cloud-firewall/) | Block connections to cloud services from Google, Amazon, Facebook, Amazon, Microsoft, Apple and Cloudflare. | 
    | [CSS exfil protection](https://addons.mozilla.org/en-US/firefox/addon/css-exfil-protection/) | Guard your browser against data theft from web pages using CSS. |
    | [Disconnect](https://addons.mozilla.org/en-US/firefox/addon/disconnect/) | Visualise and block web tracking. |
    | [Noscript](https://noscript.net/) | Allow only trusted web sites to execute JavaScript, Java, Flash and other plugins. |
    | [HTTPS everywhere](https://addons.mozilla.org/en-US/firefox/addon/https-everywhere/) | Encrypt web traffic, make browsing more secure. |
    | [Privacy badger](https://addons.mozilla.org/en-US/firefox/addon/privacy-badger17/) | Stop advertisers and other third-party trackers from secretly spying on you. |
    | [Decentraleyes](https://addons.mozilla.org/en-US/firefox/addon/decentraleyes/) | Block tracking via content delivery networks operated by third parties. |
    | [Terms of service; didn't read](https://addons.mozilla.org/en-US/firefox/addon/terms-of-service-didnt-read/) | Understand websites' terms & privacy policies, with ratings and summaries. |


<br>

<center> <img src="../../assets/img/separator_settings.svg" alt="Firefox hardening" width="150px"></img> </center>

## Privacy & security settings

=== "Windows, macOS & Linux"

    Open a new tab in Firefox. Remove any clutter from the empty tab, such as `Top Sites` or `Highlights`. Next, type `about:preferences` in the address bar to access Firefox's privacy and security settings. More detailed instructions below.

    ??? tip "Show me the step-by-step guide for Windows, macOS & Linux (Ubuntu)"

        *Warning*: Apply these settings with caution, some might break web sites. Add new settings progressively and disable them in case of negative impacts.

        <center>

        | Menu | Settings |
        | ------ | ------ |
        | General | In the section `Language`, uncheck the box `Check your spelling as you type`. |
        | General | In the section `Browsing`, uncheck the two boxes `Recommend extensions as you browse` and `Recommend features as you browse`. |
        | Home | In the section `Firefox Home Content`, uncheck the boxes `Top Sites`, `Highlights` and `Snippets`. |
        | Search | In the section `Search Suggestions`, uncheck the box `Provide search suggestions`. |
        | Search | In the section `Search Shortcuts`, remove Google, Bing, Amazon and Ebay. |
        | Search | Browse to [Disroot Searx](https://search.disroot.org/) and add it to the default search engines by clicking on the 3-dot actions menu in the address bar. <br><br> *Remark*: You'll find more suggestions on privacy respecting search engines at the end of this section. |
        | Search | Go back to the section `Default Search Engine` in Firefox's settings and select Disroot SearX. |
        | Privacy & Security | In the section `Tracking Protection`, select `Always Send websites a Do Not Track signal`.|
        | Privacy & Security | In the section `Cookies and Site Data`, select `Delete cookies and site data when Firefox is closed`. Then click on `Clear Data` and erase all cookies and site data stored by Firefox. |
        | Privacy & Security | In the section `Logins and Passwords`, unselect `Ask to save logins and passwords for websites`. |
        | Privacy & Security | In the section `History`, select `Use custom settings for history`. <br><br>Uncheck the boxes `Remember browsing and download history` and `Remember search and form history`. <br><br>Instead, check the box `Clear history when Firefox closes`. Then click on `Clear History` and erase all data stored by Firefox. <br><br>*Remark*: this is a work-around, as for some obscure reason `Never remember history` breaks many add-ons. |
        | Privacy & Security | In the section `Firefox Data Collection and Use`, uncheck all entries. |
        | Privacy & Security | In the section `HTTPS-Only Mode`, select `Enable HTTPS-Only Mode in all windows`. This enables the Hypertext Transfer Protocol Secure ([HTTPS](https://en.wikipedia.org/wiki/HTTPS/)) by default, a form of encryption which protects web traffic. A green lock should show up in Firefox's address bar each time you navigate to a website. |
        | General | (Optional) Check the box `Always check if Firefox is your default browser` and click on `Make Default`. |

        </center>


=== "Android"

    Launch Firefox. On the welcome screen, scroll down and click on the button `Start browsing`. Remove any clutter from the empty tab, such as `Google` or `Top Articles`. Next, navigate to `Menu ‣ Settings` and adjust Firefox's privacy and security settings. More detailed instructions below.

    ??? tip "Show me the step-by-step guide for Android"

        *Warning*: Apply these settings with caution, some might break web sites. Add new settings progressively and disable them in case of negative impacts.

        <center>

        | Menu | Settings |
        | ------ | ------ |
        | Search | In the section `Default search engine`, remove Google, Bing, Amazon and Qwant. |
        | Search | In the section `Default search engine`, click on `+ Add search engine`. Under `Other`, fill in the following:<br><br> • `Name`: `Disroot SearX`<br><br> •`Search string to use`: `https://search.disroot.org/search?q=%s` <br><br>Then click on `🗸` to apply all changes. <br><br>*Remark*: You'll find more suggestions on privacy respecting search engines at the end of this section. |
        | Search | Back in the section `Default search engine`, select `Disroot SearX`. |
        | Search | In the section `Address bar`, disable the options `Autocomplete URLs`, `Show clipboard suggestions`, `Search browsing history` and `Show search suggestions`. |
        | Customise | In the section `Home`, disable `Show most visited sites`. |
        | Logins and passwords | Change the option `Save logins and passwords` to `Never save`. |
        | Logins and passwords | Disable `Autofill`. |
        | Enhanced Tracking Protection | Make sure the option `Enhanced Tracking Protection` is enabled. Select between `Standard` and `Strict` settings. Stricter settings will block more trackers and ads, but are also more likely to break websites.|
        | Delete browsing data on quit | Enable the option `Delete browsing data on quit`. If you want open tabs to be restored after closing Firefox, unselect `Open tabs`. |
        | Data collection | Disable `Usage and technical data`, as well as `Marketing data`. |
        | General | (Optional) Enable `Set as default browser`. |

        </center>

<div style="    margin-top: -20px;">
</div>

??? tip "Show me a summary video"

    <center>

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/7e4abf0d-c96b-4455-bb44-6951e1b0619c" frameborder="0" allowfullscreen></iframe>

    </center>


??? info "Tell me more about privacy-respecting search engines"

    <center>

    | Search engine | Description |
    | ------ | ------ |
    | [Searx](https://asciimoo.github.io/searx/) |  Open source meta search engine. Aggregates anonymous search results from various engines. [Various instances are accessible online](https://searx.space), for example from [Disroot](https://search.disroot.org/). Can also be self-hosted. |
    | [Duckduckgo](https://duckduckgo.com/) | US-based meta search engine, mainly aggregates Bing/Yahoo results. |
    | [Ecosia](https://www.ecosia.org/) | German meta search engine, mainly provides Bing results and plants trees. |
    | [Swisscows](https://swisscows.com/) | Swiss meta search engine, mainly provides Bing results. |
    | [Mojeek](https://www.mojeek.com/) | UK based search engine. |
    | [Metager](https://metager.de/) | German open source meta search engine. |
    | [Qwant](https://www.qwant.com/) | French meta search engine,  VC-funded (large multimedia company Axel-Springer is an investor). |
    | [Startpage](https://www.startpage.com/) | Dutch meta search engine, mainly provides Google results. System 1, an ad company, is shareholder since October 2019. |

    </center>


<br>

<center> <img src="../../assets/img/separator_privacysettings.svg" alt="Hardened Firefox" width="150px"></img> </center>

## User.js

Firefox offers a whole range of advanced privacy and security settings. On desktop devices, these can be accessed by typing `about:config` in the address bar and confirming a security warning. Given the sheer amount of options, this can however quickly become tedious. An easier way is to install a `user.js` file. This small JavaScript file contains a number of pre-configured settings and loads each time you launch Firefox. More details on how to install the `user.js` file below!

??? tip "Show me a detailed step-by-step guide for Windows, macOS & Linux (Ubuntu)"

    *Warning*: use `user.js` files with caution. The stricter the privacy level, the more websites might break!

    Back up your current configuration, which is stored in a file named `pref.js`:

    <center>

    | Instructions | Description |
    | ------ | ------ |
    | Step 1 | Type `about:support` in Firefox's address bar. |
    | Step 2 | Go to `Application Basics`. |
    | Step 3 | Click on `Open Directory`. |
    | Step 4 | Back up a copy of the `pref.js` file. |

    </center>

    Now download your preferred `user.js` template. Find out more about available templates at the end of this section. Place the downloaded `user.js` file in the same folder as the `pref.js` file. Depending on your browser and operating system, this folder should be located here:


    <center>

    | OS | Path |
    | ------ | ------ |
    | Windows | `%APPDATA%\Mozilla\Firefox\Profiles\XXXXXXXX.your_profile_name\user.js` |
    | macOS | `~/Library/Application Support/Firefox/Profiles/XXXXXXXX.your_profile_name` |
    | Linux (Ubuntu) | `~/.mozilla/firefox/XXXXXXXX.default-release/user.js` |

    </center>

    If at any point you need to revert back to your initial settings, just restore the file `pref.js` and delete the `user.js` file.


??? tip "Show me a summary video"

    <center>

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/4b9255d5-3d9f-4319-bd3a-8214a396df18" frameborder="0" allowfullscreen></iframe>

    </center>


??? question "Where can I find user.js templates?"

    * [Ghacks](https://github.com/arkenfox/user.js) strikes a nice balance between privacy and usability. More information on the [wiki page](https://github.com/ghacksuserjs/ghacks-user.js/wiki/)
    * [Pyllyukko](https://github.com/pyllyukko/user.js/)
    * [Relaxed Pyllyukko](https://github.com/pyllyukko/user.js/tree/relaxed/)
    * [Privacy handbuch minimal configuration (German)](https://www.privacy-handbuch.de/download/minimal/user.js)
    * [Privacy handbuch moderate configuration (German)](https://www.privacy-handbuch.de/download/moderat/user.js)
    * [Privacy handbuch strict configuration (German)](https://www.privacy-handbuch.de/download/streng/user.js)
    * [A tool to make your own user.js file](https://ffprofile.com/)
    * [A tool to compare user.js files](https://reckendrees.systems/comparison/)
    * [Explanation of common user.js settings](http://kb.mozillazine.org/About:config_Entries/)


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Firefox support" width="150px"></img> </center>

## Support

For further details or questions, refer to:

* [Firefox's support documentation](https://support.mozilla.org/en-US/products/firefox/get-started) or ask [the Firefox community](https://support.mozilla.org/en-US/questions/new) for help.

* [uBlock's official documentation](https://github.com/gorhill/uBlock/wiki/) or one of the many [online tutorials](https://www.maketecheasier.com/ultimate-ublock-origin-superusers-guide/) if you're interested in more advanced usage.

<center>
<img align="center" src="https://imgs.xkcd.com/comics/perspective.png" alt="Firefox"></img>
</center>

<br>
