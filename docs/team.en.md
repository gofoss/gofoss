---
template: main.html
title: The project team
description: GoFOSS is a volunteer-run and not-for-profit project. Meet the team. Georg Jerska. Lenina Helmholtz. Tom Parsons.
---

# The project GoFOSS team

<table style="table-layout: fixed; width: 100%">

<thead>
    <tr>
        <th style="width:33%">Georg Jerska</th>
        <th style="width:33%">Lenina Helmholtz</th>
        <th style="width:33%">Tom Parsons</th>
    </tr>
</thead>

<tbody>
    <tr>
        <td><img src=../../assets/img/georg.png alt=Georg style="height:200px; width:200px"><br><br>Georg works in the performing arts. He spent part of his career in Germany, and has a special interest in protecting the privacy of citizens.</td>
        <td><img src=../../assets/img/lenina.png alt=Lenina style="height:200px; width:200px"><br><br>Lenina has a technical background and gives college lectures. She is always curious about new FOSS tools.</td>
        <td><img src=../../assets/img/tom.png alt=Tom style="height:200px; width:200px"><br><br>Tom is very social and believes in a better internet. He hopes to contribute good work to the project.</td>
    </tr>
</tbody>

</table>