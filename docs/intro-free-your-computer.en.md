---
template: main.html
title: How to install Linux on your computer
description: What is Linux? Which Linux OS is the best? Is Linux better than Windows? Is Linux better than macOS? What is Ubuntu?
---

# Free your computer, switch to Linux

<center>
<html>
<embed src="../../assets/echarts/os_stats.html" style="width: 100%; height:520px">
</html>
</center>

The average user spends [between two and six hours a day](https://www.bondcap.com/report/itr19/) at the computer. Most of these machines run Windows. Some run macOS. And a few run ChromeOS. Consequently, users (un)willingly share loads of data with [Microsoft](https://privacy.microsoft.com/en-us/privacystatement/), [Apple](https://www.apple.com/legal/privacy/en-ww/) or [Google](https://policies.google.com/privacy?hl=en/). Is this situation inevitable? No, it's not — the beauty about computers is that they are, [by design](https://en.wikipedia.org/wiki/Turing_completeness/), capable of running all kinds of operating systems.

[Linux](https://en.wikipedia.org/wiki/Linux/) is such an operating system, and a great alternative to users who don't trust Big Tech with their data. Long relegated to a niche existence, Linux reached maturity. It even boasts unique advantages compared to its closed-source rivals: Linux is free and open source, respects user privacy, runs on older hardware, offers strong community support and is easy to keep up to date.


??? info "Tell me more about concerns with Windows, macOS & ChromeOS"

    <center>

    | Concerns | Description |
    | ------ | ------ |
    |Privacy |Microsoft, Apple and Google collect, use, transfer and disclose user data, some anonymised, some not. |
    |Proprietary software |Windows, macOS and ChromeOS are, use or promote closed source software. There is no way to find out exactly what is going on inside a user's computer. |
    |Digital restrictions management (DRM) | Microsoft, Apple and Google use DRM to restrict what users can do with their computers. |
    |Programmed obsolescence |Microsoft's, Apple's and Google's update policy forces users to regularly renew functional software and hardware. At the user's and environment's expense. |
    |Lacking interoperability |Microsoft, Apple and Google design services incompatible with other options to lock their users in. |
    |Vulnerabilities |Despite being maintained by large corporations, Windows, macOS and ChromeOS present severe vulnerabilities and are targets for spyware, malware and viruses. |

</center>



??? question "Is Linux THE solution?"

    No. Linux is not for everyone. All depends on your needs, your hardware and your openness to change. But a switch to Linux is definitely worth a try. [Ubuntu](https://ubuntu.com/) or [Linux Mint](https://linuxmint.com) are two recommendable options for first time users. Everything more or less works out of the box, and both distributions are supported by large communities:

    * **Ubuntu**: check out the [user forum](https://ubuntuforums.org/), this other [user forum](https://askubuntu.com/) or the [wiki](https://wiki.ubuntu.com/)
    * **Linux Mint**: check out the [user forum](https://forums.linuxmint.com) or the [documentation page](https://linuxmint.com/documentation.php)

    Linux comes in many more flavors, called distributions or “distros”. They all have their specificities:

    <center>

    | Distribution | Description |
    | ------ | ------ |
    | [Distro chooser](https://distrochooser.de/en/) | Great site to guide your choice. |
    | [Debian](https://www.debian.org/) | One of the most widely used distributions, consisting entirely of free software (with the possibility to add proprietary elements). Openly and transparently handles security vulnerabilities. Offers great support, for example in this [forum](https://forums.debian.net/) and [wiki](https://wiki.debian.org/). |
    | [Fedora](https://getfedora.org/) | Innovative, free, and open source platform for hardware, clouds, and containers. |
    | [Arch](https://archlinux.org/) | Lightweight and flexible Linux distribution that tries to Keep It Simple (KISS). Arch Linux does not use a graphical interface to install or configure your operating system. The Arch community offers great support, for example in this [forum](https://bbs.archlinux.org/) and [wiki](https://wiki.archlinux.org/). |
    | [Qubes OS](https://www.qubes-os.org/) | Open source operating system designed to provide strong security. Used by Edward Snowden. |
    | [Tails](https://tails.boum.org/) | Live operating system that runs on almost any computer from a USB stick or a DVD. Aims at preserving privacy and anonymity, and circumventing censorship by forcing internet connections through the Tor network. |
    | [Knoppix](http://www.knopper.net/knoppix/index-en.html) | Bootable Live system on CD, DVD or USB flash drives. |
    | [Pure OS](https://pureos.net/) | User friendly, secure and freedom respecting OS for your daily usage. |
    | [MX Linux](https://mxlinux.org/) | Elegant and efficient desktop with simple configuration, high stability, solid performance and medium-sized footprint. The MX Linux community offers great support, for example on this [forum](https://forum.mxlinux.org/) and [wiki](https://mxlinux.org/wiki/). |
    | [Manjaro](https://manjaro.org/) | Professionally made operating system, suitable replacement for Windows or MacOS. Contains proprietary elements such as multimedia codecs. The Manjaro community offers great support, for example in this [forum](https://forum.manjaro.org/) and [wiki](https://wiki.manjaro.org/index.php/Main_Page). |
    | [Trisquel](https://trisquel.info/) | Fully free operating system for home users, small enterprises and educational centers. Derived from Ubuntu. Fully free, without proprietary software. |
    | [Gentoo](https://www.gentoo.org/) | Highly flexible, source-based Linux distribution. |
    | [Alpine Linux](https://www.alpinelinux.org/) | Security-oriented, lightweight Linux distribution based on musl libc and busybox. |
    | [Parrot](https://parrotsec.org/) | Free and open source GNU/Linux distribution designed for security experts, developers and privacy aware people. |

    </center>


??? info "I’d just like to interject for a moment..."

    What you’re referring to as Linux, is in fact, GNU/Linux, or as I’ve recently taken to calling it, GNU plus Linux. Linux is not an operating system.

    Many computer users run a modified version of the GNU system every day, without realizing it. Through a peculiar turn of events, the version of GNU which is widely used today is often called "Linux", and many of its users are not aware that it is basically the GNU system, developed by the GNU Project. There really is a Linux, and these people are using it, but it is just a part of the system they use.

    Linux is the kernel: the program in the system that allocates the machine’s resources to the other programs that you run. The kernel is an essential part of an operating system, but useless by itself; it can only function in the context of a complete operating system. Linux is normally used in combination with the GNU operating system: the whole system is basically GNU with Linux added, or GNU/Linux. All the so-called "Linux" distributions are really distributions of GNU/Linux!

    Check out [https://gnu.org](https://www.gnu.org/gnu/incorrect-quotation.html/) to learn more about Linux and GNU.

<br>

<div align="center">
<img src="https://imgs.xkcd.com/comics/linux_user_at_best_buy.png" alt="Free your computer"></img>
</div>

<br>
