---
template: main.html
title: Encrypted communication
description: What is transport layer encryption? What is end-to-end encryption? What encryption algorithms exist? What is a private key? What is a public key?
---

# Speak freely, keep your conversations private

Encrypt your communication. This makes it unreadable while travelling from one device to another. Only the recipient will know what you have to say.

**Transport layer encryption (TLS)** makes sure your communication can't be decoded while transiting on the Internet or between cell towers. However, intermediaries can access your data while passing it along or processing it. This includes many messenger services, social networks, search engines, banks, and so on.

<div align="center">
<img src="../../assets/img/tls.png" alt="Transport Layer Encryption" width="550px"></img>
</div>

**End-to-end encryption** protects your communication all the way. No one except the final recipient can decode it. Not even intermediaries passing along the data, such as for example an end-to-end encrypted messenger or email provider.

<div align="center">
<img src="../../assets/img/e2ee.png" alt="End-to-end Encryption" width="550px"></img>
</div>

??? tip "Tell me more about how encryption can help avoid online surveillance"

    <center>

    <iframe src="https://archive.org/embed/tips-to-help-avoid-online-surveillance" width="600" height="400" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

    *Courtesy of the [Electronic Frontier Foundation](https://www.eff.org/).*
    </center>


## Encrypted emails

Maybe you've already heard that most email providers:

* [sell private conversations to advertisers](https://www.wsj.com/articles/yahoo-bucking-industry-scans-emails-for-data-to-sell-advertisers-1535466959/)
* [take part in mass surveillance programs](https://www.nytimes.com/2016/10/06/technology/yahoo-email-tech-companies-government-investigations.html/)
* [suffer severe data breaches](https://en.wikipedia.org/wiki/Yahoo!_data_breaches/)
* [track their user's behavior](https://thenextweb.com/news/google-tracks-nearly-all-your-online-and-some-of-your-offline-purchases-see-it-for-yourself-here)

Nevertheless, the vast majority continues to entrust their communication to incumbent email providers. Main reason? Changing provider seems daunting. After all, many will argue that their current email address is "free" of charge, known by everybody and tied to all their accounts and subscriptions. Fortunately, changing email provider is not as hard as it seems. In this chapter, we'll discuss how to progressively move your emails to a privacy respecting provider.

<center>
<html>
<embed src="../../assets/echarts/email_stats.html" style="width: 100%; height:420px">
</html>
</center>


## Encrypted messengers

In this chapter, we'll also discuss how to encrypt your messages and calls. Nowadays, several mobile apps provide end-to-end encryption to protect your communication. Messages sent to or received via such apps can't be read by a middle man.

<br>
