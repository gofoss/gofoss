---
template: main.html
title: Contribute to the project
description: Contribute to GoFOSS. Join the community. Make suggestions. Improve the content. Help with translations. File  an issue on GitLab.
---

# Contribute to GoFOSS

<div align="center">
<img src="../../assets/img/contributing.png" alt="Contribute to GoFOSS" width="400px"></img>
</div>

This is a community project with the aim to make privacy respecting, free and open source software accessible to all. You can contribute by improving the content, helping with translations or just reaching out. Please read the following guidelines to learn about all the ways to get involved.


## Got a question?

Just reach out, no need to open an Issue on GitLab:

| Mastodon | Twitter | Reddit | Matrix | Email |
| :------: | :------: | :------: | :------: | :------: |
| [:material-mastodon:](https://hostux.social/@don_atoms/) | [:material-twitter:](https://nitter.net/gofoss_today) | [:material-reddit:](https://teddit.net/u/gofosstoday/) | [:material-matrix:](https://matrix.to/#/#gofoss-community:matrix.org/) | [:material-email:](mailto:gofoss@protonmail.com) |


## Want to help improve GoFOSS?

Feel like suggesting new or improved content? Miss some features on the website? Found a bug? Think you can help translate GoFOSS to a different language? Please follow the guidelines below to get involved.

??? tip "Contribution guidelines for non-developers"

	<center>

	| Step | Guidelines |
	| ------ | ------ |
	| Reach out | [Get in contact](#got-a-question-or-problem) with the GoFOSS community. |
	| Search for Issues | Also, [search the Issue Tracker](https://gitlab.com/curlycrixus/gofoss/-/issues) for similar open or closed requests. |
	| Check the Roadmap | And [check the Roadmap](https://gofoss.today/roadmap/) to see if nothing similar is already in the pipeline. |
	| File an Issue | If nothing shows up, you can help us by [filing a new Issue](https://gitlab.com/curlycrixus/gofoss/-/issues) with a description of the content proposal, feature suggestion or bug. If you found a bug, please also provide a minimal reproduction scenario when filing the Issue.|
	| Wait for the response | We do our best to close Issues fast. In turn, we ask that you remain available for clarifying questions or corrections. |
	| Stick around | Please note that if we don't hear back from you, we may close your Issue. |

	</center>


??? tip "Contribution guidelines for developers"

	<center>

	| Step | Guidelines |
	| ------ | ------ |
	| Reach out | [Get in contact](#got-a-question-or-problem) with the development team. There is also a [matrix channel](https://matrix.to/#/#gofoss-dev:matrix.org/) where the devs hang out, coordinate and provide support (invite only). |
	| Check the code | The complete website source code is available on [GitLab](https://gitlab.com/curlycrixus/gofoss). If you know how to code, you are free to submit Pull Requests with improved content, bug fixes or feature implementations. |
	| File an Issue | Start by [filing an Issue](https://gitlab.com/curlycrixus/gofoss/-/issues) with a description of the changes. |
	| Wait for validation | Wait for a validation by the project team. |
	| Fork the project | Once you got the green light, set up the development environment and fork the repository: <br>• install the lastest python version (at the time of writing, version 3.8 under Ubuntu) and dependencies: `sudo apt install python3 python3-pip python3-testresources` <br>• FIXME: create & activate a virtual environment: `sudo pip install virtualenv && virtualenv venv && source ./venv/bin/activate` <br>• create a local project folder, e.g. `mkdir ~/git` <br>• clone the project: `cd ~/git && git clone https://gitlab.com/curlycrixus/gofoss.git` <br>• FIXME: checkout: `cd gofoss && git checkout maint` <br>• ignore virtual environment & build files when committing changes: open the file `vi .gitignore` and add the two lines `venv/` and `site/` <br>• configure the git user who will commit: `git config user.email "<USER>@example.com" && git config user.name "<USERNAME>"` |
	| Update your repository | After the initial git checkout, it's good practice to regularly (daily) update your local repository using `git pull --rebase`. |
	| Install Material for Mkdocs | <br>• install Material for Mkdocs: `pip install mkdocs-material` <br>• install the [multi-language plugin](https://ultrabug.github.io/mkdocs-static-i18n/en/): `pip install mkdocs-static-i18n` <br>• install dependencies: `pip install -r requirements.txt`|
	| Develop | Add your developments, fixes or features. Make sure everything works fine. Preview changes in your browser on `localhost:8000` with the command `mkdocs serve`. |
	| Push | If everything works fine, add clear comments to your commits and push your changes to a separate git branch: FIXME (this command pushes to the master branch, right?): `git add . && git commit -m "COMMENT" && git push` |
	| Send a Pull Request | Send a Pull Request to FIXME: `gofoss:master`. |
	| Wait for validation | Wait for the project team to review the changes. |
	| Delete your branch | After your Pull Request is merged, you can safely delete your branch and pull the changes from the main (upstream) repository. |

	</center>

	Please note that the overall **directory structure** must remain unchanged:

	```bash
	.
	├─ docs/
	│  ├─ assets/
	│  │  └─ css/			# stylesheets (*.css), local fonts (*.ttf) & admonition icons (*.svg)
	│  │  └─ echarts/		# echart files (*.html, *.min.js)
	│  │  └─ img/			# image files (*.svg, *png, etc.)
	│  └─ *.xyz.md			# multi-language markdown files (*.en.md, *.fr.md, *.de.md, etc.)
	├─ overrides/			# theme extensions (home_xyz.html, main.html, overrides.xyz.min.css)
	├─ LICENSE			# mkdocs-material license
	├─ netlify.toml		# build instructions for netlify deploy
	├─ requirements.toml		# build dependencies
	├─ runtime.txt			# python version for netlify deploy
	└─ mkdocs.yml			# mkdocs configuration file
	```


??? tip "Contribution guidelines for translators"

	If you are reasonably literate in one of the target languages and have basic knowledge of markdown syntax, join the appropriate mailing list:

    * [EN ‣ FR translation team (Framasoft community)](https://framalistes.org/sympa/info/framalang/)
    * [EN ‣ DE translation team](https://framalistes.org/sympa/info/gofosslang_de/)
    * [EN ‣ ES translation team](https://framalistes.org/sympa/info/gofosslang_es/)


## Want to donate?

GoFOSS is a volunteer-run, non-profit project. No ads, no tracking, no sponsored content, no affiliates. At this stage, operational costs remain manageable and we don't seek financial contributions.

Instead, consider [contributing to the project](#contribute-to-gofoss). Or supporting other [dedicated coders building free, open source software](https://gofoss.today/thanks/). Or helping digital rights groups and software foundations to advocate for better privacy laws, user rights and the freedom to innovate:

* the [Electronic Frontier Foundation](https://www.eff.org/)
* the [Free Software Foundation](https://www.fsf.org/)
* the [Linux Foundation](https://www.linuxfoundation.org/)
* the [Apache Software Foundation](https://www.apache.org/)
* the [Freedom of the Press Foundation](https://freedom.press/)
* and many more

<div align="center">
<img src="https://imgs.xkcd.com/comics/fixing_problems.png" alt="Contributing to GoFOSS"></img>
</div>

<br>
