---
template: main.html
title: A guide towards Internet privacy
description: GoFOSS is a guide to replace services from large tech firms with free, open source and privacy respecting software.
---

# We believe in a free, open and privacy respecting Internet

<center> <img src="../../assets/img/gofoss_favicon.png" alt="GoFOSS" width="150px"></img> </center>

[GoFOSS](https://gofoss.today/) is a guide to replace services from large tech firms with free, open source and privacy respecting software.

The project is volunteer-run and not-for-profit. Our small team of open source enthusiasts came together in 2020 to create an accessible guide towards a free, secure and privacy respecting Internet.

We deplore that many tech companies generate profits at the expense of user privacy. And we are increasingly concerned about governments intruding into the private sphere of citizens.

Freedom and privacy are fundamental human rights. We believe everyone should be able to use their devices in a secure and private manner. Browsing the Internet, using social networks, sharing media or collaborating at work — these things need to be possible without someone recording, monetizing or censoring data.

<br>