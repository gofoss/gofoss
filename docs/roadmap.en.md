---
template: main.html
title: A glimpse into the future
description: The GoFOSS Roadmap 2020 - 2021. A glimpse into the future. New features. Upgrades. Content updates. Issue fixes.
---

# Roadmap 2020 - 2022

<center> <img src="../../assets/img/roadmap.png" alt="Roadmap" width="500px"></img> </center>

We'll continue adding new content and features, creating localised versions and keeping everything tidy and up to date. There will be regular minor releases for corrections and maintenance, and quarterly major releases to introduce new features and content.


##Q1 2022

| Change | Description |
| ------ | ------ |
|New features | -- |
|Web development |• Upgrade [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/upgrading/) <br>• Upgrade [E-Charts](https://echarts.apache.org/) |
|Content updates |• Update Ubuntu instructions to [LTS 22.04](https://ubuntu.com/about/release-cycle) <br>• Update server hardening section <br>• Add [Nextcloud](https://nextcloud.com/) |
|Issue fixes |• Fix typos <br>• Fix broken links |


##Q4 2021

| Change | Description |
| ------ | ------ |
|New features |• Release localised version (French) |
|Web development |• Upgrade Material for MkDocs <br>• Upgrade E-Charts |
|Content updates |• Update cron jobs screencasts <br>• Add GPGP to email section <br>• Add Bitwaren & Yubico Authenticator to password section <br>• Add Linux Mint to computer section |
|Issue fixes |• Verify iOS, macOS & Windows instructions for Element & Jami <br>• Fix typos <br>• Fix broken links |


##Q3 2021

| Change | Description |
| ------ | ------ |
|New features |• Published source code on Gitlab<br>• Added dark mode <br>• Released localised version (German) <br>• Added README <br> • Added contribution guidelines |
|Web development |• Upgraded Material for MkDocs <br>• Overhauled layout (landing page, fonts, icons, admonitions, png files, etc.) <br>• Upgraded E-Charts |
|Content updates |• Added browser overview to Firefox section <br>• Added VPN providers (RiseupVPN, CalyxVPN) <br>• Added encrypted messengers (Element, Jami, Briar, Silence) <br>• Added FreeFileSync to backup section <br>• Curated FOSS apps list <br>• Curated Ubuntu apps list <br>• Added cloud providers (Chatons, Digitalcourage, Picasoft) <br>• Added Fediverse (Mastodon, PeerTube, PixelFed, Friendica, Lemmy, Funkwhale, etc.) <br>• Curated thanks section <br>• Updated roadmap |
|Issue fixes |• Fixed cron jobs for dehydrated & ClamAV <br>• Migrated Reddit & Twitter links to Teddit & Nitter <br>• Fixed typos <br>• Fixed broken links & deprecated services <br>• Fixed broken table headings |


##Q2 2021

| Change | Description |
| ------ | ------ |
|New features |• Migrated to Material for MkDocs <br> • Added screencasts <br>• Added social networks |
|Web development |• Overhauled layout (landing page, navigation, tables, colors, fonts, sections, etc.) <br>• Upgraded E-Charts <br>• Re-designed 3-star system for beginners, intermediate, advanced users |
|Content updates |• Updated all website sections <br>• Added Arkenfox to user.js section <br>• Provided specifics on Protonmail's encryption <br>• Curated Ubuntu apps list <br> • Added Secure domain section <br> • Added Xiaomi Mi A2 to CalyxOS section <br>• Curated thanks section <br>• Added Roadmap |
|Issue fixes |• Removed links to CDNs <br> • Removed NordVPN |


##Q4 2020

| Change | Description |
| ------ | ------ |
|New features | -- |
|Web development |• Overhauled visual identity <br>• Updated website content & E-Charts |
|Content updates | -- |
|Issue fixes | -- |


##Q3 2020

| Change | Description |
| ------ | ------ |
|New features |• Launched [https://gofoss.today](https://gofoss.today/) |
|Upgrades | -- |
|Content updates | -- |
|Issue fixes | -- |


<br>