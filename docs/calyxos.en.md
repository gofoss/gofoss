---
template: main.html
title: How to install CalyxOS
description: Degoogle your phone. What is Calyxos? How to install Calyxos? What is microg? Is microg legal? Lineageos vs Calyxos? Grapheneos vs Calyxos?
---

# CalyxOS, a privacy-focussed mobile OS

<center>
<img align="center" src="../../assets/img/calyxos.png" alt="How to install CalyxOS" width ="600px"></img>
</center>

<br>

!!! level "This chapter is geared towards intermediate users. Some tech skills are required."

## Compatibility

[CalyxOS](https://calyxos.org) is a privacy-focussed Android mobile operating system, without Google's Play Services. It also allows to use [microG](https://microg.org/), which replaces Google's proprietary libraries with free and open-source code. CalyxOS focuses on privacy and security: safe communication with end-to-end encryption, private browsing with Tor and DuckDuckGo, automatic security updates, verified boot, and much more.

CalyxOS is currently available to the Pixel phone line as well as the Xiaomi Mi A2. If you own one of these phones, make sure your *exact* handset model is featured in [CalyxOS's device list](https://calyxos.org/get/). Also make sure your phone's bootloader is "unlockable". In the US for example, the bootloader of Verizon's Pixel phones can't be unlocked, which means that CalyxOS can't be installed.

Before switching to CalyxOS, keep in mind that while almost everything works just fine, [some apps don't play nice](https://github.com/microg/android_packages_apps_GmsCore/wiki/Implementation-Status/). Including a few apps from Google, such as Android Wear, Google Fit, Google Cast or Android Auto. Fortunately, there are great [FOSS alternatives](https://gofoss.today/foss-apps/) available. Finally, mind that using paid apps without Google's Play Store can be a little tricky.


<br>

<center> <img src="../../assets/img/separator_backups.svg" alt="Backup" width="150px"></img> </center>

## Backup

All data will be erased from your device during the installation process. Don't take any chances, [back up your phone](https://gofoss.today/backups/)!

<br>


<center> <img src="../../assets/img/separator_bug.svg" alt="Flash CalyxOS" width="150px"></img> </center>

## Preparation

### Firmware

Log into your computer and [download the CalyxOS firmware](https://calyxos.org/get/) for your device. Make sure to look for your *exact* handset model and download the corresponding `.zip` file to your computer. It should be called something like `XXX-factory-2021.XX.XX.XX.zip`.


### Flasher

To transfer — or *flash* — the CalyxOS firmware onto your phone, a so-called device flasher is required. More detailed instructions below.

=== "Windows"

    ??? tip "Show me the step-by-step guide for Windows"

        [Download the latest version of the device flasher for Windows](https://github.com/AOSPAlliance/device-flasher/releases/) to your computer. The file should be named something like `device-flasher.exe`. Also download and install [Google's USB drivers](https://developer.android.com/studio/run/win-usb/). Here the instructions for Windows 10:

        <center>

        | Steps | Description |
        | :------: | ------ |
        | 1 | Connect the Android device to the computer via USB. |
        | 2 | From Windows Explorer, open `Computer Management`. |
        | 3 | In the Computer Management's left pane, select `Device Manager`. |
        | 4 | In the Device Manager's right pane, locate and expand `Portable Devices` or `Other Devices`. |
        | 5 | Right-click the name of your device and select `Update Driver Software`. |
        | 6 | In the Hardware Update wizard, select `Browse my computer for driver software` and click `Next`. |
        | 7 | Click `Browse` and locate the Google USB driver folder. It should be located in `android_sdk\extras\google\usb_driver\`. |
        | 8 | Click `Next` to install the driver. |

        </center>


=== "macOS"

    ??? tip "Show me the step-by-step guide for macOS"

        [Download the latest version of the device flasher for macOS](https://github.com/AOSPAlliance/device-flasher/releases/) to your computer. The file should be named something like `device-flasher.darwin`.


=== "Linux (Ubuntu)"

    ??? tip "Show me the step-by-step guide for Linux (Ubuntu)"

        [Download the latest version of the device flasher for Linux](https://github.com/AOSPAlliance/device-flasher/releases/) to your computer. The file should be named something like `device-flasher.linux`.



### USB Debugging & OEM Unlocking

As stated before, the Pixel's bootloader must be "unlockable". In other words, you need to be able to activate the **OEM unlocking** function of your phone. More detailed instructions below.

??? tip "Show me the step-by-step guide"

    <center>

    | Steps | Description |
    | :------: | ------ |
    | 1 | Remove your SIM card and connect to a WiFi network. |
    | 2 | Open `Settings ► System ► About phone`. |
    | 3 | Tap seven times on `Build number` to enable the developer options. |
    | 4 | Go back, and select the `Developer options` menu. |
    | 5 | Scroll down, and check the `Android debugging` or `USB debugging` boxes. |
    | 6 | Enable `OEM Unlocking` in the `Developer options` menu. |
    | 7 | Turn off your phone. Reboot the phone into the so-called *bootloader* or *fastboot* mode by powering it off, and then holding down both `Volume DOWN` and `POWER` buttons. Release them once the word `FASTBOOT` appears on the screen. |
    | 8 | Plug your Android device into the computer. |
    | 9 | Change the USB mode to `file transfer (MTP)`. |

    </center>

<br>

<center> <img src="../../assets/img/separator_settings.svg" alt="Install CalyxOS" width="150px"></img> </center>


## Installation

=== "Windows"

    ??? tip "Show me the step-by-step guide"

        <center>

        | Instructions | Description |
        | :------: | ------ |
        |Prepare the files |On your computer, place the previously downloaded CalyxOS firmware file `XXX-factory-2021.XX.XX.XX.zip` as well as the `device-flasher.exe` file in the same folder. |
        |Run the flasher |Double-click the flasher to run it, and follow the on-screen instructions as detailed below. Alternatively, open a command prompt in the directory, run the flasher with the command `.\device-flasher.exe` and follow the on-screen instructions as detailed below. |
        | Unlock the bootloader | Use the volume and power keys to select `Unlock the bootloader`. |
        | Wait while flashing | Wait while your phone is being flashed, this can take some time. The device will reboot multiple times and show several screens. **Be patient and do not unplug the phone during the process!** |
        | Lock the bootloader | Once the on-screen instructions prompt you to, use the volume and power keys to select `Lock the bootloader`. |
        | Wait while booting | Wait until the on-screen instructions confirm that flashing is complete. The phone should reboot. The first start can take a while. |
        | Congrats! | CalyxOS is installed on your phone! Don't forget to disable `OEM Unlocking` in the `Developer options` menu. |

        </center>


=== "macOS"

    ??? tip "Show me the step-by-step guide"

        <center>

        | Instructions | Description |
        | :------: | ------ |
        |Prepare the files |On your computer, place the previously downloaded CalyxOS firmware file `XXX-factory-2021.XX.XX.XX.zip` as well as the `device-flasher.darwin` file in the same folder. |
        |Run the flasher |Double-click the flasher to run it, and follow the on-screen instructions as detailed below. Alternatively, open a terminal in the directory, run the flasher with the command `chmod +x ./device-flasher.darwin; ./device-flasher.darwin` and follow the on-screen instructions as detailed below. <br><br>*Note*: you may have to [disable macOS's Gatekeeper](https://support.apple.com/en-us/HT202491) for the installation to run properly. |
        | Unlock the bootloader | Use the volume and power keys to select `Unlock the bootloader`. |
        | Wait while flashing | Wait while your phone is being flashed, this can take some time. The device will reboot multiple times and show several screens. **Be patient and do not unplug the phone during the process!** |
        | Lock the bootloader | Once the on-screen instructions prompt you to, use the volume and power keys to select `Lock the bootloader`. |
        | Wait while booting | Wait until the on-screen instructions confirm that flashing is complete. The phone should reboot. The first start can take a while. |
        | Congrats! | CalyxOS is installed on your phone! Don't forget to disable `OEM Unlocking` in the `Developer options` menu. |

        </center>


=== "Linux (Ubuntu)"

    ??? tip "Show me the step-by-step guide"

        <center>

        | Instructions | Description |
        | :------: | ------ |
        |Prepare the files |On your computer, place the previously downloaded CalyxOS firmware file `XXX-factory-2021.XX.XX.XX.zip` as well as the `device-flasher.linux` file in the same folder. |
        |Run the flasher |Double-click the flasher to run it, and follow the on-screen instructions as detailed below. Alternatively, open a terminal in the directory, run the flasher with the command `sudo chmod +x ./device-flasher.linux; sudo ./device-flasher.linux` and follow the on-screen instructions as detailed below. |
        | Unlock the bootloader | Use the volume and power keys to select `Unlock the bootloader`. |
        | Wait while flashing | Wait while your phone is being flashed, this can take some time. The device will reboot multiple times and show several screens. **Be patient and do not unplug the phone during the process!** |
        | Lock the bootloader | Once the on-screen instructions prompt you to, use the volume and power keys to select `Lock the bootloader`. |
        | Wait while booting | Wait until the on-screen instructions confirm that flashing is complete. The phone should reboot. The first start can take a while. |
        | Congrats! | CalyxOS is installed on your phone! Don't forget to disable `OEM Unlocking` in the `Developer options` menu. |

        </center>


<br>

<center> <img src="../../assets/img/separator_microg.svg" alt="microG" width="150px"></img> </center>

## microG

CalyxOS ships with [microG](https://calyxos.org/tech/microg/). It's an open source replacement for Google's Play Services, which allows you to use features like push notifications or location without continually uploading your data to Google's servers. By default, microG is enabled without linking to any Google account.


=== "Location"

    By default, CalyxOS is configured to use location information from Mozilla. If instead you want to use a local database with cell tower information and be completely independent from third party providers, follow the instructions detailed below.

    ??? tip "Show me the step-by-step guide"

        <center>

        | Steps | Description |
        | :------: | ------ |
        | 1 | Open F-droid and install an app called `Local GSM location`. |
        | 2 | Head over to the microG settings app. Navigate to `Location modules` and enable `GSM Location Service` as well as `Nominatim`. |
        | 3 | Navigate to `Self-Check` and verify whether everything is set up properly. |

        </center>


=== "Push notifications"

    CalyxOS avoids using Google's services as much as possible. There's one exception: many apps rely on Google Cloud Messaging (GCM), a proprietary system developed by Google, to push notifications to your device. microG can provide access to push notifications by enabling (limited) use of the GCM service. More details below.

    ??? tip "Show me the step-by-step guide"

        <center>

        | Steps | Description |
        | :------: | ------ |
        | 1 | Open the microG settings app. |
        | 2 | Navigate to `Google device registration` and register your device. |
        | 3 | Navigate to `Google Cloud Messaging` and activate push notifications. |

        </center>

    ??? question "How can I trust CalyxOS if it uses Google Cloud Messaging?"

        Registering your device and enabling push notifications is optional. Doing so might provide limited data to Google, such as a unique ID. microG however makes sure to strip away as much identifying bits as possible. Activating push notifications can also enable Google to (partly) read the content of your notifications, depending on how apps use Google Cloud Messaging.


=== "F-Droid"

    [F-Droid](https://f-droid.org/) is an app store exclusively hosting free and open-source applications, as develop in a [previous chapter on FOSS apps](https://gofoss.today/foss-apps/). Make sure to enable microG's repository in F-Droid, as detailed below.

    ??? tip "Show me the step-by-step guide"

        <center>

        | Steps | Description |
        | :------: | ------ |
        |1 |Launch F-Droid. |
        |2 |Navigate to `Settings ► Repositories`. |
        |3 |Enable the microG repository. |

        </center>

=== "Paid apps"

    Installing paid apps without Google's Play Store can be a little tricky. Here a workaround:

    ??? tip "Show me the step-by-step guide"

        <center>

        | Steps | Description |
        | :------: | ------ |
        |1 |[Browse to Google's online play store](https://play.google.com/store/). |
        |2 |Buy apps with an old or disposable Google account. |
        |3 |Log into [Aurora store](https://f-droid.org/en/packages/com.aurora.store/) with the same Google credentials. |
        |4 |Download the purchased apps. |

        </center>

<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Support" width="150px"></img> </center>

## Support

For further details or questions, refer to [CalyxOS's documentation](https://calyxos.org/get/install/) or ask [CalyxOS's community](https://calyxos.org/community/) for help.

<div align="center">
<img src="https://imgs.xkcd.com/comics/xkcd_phone.png" alt="CalyxOS"></img>
</div>

<br>
