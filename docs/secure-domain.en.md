---
template: main.html
title: How to set up a secure domain
description: Selfhosting. DuckDNS. Let's Encrypt. How to install OpenVPN? What is Pi-Hole? What is an Apache Virtual Host? What is a reverse proxy?
---

# How to securely access your server from anywhere

<div align="center">
<img src="../../assets/img/pihole.png" alt="Install Pi-Hole" width="450px"></img>
</div>

!!! level "This chapter is geared towards advanced users. Solid tech skills are required."

This section explains how to:

* point a (sub-)domain to your home IP address with DuckDNS
* encrypt traffic with Let's Encrypt
* access local services, use a privacy respecting DNS provider and block ads with Pi-Hole
* shield the server from the Internet with a reverse proxy
* securely access the server from anywhere in the world with OpenVPN

## Set up a domain with DuckDNS

<div align="center">
<img src="../../assets/img/duckdns.png" alt="DuckDNS" width="500px"></img>
</div>

Human-readable addresses are easier to remember than random IPs. Rather than typing `18.192.76.182` into their browser, most users prefer to navigate to [www.gofoss.today](https://gofoss.today/). For this to work, you need a unique name which points to your home IP address, also known as *domain name*. In addition, a domain name is required to encrypt traffic, as explained in the next section.

[DuckDNS](https://www.duckdns.org/) allows to register a (sub-)domain for free and dynamically points it to your home IP address. This means it even works if your IP address changes quite often. Simply head over to DuckDNS and register a (sub-)domain of your choice. As soon as the registration is confirmed, you will receive a unique token.

For the purpose of this tutorial, we've registered the (sub-)domain `gofoss.duckdns.org` and received the token `ebaa3bd3-177c-4230-8d91-a7a946b5a51e`.. This will allow us later on to access services on the server via addresses such as `https://myservice.gofoss.duckdns.org`.

<br>

<center> <img src="../../assets/img/separator_letsencrypt.svg" alt="Let's Encrypt certificate" width="150px"></img> </center>

## Encrypt traffic with Let's Encrypt

All traffic should be encrypted over HTTPS. The browser's address bar should always display a secure connection, no matter if you browse the Internet or access self-hosted services. This means you need SSL certificates – either buy them or get them for free from [Let's Encrypt](https://letsencrypt.org/), a trusted [Certificate Authority](https://en.wikipedia.org/wiki/Certificate_authority/). More detailed instructions below.

??? tip "Show me the step-by-step guide"

    ### Install Dehydrated

    Let's Encrypt needs to [verify that you actually own the domain](https://letsencrypt.org/docs/challenge-types/) before delivering any certificate. A nifty program called [Dehydrated](https://github.com/dehydrated-io/dehydrated/) makes this possible. Log into the server and download Dehydrated:

    ```bash
    sudo apt install git
    git clone https://github.com/dehydrated-io/dehydrated.git
    ```

    Navigate to the directory `dehydrated` and create a first configuration file:

    ```bash
    cd dehydrated
    vi domains.txt
    ```

    Add the following line, and make sure to provide your own domain name:

    ```bash
    *.gofoss.duckdns.org > gofoss.duckdns.org
    ```

    Save and close the file (`:wq!`), then create a second configuration file:

    ```bash
    vi config
    ```

    Add the following lines. Make sure to provide your own email address:

    ```bash
    CHALLENGETYPE="dns-01"
    BASEDIR=/etc/dehydrated
    HOOK="${BASEDIR}/hook.sh"
    CONTACT_EMAIL=gofoss@gofoss.com
    ```

    Save and close the file (`:wq!`), then create a third configuration file:

    ```bash
    vi hook.sh
    ```

    Add the following lines. Make sure to provide your own domain name, as well as your own token delivered by DuckDNS:

    ```bash
    DOMAIN="gofoss.duckdns.org"                         # provide your domain here
    TOKEN="ebaa3bd3-177c-4230-8d91-a7a946b5a51e"        # provide your token here

    case "$1" in
        "deploy_challenge")
            curl "https://www.duckdns.org/update?domains=$DOMAIN&token=$TOKEN&txt=$4"
            echo
            ;;
        "clean_challenge")
            curl "https://www.duckdns.org/update?domains=$DOMAIN&token=$TOKEN&txt=removed&clear=true"
            echo
            ;;
        "deploy_cert")
            ;;
        "unchanged_cert")
            ;;
        "startup_hook")
            ;;
        "exit_hook")
            ;;
        *)
            echo Unknown hook "${1}"
            exit 0
            ;;
    esac
    ```

    Save and close the file (`:wq!`).

    **Caution:** Above instructions need to be modified if you work with another domain than DuckDNS. Please refer to [Dehydrated's documentation](https://github.com/dehydrated-io/dehydrated/wiki/) or [Let's Encrypt's community](https://community.letsencrypt.org/) to properly set up the hook for DNS-based validation.

    The next set of commmands creates a directory where all SSL certificates will be stored, called `certs`. The files `dehydrated` and `hook.sh` are made executable. The `dehydrated` directory moves to `/etc/dehydrated`. Finally, `gofossadmin` is given the ownership of the directory `/etc/dehydrated`. Make sure to adjust the administrator name to your own setup:

    ```bash
    mkdir certs
    chmod a+x dehydrated
    chmod a+x hook.sh
    cd ..
    sudo mv dehydrated /etc/dehydrated
    sudo chown -R gofossadmin:gofossadmin /etc/dehydrated
    ```

    Check the directory layout:

    ```bash
    sudo ls -al /etc/dehydrated
    ```

    The output should look something like:

    ```bash
    drwxr-xr-x 2 gofossadmin gofossadmin   4096 Jan 01 00:00 .
    drwxr-xr-x 4 gofossadmin gofossadmin   4096 Jan 01 00:00 ..
    -rw-r--r-- 1 gofossadmin gofossadmin    200 Jan 01 00:00 certs
    -rw-r--r-- 1 gofossadmin gofossadmin    200 Jan 01 00:00 config
    -rwxr-xr-x 1 gofossadmin gofossadmin 700000 Jan 01 00:00 dehydrated
    -rw-r--r-- 1 gofossadmin gofossadmin    200 Jan 01 00:00 docs
    -rw-r--r-- 1 gofossadmin gofossadmin     20 Jan 01 00:00 domains.txt
    -rwxr-xr-x 1 gofossadmin gofossadmin    700 Jan 01 00:00 hook.sh
    ```


    ### Create certificates

    Register with Let's Encrypt:

    ```bash
    bash /etc/dehydrated/dehydrated --register --accept-terms
    ```

    This command creates a directory `/etc/dehydrated/accounts`, containing your registration information. The terminal should show something like:

    ```bash
    # INFO: Using main config file /etc/dehydrated/config
    + Generating account key...
    + Registering account key with ACME server...
    + Done!
    ```

    Now, create the SSL certificates:

    ```bash
    bash /etc/dehydrated/dehydrated -c
    ```

    The terminal should prompt something like:

    ```bash
    + Creating chain cache directory /etc/dehydrated/chains
    Processing gofoss.duckdns.org
    + Creating new directory /etc/dehydrated/certs/gofoss.duckdns.org ...
    + Signing domains...
    + Generating private key...
    + Generating signing request...
    + Requesting new certificate order from CA...
    + Received 1 authorizations URLs from the CA
    + Handling authorization for gofoss.duckdns.org
    + 1 pending challenge(s)
    + Deploying challenge tokens...
    OK
    + Responding to challenge for gofoss.duckdns.org authorization...
    + Challenge is valid!
    + Cleaning challenge tokens...
    OK
    + Requesting certificate...
    + Checking certificate...
    + Done!
    + Creating fullchain.pem...
    + Done!
    ```

    That's it! Two SSL keys have been created to encrypt all traffic to and from your self-hosted services. We'll explain later how to use these keys.

    <center>

    | File | Location |
    | ------ | ------ |
    | Public key | /etc/dehydrated/certs/gofoss.duckdns.org/fullchain.pem |
    | Private key | /etc/dehydrated/certs/gofoss.duckdns.org/privkey.pem |

    </center>


    ### Autorenew certificates

    Let's Encrypt's certificates expire after 90 days, and can be renewed 30 days before expiry. It's a good idea to automate these updates. Create a log file:

    ```bash
    sudo touch /var/log/dehydrated
    ```

    Next, create a weekly cron job which checks the certificate's validity and, if required, renews it:

    ```bash
    sudo vi /etc/cron.weekly/dehydrated
    ```

    Add the following content:

    ```bash
    #!/bin/sh

    MYLOG=/var/log/dehydrated
    echo "Checking cert renewals at `date`" >> $MYLOG
    /etc/dehydrated/dehydrated -c >> $MYLOG 2>&1
    ```

    Save and close the file (`:wq!`). Now, make the script executable:

    ```bash
    sudo chmod +x /etc/cron.weekly/dehydrated
    ```

    Test the cronjob:

    ```bash
    sudo bash /etc/cron.weekly/dehydrated
    sudo cat /var/log/dehydrated
    ```

    The terminal output should look something like:

    ```bash
    Checking cert renewals at Sun 01 Jan 2021 01:29:06 PM CEST
    # INFO: Using main config file /etc/dehydrated/config
    Processing *.gofoss.duckdns.org
     + Checking domain name(s) of existing cert... unchanged.
     + Checking expire date of existing cert...
     + Valid till Mar 12 10:23:18 2021 GMT (Longer than 30 days). Skipping renew!
    ```


??? tip "Show me a summary video"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/8ab6fc0e-fee9-4ca4-acc5-aa143c967e41" frameborder="0" allowfullscreen></iframe>
    </center>


??? question "What about self-signed certificates?"

    Using self-signed certificates for HTTPS encryption is possible. That said, they often won't be recognised by browsers and applications, or generate warnings and error messages. On Android phones for example, self-signed certificates must first be imported and trusted. For these reasons, we suggest to privilege a Certificate Authority like Let's Encrypt.


<br>

<center> <img src="../../assets/img/separator_pihole.svg" alt="Pi-hole" width="150px"></img> </center>

##  Manage traffic & block ads with Pi-Hole

<br>

<div align="center">
<img src="../../assets/img/pihole_interface.png" alt="Pi-hole interface" width="500px"></img>
</div>

[Pi-hole](https://pi-hole.net/) is a nifty tool: it's capable of resolving local addresses such as `https://myservice.gofoss.duckdns.org`, lets you pick an upstream DNS provider of your choice and blocks ads & trackers. Follow the instructions below to set up Pi-Hole on your server.

??? tip "Show me the step-by-step guide"

    ### Pi-Hole installation

    At the time of writing, 5.2 was the latest Pi-Hole release. It's quite lightweight, 50MB of free space and 512MB of RAM are enough to run it on Ubuntu. Pi-hole also requires a static IP address as well as Apache and PHP, which we [configured in previous chapters](https://gofoss.today/server-hardening-basics/). Run Pi-hole's install script:

    ```bash
    sudo curl -sSL https://install.pi-hole.net | bash
    ```

    Follow the on-screen instructions:

    <center>

    | Instruction | Description |
    | ------ | ------ |
    | This installer will transform your device into a network-wide ad blocker! | Hit `ENTER` to continue. |
    | The Pi-hole is free, but powered by your donations | Hit `ENTER` to continue. |
    | The Pi-hole is a SERVER so it needs a STATIC IP ADDRESS to function properly | Hit `ENTER` to continue. |
    | Choose An Interface | Select your network interface. Press `SPACE` to toggle the selection, `TAB` to confirm your selection, and `ENTER` to continue.|
    | Select Upstream DNS Provider. To use your own, select Custom | Select a DNS provider of your choice. In this example, we'll use [UncensoredDNS](https://blog.uncensoreddns.org/), but feel free to select any other DNS provider! We suggest to choose a DNS provider other than your ISP, Cloudfare or Google. Learn more about DNS providers at the end of this section. |
    | Pi-hole relies on third party lists in order to block ads. | Hit `ENTER` to continue. We'll explain later how to add more block lists. |
    | Select Protocols | Choose your protocol. IPv4 and IPv6 should be selected by default. Hit `OK` to continue. |
    | Do you want to use your current network settings as a static address? | In a previous section, we set up a [static IP address](https://gofoss.today/ubuntu-server/) for the server. In our example, it's `192.168.1.100`, adjust this to your own configuration. Hit `TAB` to select `Yes` and `ENTER` to continue. |
    | It is possible your router could still try to assign this IP to a device, which would cause a conflict | Read the warning, and hit `ENTER` to continue. |
    | Do you wish to install the web admin interface? | Select `On` if you want a web interface (recommended), and hit `ENTER` to continue. |
    | Do you wish to install the web server (lighttpd)? | We'll use Apache as web server, instead of lighttpd. Select `Off` and confirm with `SPACE`, then hit `ENTER` to continue. |
    | Do you want to log queries? | Select `On` (recommended) and hit `ENTER` to continue. |
    | Select a privacy mode for FTL | Select the privacy level of your choice, and hit `ENTER` to continue: <br><br> • `Show everything`: shows records everything; provides the maximum amount of statistics. <br> • `Hide domains`: displays and stores all domains as "hidden"; disables the Top Domains and Top Ads tables on the dashboard. <br> • `Hide domains and clients`: displays and stores all domains as "hidden" and all clients as "0.0.0.0"; disables all tables on the dashboard. <br> • `Anonymous mode`: disables basically everything except the live anonymous statistics; no history is saved to the database, nothing is shown in the query log, there are no top item lists. Provides enhanced privacy. |
    | Installation Complete! |Once the installation completes, you should see this:<br><br><center><img src="../../assets/img/pihole-installation.png" alt="Pi-hole installation" width="60%"></img></center> |

    </center>


    ### Web interface

    We're going to set up an Apache Virtual Host as a Reverse Proxy to access the Pi-hole web interface. Sounds complex, but in essence a reverse proxy further shields the server from the Internet. You can read more about it at the end of this section.

    Set the right permissions:

    ```bash
    sudo chown www-data:www-data -R /var/www/html/admin/
    sudo usermod -aG pihole www-data
    sudo chown -R pihole:pihole /etc/pihole
    ```

    Create an Apache configuration file:

    ```bash
    sudo vi /etc/apache2/sites-available/mypihole.gofoss.duckdns.org.conf
    ```

    Add the following content and make sure to adjust the settings to your own setup, such as domain names (`mypihole.gofoss.duckdns.org`), path to SSL keys, IP addresses and so on:

    ```bash
    <VirtualHost *:80>

        ServerName              mypihole.gofoss.duckdns.org
        ServerAlias             www.mypihole.gofoss.duckdns.org
        Redirect permanent /    https://mypihole.gofoss.duckdns.org/

    </VirtualHost>

    <VirtualHost *:443>

        ServerName              mypihole.gofoss.duckdns.org
        ServerAlias             www.mypihole.gofoss.duckdns.org
        ServerSignature         Off

        SSLEngine               On
        SSLProxyEngine          On
        SSLProxyCheckPeerCN     Off
        SSLCertificateFile      /etc/dehydrated/certs/gofoss.duckdns.org/fullchain.pem
        SSLCertificateKeyFile   /etc/dehydrated/certs/gofoss.duckdns.org/privkey.pem
        DocumentRoot            /var/www/html/admin

        <Location />
            Order deny,allow
            Deny from all
            Allow from 127.0.0.1
            Allow from 192.168.1.0/24
            Allow from 10.8.0.1/24
        </Location>

        <Directory /var/www/html/admin/>
            Options +FollowSymlinks
            AllowOverride All
            Require all granted
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/mypihole.gofoss.duckdns.org-error.log
        CustomLog ${APACHE_LOG_DIR}/mypihole.gofoss.duckdns.org-access.log combined

    </VirtualHost>
    ```

    Once the content is added, save and close the file (`:wq!`). Note how we enable SSL encryption with the instruction `SSLEngine On`, and use the SSL certicate `/etc/dehydrated/certs/gofoss.duckdns.org/fullchain.pem` as well as the private SSL key `/etc/dehydrated/certs/gofoss.duckdns.org/privkey.pem`, which were created earlier on.

    Next, enable the Apache Virtual Host (adjust the domain accordingly):

    ```bash
    sudo a2ensite mypihole.gofoss.duckdns.org.conf
    sudo systemctl reload apache2
    ```

    Restart Pi-hole and check if everything runs correctly. The output should display "Active":

    ```bash
    sudo systemctl restart pihole-FTL
    sudo systemctl status pihole-FTL
    ```


    ### Pi-Hole configuration

    Change the default password for Pi-Hole's web interface. [Provide a strong, unique password](https://gofoss.today/passwords/):

    ```bash
    sudo pihole -a -p
    ```

    Browse to `http://192.168.1.100/admin/` and login with the new credentials. Make sure to adjust the server's IP address to your own setup. The web interface should show on the screen. It provides various settings, [which are further described on Pi-Hole's website](https://github.com/pi-hole/AdminLTE/):

    <center>

    | Feature | Description |
    | ------ | ------ |
    | Gravity |Browse to `Tools ‣ Update Gravity` to fetch the latest block lists. Be patient, it can take some seconds. |
    | Dashboard |Displays statistics: how many domains have been visited or blocked, how many domains are on the blocklist, and so on. |
    | Queries |Detailed information on queries. |
    | Blocklists |Navigate to `Group management ‣ Adlists` to add more blocklists and filter additional ads and malware. You'll find more information about Pi-hole Blocklists at the of this section. |
    | Settings |Easily manage and configure Pi-hole: DNS servers, privacy, etc. |
    | Local DNS |Pi-hole is capable of resolving local addresses. Navigate to `Local DNS ‣ DNS Records` and add the following domain/IP combination (adjust according to your own setup): <br><br>`Domain`: `mypihole.gofoss.duckdns.org` <br> `IP Address`:  `192.168.1.100` |

    </center>



??? tip "Show me a summary video"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/09477516-5382-44f7-bcb5-c846375e723e" frameborder="0" allowfullscreen></iframe>
    </center>

??? info "Tell me more about DNS and privacy"

    On the Internet, each computer has its own [IP address](https://en.m.wikipedia.org/wiki/IP_address/). Since these IP addresses aren't easy to remember, the Domain Name System ([DNS](https://en.wikipedia.org/wiki/Domain_Name_System/)) was invented. It's like the phone book of the Internet: each time you visit a website, a DNS resolver looks up the corresponding IP address and connects your device to the appropriate computer/server. DNS therefore plays a key role when browsing the Internet. But since everything happens automatically, users often forget that DNS queries are an inherent risk to their privacy:

    <center>

    | Risks | Description |
    | ------ | ------ |
    |Logging |DNS resolvers log every site you visit. Depending on your current DNS resolver, [Google](https://en.wikipedia.org/wiki/Google_Public_DNS/), [Cloudflare](https://www.cloudflare.com/), your Internet Service Provider (ISP), your telecom provider or any other third-party sees and stores a list of all websites you visit. Whether or not the traffic is encrypted over HTTPS doesn't matter. |
    |No encryption |DNS queries are mostly unencrypted. Even if you fully trust your DNS resolver, others could eavesdrop and try to manipulate you ([spoofing attack](https://en.wikipedia.org/wiki/Spoofing_attack/)). |
    |Censorship |Providers can also censor online activities by tracking DNS queries. |

    </center>

    Pi-hole let's you choose which DNS resolver to use when browsing the Web. Here some privacy-aware DNS providers:

    <center>

    | DNS provider | Country | DNS #1 | DNS #2 |
    | ------ | ------ | ------  |------ |
    | [Digitalcourage](https://en.wikipedia.org/wiki/Digitalcourage/) | Germany  | 5.9.164.112 | -- |
    | [UncensoredDNS](https://blog.uncensoreddns.org/) | Denmark |89.233.43.71 | 91.239.100.100 |
    | [Dismail](https://dismail.de/info.html#dns/) | Germany |80.241.218.68  | 159.69.114.157 |
    | [DNS Watch](https://dns.watch/) | Germany  | 84.200.69.80 | 84.200.70.40 |
    | [FDN](https://www.fdn.fr/actions/dns/) | France | 80.67.169.12 | 80.67.169.40 |
    | [OpenNIC](https://servers.opennic.org/) | Various | Various | Various |

    </center>


??? info "Tell me more about ad blocking"

    Pi-Hole filters web traffic and blocks ads or trackers on your devices, without the need for any additional software. By navigating to Pi-Hole's web interface, you can add blocklists to filter additional ads and malware:

    <center>

    | Block lists | Description |
    | ------ | ------ |
    |Default |• https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts <br>• https://mirror1.malwaredomains.com/files/justdomains <br>• http://sysctl.org/cameleon/hosts <br>• https://zeustracker.abuse.ch/blocklist.php?download=domainblocklist <br>• https://s3.amazonaws.com/lists.disconnect.me/simple_tracking.txt <br>• https://s3.amazonaws.com/lists.disconnect.me/simple_ad.txt |
    |Advertisement |• https://adaway.org/hosts.txt <br>• https://v.firebog.net/hosts/AdguardDNS.txt <br>• https://s3.amazonaws.com/lists.disconnect.me/simple_ad.txt <br>• https://v.firebog.net/hosts/Easylist.txt <br>• https://raw.githubusercontent.com/StevenBlack/hosts/master/data/SpotifyAds/hosts <br>• https://raw.githubusercontent.com/StevenBlack/hosts/master/data/UncheckyAds/hosts |
    |Tracking & telemetry |• https://v.firebog.net/hosts/Airelle-trc.txt <br>• https://v.firebog.net/hosts/Easyprivacy.txt <br>• https://v.firebog.net/hosts/Prigent-Ads.txt <br>• https://raw.githubusercontent.com/StevenBlack/hosts/master/data/tyzbit/hosts |
    |Malicious sites |• https://v.firebog.net/hosts/Airelle-hrsk.txt <br>• https://s3.amazonaws.com/lists.disconnect.me/simple_malvertising.txt <br>• https://mirror1.malwaredomains.com/files/justdomains <br>• https://mirror.cedia.org.ec/malwaredomains/immortal_domains.txt <br>• https://www.malwaredomainlist.com/hostslist/hosts.txt <br>• https://bitbucket.org/ethanr/dns-blacklists/raw/8575c9f96e5b4a1308f2f12394abd86d0927a4a0/bad_lists/Mandiant_APT1_Report_Appendix_D.txt <br>• https://v.firebog.net/hosts/Prigent-Malware.txt <br>• https://v.firebog.net/hosts/Prigent-Phishing.txt <br>• https://v.firebog.net/hosts/Shalla-mal.txt <br>• https://raw.githubusercontent.com/StevenBlack/hosts/master/data/add.Risk/hosts |

    </center>


??? info "Tell me more about terminal commands"

    Most of Pi-Hole's configuration can be performed on the web interface. Alternatively, you can also configure Pi-Hole directly on the server, via the terminal. The full list of commands can be found on [Pi-Hole's website](https://docs.pi-hole.net/core/pihole-command/):

    <center>

    | Command | Description |
    | ------ | ------ |
    | pihole status | status |
    | pihole -t tail log | real time log |
    | pihole -c | statistics |
    | pihole -w -l | list whitelisted domains |
    | pihole -w example.com | add example.com to whitelist |
    | pihole -w -d example.com | remove example.com from whitelist |
    | pihole -b -l | list blacklisted domains |
    | pihole -b example.com | add example.com to blacklist |
    | pihole -b -d example.com | remove example.com from blacklist |
    | pihole -up | update pihole |
    | pihole -l off | query logging off |
    | pihole -l on | query logging on |
    | pihole enable | enable pihole |
    | pihole disable | disable pihole |
    | pihole disable 10m | disable pihole for 10 minutes |
    | pihole disable 60s | disable pihole for 60 seconds |
    | pihole uninstall | uninstall pihole |

    </center>


??? info "Tell me more about Reverse Proxies"

    A Reverse Proxy is a service sitting between the software running on the server and the Internet. All services suggested in this guide, including Pi-Hole, are set up as Apache Virtual Hosts behind a Reverse Proxy. This allows to:

    * prevent the server from being directly exposed to the Internet
    * run several services side by side on a single server
    * minimise the number of open ports (typically ports 80 and 443)
    * reach services over custom addresses, rather than random IP addresses and port numbers
    * easily manage SSL certificates
    * configure distinct security policies, such as HTTP headers, user authentication, access restrictions, and so on

    In Ubuntu, Apache Virtual Host configuration files are located in the directory `etc/apache2/sites-available`. Their structure more or less looks like this:

    ```bash
    <VirtualHost *:80>

        ServerName              example.com                         # address of the service
        ServerAlias             www.example.com                     # address of thee service, including www subdomain
        Redirect permanent /    https://example.com/                # redirection of all non-encrypted http:// traffic to encrypted https:// traffic

    </VirtualHost>

    <VirtualHost *:443>

        ServerName              example.com                         # address of the service
        ServerAlias             www.example.com                     # address of the service, including www
        ServerSignature         Off                                 # hide server information

        SSLEngine               On                                  # use SSL/TLS protocol
        SSLProxyEngine          On                                  # use SSL/TLS protocol for proxy
        SSLCertificateFile      /path/to/fullchain.pem              # location of the SSL certificate file in PEM format
        SSLCertificateKeyFile   /path/to/privkey.pem                # location of the PEM-encoded private SSL key file

        DocumentRoot            /var/www/example                    # directory from which Apache will serve files

        <Location />                                                # limit access to server, home network (LAN) and VPN
            Order deny,allow
            Deny from all
            Allow from 127.0.0.1
            Allow from 192.168.1.0/24
            Allow from 10.8.0.1/24
        </Location>

        <Directory /var/www/example>
            Options -Indexes +FollowSymLinks                        # prevent directory listings and follow symbolic links
            AllowOverride All                                       # directives from .htaccess file which can override configuration directives
            Require all granted
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/example.com-error.log            # log files
        CustomLog ${APACHE_LOG_DIR}/example.com-access.log combined # log files

    </VirtualHost>
    ```

    The Virtual Host is enabled with `sudo a2ensite example.com`. Check if there are any syntax errors with `sudo apachectl configtest`.



<br>

<center> <img src="../../assets/img/separator_openvpn.svg" alt="Install OpenVPN" width="150px"></img> </center>

## Secure remote access with OpenVPN

At this stage, the server is only accessible from within your home network, since it's shielded from the Internet by a firewall. One solution to enable remote access would be to "poke" multiple holes into the firewall and forward ports for each service from your router to the server. This would however expose multiple ports to the Internet and provide attackers with a larger attack surface to force their way into your network.

A Virtual Private Network ([VPN](https://en.wikipedia.org/wiki/Virtual_private_network/)) allows to connect to the server from anywhere in the world while mitigating this risk. Only one single port is made public, and only pre-authorised connections are allowed, requiring both a certificate and a password. Read on below for detailed instructions.

??? tip "Show me the step-by-step guide"

    ### OpenVPN installation

    [OpenVPN](https://openvpn.net/) is open source and uses OpenSSL, TLS and many security features. It can be set up with an [easy install script](https://github.com/Nyr/openvpn-install/):

    ```bash
    wget https://git.io/vpn -O openvpn-install.sh
    sudo bash openvpn-install.sh
    ```

    Follow the on-screen instructions:

    <center>

    | Prompt | Instructions |
    | ------ | ------ |
    | What is the public IPv4 address or hostname? | Check your public IPv4 address on [whatsmyip.org](https://www.whatsmyip.org/). Let's assume our public IP address is `88.888.88.88` (adjust accordingly).  |
    | What IPv6 address should the OpenVPN server use? | Select the first IPv6 address. |
    | Which protocol do you want for OpenVPN connections? | Choose the recommended UDP protocol.  |
    | What port do you want OpenVPN listening to? | Choose a port, for the purpose of this tutorial we'll use the standard port `1194`. |
    | Which DNS do you want to use with the VPN? | Since we're using Pi-Hole as DNS server, select the option `Current system resolvers`. |
    | Finally, tell me your name for the client certificate. | Choose a name for your first VPN client certificate. For the purpose of this tutorial, we'll create a certificate labelled `computer_vpn`, adjust accordingly. |

    </center>


    Once the installation completes, the terminal should look similar to this:

    ```bash
    Welcome to this OpenVPN road warrior installer!

    I need to ask you a few questions before starting setup.
    You can use the default options and just press enter if you are ok with them.

    This server is behind NAT. What is the public IPv4 address or hostname?
    Public IPv4 address / hostname [88.888.88.88]: 88.888.88.88

    What IPv6 address should the OpenVPN server use?
        1) 1a00:a0a:10a:10a0:a00:00ff:faf0:a011
        2) 1a00:a0a:10a:10a0:a00:00bb:faf0:a022
    IPv6 address [1]: 1

    Which protocol do you want for OpenVPN connections?
    1) UDP (recommended)
    2) TCP
    Protocol [1]: 1

    What port do you want OpenVPN listening to?
    Port [1194]: 1194

    Which DNS do you want to use with the VPN?
    1) Current system resolvers
    2) 1.1.1.1
    3) Google
    4) OpenDNS
    5) NTT
    6) AdGuard
    DNS [1]: 1

    Finally, tell me a name for the client certificate.
    Client name [client]: computer_vpn

    We are ready to set up your OpenVPN server now.

    [etc.]

    Finished!

    Your client configuration is available at: /home/gofossadmin/computer_vpn.ovpn
    If you want to add more clients, just run this script again!
    ```


    ### Certificates

    A first VPN client certificate, `computer_vpn.ovpn`, has been generated during the installation of OpenVPN. Create as many certificates as needed by re-running the install script, one for each device connecting to the server: desktop computers, laptops, tablets, phones, etc.

    For the purpose of this tutorial, we'll create a second certificate called `phone_vpn.ovpn`. Of course, you can choose any name for the certificates. Just make sure to adjust the commands accordingly:

    ```bash
    sudo bash openvpn-install.sh
    ```

    The terminal should display something similar to:

    ```bash
    Looks like OpenVPN is already installed.

    What do you want to do?
    1) Add a new user
    2) Revoke an existing user
    3) Remove OpenVPN
    4) Exit
    Select an option: 1

    Tell me a name for the client certificate.
    Client name: phone_vpn

    [...]

    Write out database with 1 new entries
    Data Base Updated

    Client phone_vpn added, configuration is available at: /home/gofossadmin/phone_vpn.ovpn
    ```

    Finally, set the correct permissions for the certificate(s):

    ```bash
    sudo chmod 755 computer_vpn.ovpn
    sudo chmod 755 phone_vpn.ovpn
    ```


    ### OpenVPN configuration

    Open port 1194 (UDP), or whatever port you specified during the installation of OpenVPN:

    ```bash
    sudo ufw allow 1194/udp
    ```

    Check if the port has been successfully added to the firewall rules:

    ```bash
    sudo ufw status numbered
    ```

    Also check your router settings and make sure the port used by OpenVPN is forwarded correctly. Refer to the router's manual for more information.

    Confirm whether the virtual interface `tun0` works, and get the name of the default subnet:

    ```bash
    ip ad | grep tun0
    ```

    Get the OpenVPN server's IP address:

    ```bash
    ip route | grep tun0
    ```

    Make sure traffic is routed via the VPN tunnel:

    ```bash
    sudo apt install traceroute
    traceroute 10.8.0.1
    ```

    Next, we are going to tell clients which connect to the VPN to use Pi-Hole as primary DNS server. Edit the OpenVPN configuration file:

    ```bash
    sudo vi /etc/openvpn/server/server.conf
    ```

    Comment out all existing `dhcp-option` settings and replace them with the `tun0` interface:

    ```bash
    #push "dhcp-option DNS XX.XX.XXX.XXX"
    #push "dhcp-option DNS XX.XX.XXX.XXX"
    push "dhcp-option DNS 10.8.0.1"
    ```

    Restart the OpenVPN server:

    ```bash
    sudo systemctl restart openvpn-server@server
    ```

    Browse to the Pi-Hole web interface, in this example `http://192.168.1.100/admin`. Then navigate to `Settings ‣ DNS ‣ Interface listening behavior` and select the option `Listen on all interfaces, permit all origins`.

    **Caution**: check your router settings and make sure that port `53` is closed. Refer to the router's manual for more information. Alternatively, you can scan for open ports using the tools of [Gibson Research Corporation](https://www.grc.com/x/ne.dll?bh0bkyd2): select `Proceed ‣ All service ports` and make sure port `53` doesn't appear as `open`.



    ### Clients

    Only devices with valid certificates can establish a VPN connection to securely access self-hosted services. Here is how to transfer the previously generated certificates from the server to the devices.

    === "Desktop devices (Ubuntu/Linux)"

        We assume the client device runs Ubuntu/Linux and can establish a [remote SSH login](https://gofoss.today/ubuntu-server/). Open a terminal on the client device, switch to the administrator user and retrieve all certificates from the server:

        ```bash
        su - gofossadmin
        scp -v -P 2222 gofossadmin@192.168.1.100:/home/gofossadmin/*.ovpn .
        ```

        Add OpenVPN to the network manager:

        ```bash
        sudo apt install network-manager-openvpn
        sudo apt install network-manager-openvpn-gnome
        sudo service network-manager restart
        ```

        Finally, configure the device's network connection:

        <center>

        | Step | Instruction |
        | :------: | ------ |
        | 1 |Click on the Wifi icon in the top bar. |
        | 2 |Click on `Settings`. |
        | 3 |Navigate to the `Network` menu entry. |
        | 4 |Click on the `+` icon in the `VPN` section. |
        | 5 |Select `Import from file`. |
        | 6 |Browse to the certificate (in this example `computer_vpn.ovpn`) and click on `Open`*. |
        | 7 |Click on the Wifi icon in the top bar. |
        | 8 |Enable the VPN connection `computer_vpn`. |

        </center>

        That's it! Your desktop device can securely connect to the server via VPN, from anywhere in the world.


    === "Mobile devices (Android)"

        Open F-Droid on your mobile device and install [OpenVPN for Android](https://f-droid.org/en/packages/de.blinkt.openvpn/).

        Connect the mobile device via USB to the desktop device with which you retrieved all certificates from the server. Copy the certificate from the desktop device to the mobile device (in this example `phone_vpn.ovpn`).

        Finally, open the OpenVPN app on your mobile device and configure it:

        <center>

        | Step | Instruction |
        | :------: | ------ |
        | 1 |Tap on `+` and then `Import`. |
        | 2 |Browse to the certificate and import it. |

        </center>

        That's it! Your mobile device can securely connect to the server via VPN, from anywhere in the world.



<br>

<center> <img src="../../assets/img/separator_tasks.svg" alt="Final checks" width="150px"></img> </center>

## Final checks

Now that everything is set up, traffic should be handled as follows:

* Each time one of your devices looks up an address, it securely connects to the server via VPN
* If the address contains ads or trackers, Pi-Hole blocks those elements
* If the address points to a self-hosted service, Pi-Hole redirects the request to the server
* If the address points to an external website or service, Pi-Hole redirects the request to an upstream DNS server of your choice (e.g. Digitalcourage, UncensoredDNS, etc.)
* All traffic is encrypted via HTTPS

Make sure everything runs smoothly:

* browse to [ads-blocker.com](https://ads-blocker.com/testing/) or [cnn.com](https://edition.cnn.com/) an verify if ads are blocked. You can also browse to `https://mypihole.gofoss.duckdns.org` (adjust accordingly) and check the dashboard for blocked queries.
* browse to a local service, such as `https://mypihole.gofoss.duckdns.org` (adjust accordingly), and verify the address is resolved correctly
* browse to [dnsleaktest.com](https://www.dnsleaktest.com/) or [bash.ws](https://bash.ws/dnsleak) and make sure you're using the correct upstream DNS resolver


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Support" width="150px"></img> </center>

## Support

For further details or questions, refer to [Pi-hole's documentation](https://docs.pi-hole.net/), [Let's Encrypt's documentation](https://letsencrypt.org/docs/), [OpenVPN's documentation](https://openvpn.net/vpn-server-resources/) or ask the [Pi-hole community](https://discourse.pi-hole.net/), the [Let's Encrypt community](https://community.letsencrypt.org/) or the [OpenVPN community](https://openvpn.net/community/) for help.


</br>

<div align="center">
<img src="https://imgs.xkcd.com/comics/google_announcement.png" alt="Google announcement" width="250"></img>
</div>

</br>